﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.EF;
using NLM.BLL.Exceptions;
using NLM.BLL.Services;
using NLM.BLL.DTO;

namespace KrivoRuchka.Console
{
    class Program
    {
        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        static void Main(string[] args)
        {
            EFUnitOfWork unit = new EFUnitOfWork(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Files\Projects\NLM\NureLecturerManager.WEB\App_Data\NLMDB.mdf;Integrated Security=True;MultipleActiveResultSets=True");

            var accountService = new AccountService(unit);
            //accountService.Register("Сверчков Владимир Владимирович", "123456", "Студент", "email@mail.com", @"/Content/images/1.jpg");


            //var user = accountService.SearchUser(FIO: "Сверчков Владимир Владимирович", Email: "email@mail.com").Single();

            //try
            //{
            //    accountService.SearchStudent(user.UserId);
            //}
            //catch (NotFoundException)
            //{
            //    accountService.CreateGroup("БИКС-14-3");
            //    accountService.CreateStudent(user.UserId, "БИКС-14-3");
            //}


            // Lecturers

            //accountService.Register("Коваленко Андрей Валериевич", "123456", "Лектор", "email2@mail.com", @"/Content/images/1.jpg");

            bool adminRegister = accountService.Register("Алексеев Эдуард Андреевич", "123456", ADMIN_TYPE, "email1@mail.com", @"/Content/images/1.jpg");

            bool lecturerRegister = accountService.Register("Коваленко Владимир Викторович", "123456", LECTURER_TYPE,"email2@mail.com", @"/Content/images/1.jpg");
            UserDTO lecturer = accountService.SearchUser(type: LECTURER_TYPE).FirstOrDefault();
            bool lecturerCreation = accountService.CreateLecturer(lecturer.UserId, "Профессор", "ПИ", "Кококо");


            bool studReg = accountService.Register("Сверчков Виктор Владимирович", "123456", STUDENT_TYPE, "kostian205@meta.ua", @"/Content/images/1.jpg");
            UserDTO student = accountService.SearchUser(type: STUDENT_TYPE).FirstOrDefault();
            //bool groupCreate = accountService.CreateGroup("БИКС-14-3");
            //bool studCr = accountService.CreateStudent(student.UserId, "БИКС-14-3");

            //System.Console.WriteLine("AdminReg: {0}, LecturerReg: {1}, LecturerCr: {2}, StudReg: {3}, GroupCr: {4}, StudCr: {5}",
            //                                adminRegister, lecturerRegister, lecturerCreation, studReg, groupCreate, studCr);
            //accountService.Register("Бельзецкий Роксан Леонидович", "123456", "Лектор", "email2@mail.com", @"/Content/images/1.jpg");
        }
    }
}
