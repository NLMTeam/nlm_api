﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;
using NLM.BLL.DTObjects;

namespace NLM.BLL.Interfaces
{
    public interface IHelpService
    {
        //Literature
        ICollection<LiteratureDTO> GetLiteratureForDiscipline(int disciplineId);
        ICollection<LiteratureDTO> GetLiteratureForDiscipline(string disciplineName);
        ICollection<LiteratureDTO> SearchLiterature(int literatureId = -1, String Name = null, int disciplineId = -1);
        bool CreateLiterature(string name, int disciplineId);
        bool CreateLiterature(string name, string disciplineName);

        bool ChangeBookDiscipline(int bookId, int disciplineId);

        bool UpdateLiterature(int literatureId, string name = null, int disciplineId = -1);

        bool DeleteBook(int bookId);

        // Shared materials
        ICollection<SharedMaterialDTO> SearchSharedMaterials(int id = 0, string type = null, string materialDoc = null,
            int disciplinesId = 0);
        bool CreateSharedMaterial(string type, string materialDoc, int disciplinesId);
        bool DeleteSharedMaterials(int sharedMaterialsId);
        bool UpdateSharedMaterial(int sharedMaterialsId, string type = null, string materialDoc = null);
        ICollection<SharedMaterialDTO> GetSharedMaterialsForDiscipline(int disciplineId);
    }
}
