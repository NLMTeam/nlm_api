﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;

namespace NLM.BLL.Interfaces
{
    public interface ICourseProjectService
    {
        StudentMarksDTO GetCourseProjectMarksOfGroup(string disciplineName, string GroupNumber);
        bool SetCourseProjectMark(int CpId, int userId, int mark, DateTime pass = default(DateTime), int disciplineForYearId = -1);
        bool EditConsolidation(int consolidationId, int userId = -1, int mark = -1);
        bool DeleleStudentConsolidation(int consolidationId);
        bool DeleteConsolidation(int consolidationId);
        ICollection<CourseProjectDTO> GetCourseProjects(string disciplineName);
        ICollection<CourseProjectDTO> SearchCourseProjects(int userId = -1, int discId = -1, String description = null,
            String theme = null, String type = null, string document = null, int courseProjectId = -1);
        ICollection<ConsolidationDTO> SearchConsolidation(int consolidationId = -1, int userId = -1, int courseProjectId = -1,
                        DateTime consolidationDate = default(DateTime), DateTime pass = default(DateTime), int mark = -1, int disciplineForYearId = -1);
        void ConsolidateCourseProject(int userId, int courseProjectId, int disciplineForYearId, DateTime when = default(DateTime));
        bool CreateCourseProject(int userId, int discId, string description = "", string theme = "", string type = "", string document = "");
        bool UpdateCourseProject(int CpId, int userId = -1, String description = null, String theme = null, String type = null, int discId = -1, string document = null);
        bool DeleteCourseProject(int CpId);
    }
}
