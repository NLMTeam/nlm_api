﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTObjects;

namespace NLM.BLL.Interfaces
{
    public interface ICommunication
    {
        ICollection<QuestionDTO> SearchQuestion(int id = 0, string theme = null, string text = null, DateTime date = default(DateTime), int authorId = 0);
        ICollection<AnswerDTO> SearchAnswer(int id = 0, string text = null, DateTime date = default(DateTime), int questionId = 0, int authorId = 0);
        ICollection<NewsDTO> SearchNews(int id = 0, string theme = null, string text = null, DateTime date = default(DateTime),int authorId = 0);
        bool CreateNews(string theme, string text, int authorId);
        bool DeleteNews(int newsId);
        bool CreateQuestion(string theme, string text, int authorId);
        bool CreateAnswer(string text, int questionId, int authorId);
        bool DeleteQuestion(int questionId);
        bool DeleteAnswer(int answerId);
        bool UpdateNews(int newsId, string theme = null, string text = null);
        bool UpdateQuestion(int questionId, string theme = null, string text = null);
        bool UpdateAnswer(int answerId, string text);
        ICollection<NewsDTO> GetActualNews();
    }
}
