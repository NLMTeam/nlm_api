﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;

namespace NLM.BLL.Interfaces
{
    public interface IControlPointService
    {
        StudentMarksDTO GetControlPointsMarksOfGroup(Int32 dfyId, string groupName);
        ICollection<ControlPointDTO> SearchControlPoints(int discYId = -1, string name = null,
            DateTime near = default(DateTime),int cpId = -1, int minScore = -1, int maxScore = -1);
        bool CreateControlPoint(Int32 dfyId, string name = "", int number =0 , DateTime near = default(DateTime),
                                int minScore = 0, int maxScore = 0);
        bool UpdateControlPoint(int CpId, int dfyId = -1, String name = null, int Number = -1, DateTime near = default(DateTime),
                        int minScore = -1, int maxScore = -1);
        bool DeleteControlPoint(int CpId);
        void SetMark(string studentName, int mark, int CpId);
        void SetMark(int userId, int mark, int CpId, DateTime pass = default(DateTime), int finalMark = -1);
        ICollection<ControlPointMarkDTO> SearchControlPointMarks(int userId = -1, int mark = -1, int CpId = -1,
            DateTime pass = default(DateTime), String finalMark = null);
        bool DeleteMark(int CpId, int userId, DateTime pass);
        Int32 UpdateMarksList(ICollection<ControlPointMarkDTO> marks, Int32 userId);
    }
}
