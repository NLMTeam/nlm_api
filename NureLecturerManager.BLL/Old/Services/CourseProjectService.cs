﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.Exceptions;
using NLM.DAL.Models.Enums;

namespace NLM.BLL.Services
{
    public class CourseProjectService : Interfaces.ICourseProjectService
    {
        IUnitOfWork unit;
        public CourseProjectService(IUnitOfWork u)
        {
            unit = u;
        }


        public StudentMarksDTO GetCourseProjectMarksOfGroup(string disciplineName, string GroupId)
        {
            if (unit.Disciplines.Find(d => d.Name == disciplineName).FirstOrDefault() == null)
            {
                return null;
            }

            if (unit.Groups.Get(GroupId) == null)
            {
                return null;
            }


            var students = unit.Students.Find(s => s.GroupNumber == GroupId)
                                        .OrderBy(s => s.User.FIO);

            StudentMarksDTO result = new StudentMarksDTO();

            result.Students = students.Select(s => s.User.FIO);

            var controlPoints = unit.ControlPoints.Find(cp => cp.DisciplineForYear.Discipline.IsCourseProject &&
                                                        cp.DisciplineForYear.Discipline.Name == disciplineName);

            List<string> sectionName = new List<string>();
            List<List<int>> marks = new List<List<int>>();
            foreach (var point in controlPoints)
            {
                sectionName.Add(point.Name);
                List<int> cpmark = new List<int>();

                foreach (var student in students)
                {
                    var mark = unit.ControlPointMarks
                                   .Find(cpm => cpm.UserId == student.User.UserId && 
                                                cpm.ControlPointId == point.ControlPointId)
                                   .FirstOrDefault();
                    cpmark.Add(mark != null ? mark.Mark : -1);
                }

                marks.Add(cpmark);
            }

            result.ControlPointNames = sectionName;
            result.Marks = marks;

            return result;
        }

        public bool SetCourseProjectMark(int CPId, int userId, int mark, DateTime pass = default(DateTime), int disciplineForYearId = -1)
        {
            Consolidation cons = unit.Consolidations
                                     .Find(c => c.UserId == userId &&
                                     c.CourseProjectId == CPId)
                                     .LastOrDefault();
            if (cons != null)
            {
                cons.Mark = mark;
                cons.PassDate = pass != default(DateTime) ? pass : DateTime.Now;
                cons.DisciplineForYearId = disciplineForYearId != -1 ? disciplineForYearId : cons.DisciplineForYearId;

                unit.Consolidations.Update(cons);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteConsolidation(int consolidationId)
        {
            if (unit.Consolidations.Get(consolidationId) != null)
            {
                unit.Consolidations.Delete(consolidationId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<CourseProjectDTO> GetCourseProjects(string disciplineName = null)
        {
            ICollection<CourseProject> courseProjects = unit.CourseProjects.GetAll();

            if (disciplineName == null)
                return courseProjects.Select(t => new CourseProjectDTO(t)).ToList();

            IEnumerable<CourseProject> courseProj = (from c in courseProjects
                                                     where c.Discipline.Name == disciplineName
                                                     select c);

            List < CourseProjectDTO > list = new List<CourseProjectDTO>();
            foreach(var proj in courseProj)
            {
                list.Add(new CourseProjectDTO()
                {
                    CourseProjectId = proj.CourseProjectId,
                    Description = proj.Description,
                    Theme = proj.Theme,
                    Document = proj.Document,
                    DisciplineId = proj.DisciplineId ?? -1,
                    UserId = proj.UserId.Value,
                    Type = proj.Type.ToString()
                });
            }

            return list;
        }

        public ICollection<CourseProjectDTO> SearchCourseProjects(int userId = -1, int discId = -1, string description = null, 
                                                                    string theme = null, string type = null, string document = null, int courseProjectId = -1)
        {
            List<CourseProjectDTO> list = new List<CourseProjectDTO>();
            ICollection<CourseProject> cps = unit.CourseProjects.Find(cp => (userId == -1 || cp.UserId == userId) &&
                                                                     (document == null || cp.Document == document) &&
                                                                     (discId == -1 || cp.DisciplineId == discId) &&
                                                                     (description == null ||
                                                                      cp.Description == description) &&
                                                                     (theme == null || cp.Theme == theme) &&
                                                                     (type == null || cp.Type.ToString() == type) &&
                                                                     (courseProjectId == -1 ||
                                                                      cp.CourseProjectId == courseProjectId));
            foreach (var project in cps)
            {
                list.Add(new CourseProjectDTO(project));
            }

            return list;
        }

        public ICollection<ConsolidationDTO> SearchConsolidation(int consolidationId = -1, int userId = -1, 
                                                  int courseProjectId = -1, DateTime consolidationDate = default(DateTime), 
                                                  DateTime pass = default(DateTime), int mark = -1, int disciplineForYearId = -1)
        {
            List<ConsolidationDTO> list = new List<ConsolidationDTO>();
            ICollection<Consolidation> consolidations =
                unit.Consolidations.Find(c => (consolidationId == -1 || c.ConsolidationId == consolidationId) &&
                                              (userId == -1 || c.UserId == userId) &&
                                              (courseProjectId == -1 || c.CourseProjectId == courseProjectId) &&
                                              (consolidationDate == default(DateTime) ||
                                               c.ConsolidationDate == consolidationDate) &&
                                              (pass == default(DateTime) || c.PassDate == pass) &&
                                              (mark == -1 || c.Mark == mark) && 
                                              (disciplineForYearId == -1 || c.DisciplineForYearId == disciplineForYearId));
            foreach (var cons in consolidations)
            {
                list.Add(new ConsolidationDTO(cons));
            }

            return list;
        }

        public void ConsolidateCourseProject(int userId, int courseProjectId, int disciplineForYearId, DateTime when = default(DateTime))
        {
            if (unit.Consolidations.Find(c => c.UserId == userId && c.DisciplineForYearId == disciplineForYearId)
                                    .FirstOrDefault() != null)
                throw new AlreadyHaveAConsolidationException("You already consolidated some project!");


            if (unit.Consolidations.Find(c1 => c1.Student.GroupNumber == unit.Students.Get(userId).GroupNumber &&
                                                   c1.CourseProjectId == courseProjectId)
                                   .FirstOrDefault() != null)
                    throw new ConsolidationIsBisyException("This course project is already consolidated on someone from your group!");
            
            Consolidation cons = new Consolidation()
            {
                CourseProjectId = courseProjectId,
                UserId = userId,
                ConsolidationDate = when != default(DateTime) ? when : DateTime.Now,
                DisciplineForYearId = disciplineForYearId
            };
            unit.Consolidations.Create(cons);
            unit.Save();
        }

        public bool CreateCourseProject(int userId, int discId, string description = "", string theme = "", string type = "", string document = "")
        {
            CourseProject cp = new CourseProject() {
                UserId = userId,
                Description = description,
                Theme = theme,
                Type = (CourseProjectType)Enum.Parse(typeof(CourseProjectType), type),
                DisciplineId = discId,
                Document = document
            };

            if (unit.CourseProjects.Find(c => (c.UserId == userId
                                               && c.DisciplineId == discId
                                               && c.Description == description
                                               && c.Theme == theme && c.Type.ToString() == type))
                                               .FirstOrDefault() == null)
            {
                unit.CourseProjects.Create(cp);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
    }

        public bool UpdateCourseProject(int CpId, int userId = -1, String description = null, String theme = null, 
                                        String type = null, int discId = -1, string document = null)
        {
            if (unit.CourseProjects.Get(CpId) == null)
            {
                return false;
            }

            if (unit.CourseProjects.Find(c => (c.UserId == userId && c.DisciplineId == discId
                                                                  && c.Description == description 
                                                                  && c.Theme == theme 
                                                                  && c.Type.ToString() == type))
                                                                  .FirstOrDefault() == null)
            {
                CourseProject cp = unit.CourseProjects.Get(CpId);
                cp.DisciplineId = discId != -1 ? discId : cp.DisciplineId;
                cp.UserId = userId != -1 ? userId : cp.UserId;
                cp.Description = description != null ? description : cp.Description;
                cp.Theme = theme != null ? theme : cp.Theme;
                cp.Type = type != null ? (CourseProjectType)Enum.Parse(typeof(CourseProjectType), type) : cp.Type;
                cp.Document = document != null ? document : cp.Document;
                unit.CourseProjects.Update(cp);
                unit.Save();
                return true;
        }
            else
                throw new AlreadyRegisteredItemException();
    }

        public bool DeleteCourseProject(int CpId)
        {
            if (unit.CourseProjects.Get(CpId) == null)
            {
                return false;
            }
            else
            {
                unit.CourseProjects.Delete(CpId);
                unit.Save();
                return true;
            }
        }

        public bool EditConsolidation(int consolidationId, int userId = -1, int mark = -1)
        {
            Consolidation c = unit.Consolidations.Get(consolidationId);

            if (c == null)
            {
                return false;
            }

            c.UserId = userId != -1 ? userId : c.UserId;
            c.Mark = mark != -1 ? mark : c.Mark;

            unit.Consolidations.Update(c);
            unit.Save();
            return true;
        }

        public bool DeleleStudentConsolidation(int consolidationId)
        {
            Consolidation co = unit.Consolidations.Get(consolidationId);
            if (co == null)
            {
                return false;
            }


            if (DateTime.Now - new TimeSpan(100, 0, 0) <= co.ConsolidationDate)
            {
                unit.Consolidations.Delete(consolidationId);
                unit.Save();
                return true;
            }
            else
            {
                throw new OneHundredHoursLeftException();
            }
        }
    }
}
