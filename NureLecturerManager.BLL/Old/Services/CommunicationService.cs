﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.BLL.DTObjects;
using NLM.BLL.Exceptions;
using NLM.BLL.Interfaces;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.Services
{
    internal class CommunicationService : ICommunication
    {
        private readonly IUnitOfWork _unit;

        public CommunicationService(IUnitOfWork u)
        {
            _unit = u;
        }

        public ICollection<QuestionDTO> SearchQuestion(int id = 0, string theme = null, string text = null,
            DateTime date = new DateTime(),
            int authorId = 0)
        {
            var list = new List<QuestionDTO>(
                _unit.Questions.Find(
                    q =>
                        (id == 0 || q.Id == id) && (theme == null || q.Theme == theme) &&
                        (text == null || q.Text == text) &&
                        (date == default(DateTime) || q.PublicationDateTime == date) &&
                        (authorId == 0 || q.AuthorId == authorId)).Select(s => new QuestionDTO(s)));

            return list;
        }

        public ICollection<AnswerDTO> SearchAnswer(int id = 0, string text = null, DateTime date = new DateTime(),
            int questionId = 0,
            int authorId = 0)
        {
            var list = new List<AnswerDTO>(
                _unit.Answers.Find(
                    a =>
                        (id == 0 || a.Id == id) && (text == null || a.Text == text) &&
                        (date == default(DateTime) || a.PublicationDateTime == date) &&
                        (questionId == 0 || a.QuestionId == questionId) && (authorId == 0 || a.AuthorId == authorId))
                    .Select(s => new AnswerDTO(s)));

            return list;
        }

        public ICollection<NewsDTO> SearchNews(int id = 0, string theme = null, string text = null,
            DateTime date = new DateTime(),
            int authorId = 0)
        {
            var list = new List<NewsDTO>(
                _unit.News.Find(
                    n =>
                        (id == 0 || n.Id == id) && (theme == null || n.Theme == theme) &&
                        (text == null || n.Text == text) &&
                        (date == default(DateTime) || n.PublicationDateTime == date) &&
                        (authorId == 0 || n.AuthorId == authorId)).Select(s => new NewsDTO(s)));

            return list;
        }

        public bool CreateNews(string theme, string text, int authorId)
        {

            List<NewsDTO> oldNews = SearchNews(theme: theme, text: text, authorId: authorId).ToList();

            if (oldNews.Count == 0)
            {
                News news = new News()
                {
                    Theme = theme,
                    AuthorId = authorId,
                    PublicationDateTime = DateTime.Now,
                    Text = text
                };
                _unit.News.Create(news);
                _unit.Save();
                return true;
            }

            return false;
        }

        public bool DeleteNews(int newsId)
        {
            var oldNews = SearchNews(newsId);

            if (oldNews.Count == 0)
            {
                return false;
            }


            _unit.News.Delete(newsId);
            _unit.Save();
            return true;
        }

        public bool CreateQuestion(string theme, string text, int authorId)
        {
            ICollection<QuestionDTO> oldQuestions = SearchQuestion(theme: theme, text: text, authorId: authorId);

            if (oldQuestions.Count == 0)
            {
                Question question = new Question()
                {
                    Theme = theme,
                    AuthorId = authorId,
                    PublicationDateTime = DateTime.Now,
                    Text = text
                };
                _unit.Questions.Create(question);
                _unit.Save();
                return true;
            }

            return false;
        }

        public bool CreateAnswer(string text, int questionId, int authorId)
        {
            ICollection<AnswerDTO> oldAnswers = SearchAnswer(text: text, questionId: questionId, authorId: authorId);

            if (oldAnswers.Count == 0)
            {
                Answer answer = new Answer()
                {
                    QuestionId = questionId,
                    AuthorId = authorId,
                    PublicationDateTime = DateTime.Now,
                    Text = text
                };
                _unit.Answers.Create(answer);
                _unit.Save();
                return true;
            }

            return false;
        }

        public bool DeleteQuestion(int questionId)
        {
            var oldQuestions = SearchQuestion(questionId);

            if (oldQuestions.Count == 0)
            {
                return false;
            }

            _unit.Questions.Delete(questionId);
            _unit.Save();
            return true;
        }

        public bool DeleteAnswer(int answerId)
        {
            var oldAnswers = SearchAnswer(answerId);

            if (oldAnswers.Count == 0)
            {
                return false;
            }

            _unit.Answers.Delete(answerId);
            _unit.Save();
            return true;
        }

        public bool UpdateNews(int newsId, string theme = null, string text = null)
        {
            News news = _unit.News.Get(newsId);

            if (news == null)
            {
                return false;
            }

            news.Theme = theme ?? news.Theme;
            news.Text = text ?? news.Text;
            _unit.News.Update(news);
            _unit.Save();
            return true;
        }

        public bool UpdateQuestion(int questionId, string theme = null, string text = null)
        {
            Question question = _unit.Questions.Get(questionId);

            if (question == null)
            {
                return false;
            }

            question.Theme = theme ?? question.Theme;
            question.Text = text ?? question.Text;
            _unit.Questions.Update(question);
            _unit.Save();
            return false;
        }

        public bool UpdateAnswer(int answerId, string text)
        {
            var answer = _unit.Answers.Get(answerId);
            if (answer == null)
            {
                return false;
            }

            answer.Text = text ?? answer.Text;
            _unit.Answers.Update(answer);
            _unit.Save();
            return true;
        }

        public ICollection<NewsDTO> GetActualNews()
        {
           var list = new List<NewsDTO>(_unit.News.Find(n=>n.DateLimit<=DateTime.Now).Select(s=> new NewsDTO(s)));

           return list;
        }
    }
}
