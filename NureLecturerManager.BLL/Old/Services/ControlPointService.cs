﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.BLL.DTO;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.Interfaces;
using NLM.BLL.Exceptions;
using NLM.DAL.Repositories;

namespace NLM.BLL.Services
{
    public class ControlPointService : IControlPointService
    {
        private IUnitOfWork unit;

        public ControlPointService(IUnitOfWork u)
        {
            unit = u;
        }
        public StudentMarksDTO GetControlPointsMarksOfGroup(int discYId, string groupName)
        {
            StudentMarksDTO model = new StudentMarksDTO();

            if (unit.DisciplinesForYear.Get(discYId) == null)
            {
                return null;
            }

            if (unit.Groups.Get(groupName) == null)
                throw new GroupNotFoundException();

            var students = unit.Students.Find(s => s.GroupNumber == groupName)
                                        .OrderBy(s => s.User.FIO);

            model.Students = students.Select(s => s.User.FIO);

            List<string> names = new List<string>();
            List<List<int>> marks = new List<List<int>>();
            foreach (var point in unit.ControlPoints
                                      .Find(cp => cp.DisciplineForYearId == discYId))
            {
                List<int> pointMarks = new List<int>();
                names.Add(point.Name);

                foreach (var student in students)
                {
                    var mark = unit.ControlPointMarks
                                   .Find(cpm => cpm.ControlPointId == point.ControlPointId &&
                                                cpm.UserId == student.User.UserId).OrderByDescending(cpm => cpm.PassDate)
                                   .FirstOrDefault();

                    if (mark != null)
                    {
                        pointMarks.Add(mark.Mark);
                    }
                    else
                    {
                        pointMarks.Add(-1);
                    }
                }
                marks.Add(pointMarks);
            }
            model.ControlPointNames = names;
            model.Marks = marks;
            return model;
        }

        public ICollection<ControlPointDTO> SearchControlPoints(int discYId = -1, String name = null, 
                                                   DateTime near = default(DateTime), int cpId = -1,  int minScore = -1, int maxScore = -1)
        {
            List<ControlPointDTO> controlPoints = new List<ControlPointDTO>();
            foreach (var point in unit.ControlPoints
                                      .Find(cp => (discYId == -1 || cp.DisciplineForYearId == discYId) &&
                                                  (string.IsNullOrEmpty(name) || cp.Name == name) &&
                                                  (near == default(DateTime) || cp.Date == near) &&
                                                  (minScore == -1 || cp.MinMark == minScore) &&
                                                  (maxScore == -1 || cp.MaxMark == maxScore) &&
                                                  (cpId == -1 || cp.ControlPointId == cpId)))
            {
                controlPoints.Add(new ControlPointDTO(point));
            }

            return controlPoints;
        }

        public bool CreateControlPoint(int discYId = 0, string name = "", int number = 0,  DateTime near = default(DateTime),
                                        int minScore = 0, int maxScore = 0)
        {
            ControlPoint cp = new ControlPoint();

            cp.DisciplineForYearId = discYId;
            cp.Name = name;
            cp.Date = near;
            cp.MinMark = minScore;
            cp.MaxMark = maxScore;
            cp.Number = number;

            if (unit.ControlPoints
                    .Find(co => co.DisciplineForYearId == cp.DisciplineForYearId && co.Name == cp.Name &&
                                cp.Number == number &&
                                cp.Date == co.Date && cp.MinMark == co.MinMark && cp.MaxMark == co.MaxMark)
                    .FirstOrDefault() == null)
            {
                unit.ControlPoints.Create(cp);
                unit.Save();
                return true;
            }


            return false;
        }

        public bool UpdateControlPoint(int CpId, int discYId = -1, String name = null, int Number = -1, DateTime near = default(DateTime), int minScore = -1, int maxScore = -1)
        {
            ControlPoint DbCp = unit.ControlPoints.Get(CpId);

            if (DbCp != null)
            {
                DbCp.DisciplineForYearId = discYId == -1 ? DbCp.DisciplineForYearId : discYId;
                DbCp.Name = name == null ? DbCp.Name : name;
                DbCp.Number = Number != -1 ? Number : DbCp.Number;
                DbCp.Date = near == default(DateTime) ? DbCp.Date : near;
                DbCp.MinMark = minScore == -1 ? DbCp.MinMark : minScore;
                DbCp.MaxMark = maxScore == -1 ? DbCp.MaxMark : maxScore;

                unit.ControlPoints.Update(DbCp);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool DeleteControlPoint(int CpId)
        {
            if (unit.ControlPoints.Find(t => t.ControlPointId == CpId)
                                  .FirstOrDefault() != null)
            {
                unit.ControlPoints.Delete(CpId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<ControlPointMarkDTO> SearchControlPointMarks(int userId = -1, int mark = -1, int CpId = -1,
                                                                    DateTime pass = default(DateTime), string finalMark = null)
        {
            List<ControlPointMarkDTO> list = new List<ControlPointMarkDTO>();
            foreach (var cpMark in unit.ControlPointMarks
                                       .Find(cpm => (userId == -1 || cpm.UserId == userId) &&
                                                    (mark == -1 || cpm.Mark == mark) &&
                                                    (CpId == -1 || cpm.ControlPointId == CpId) &&
                                                    (pass == default(DateTime) || cpm.PassDate == pass) &&
                                                    (finalMark == null || cpm.FinalMark == finalMark)))

            {
                list.Add(new ControlPointMarkDTO(cpMark));
            }

            return list;
        }

        public void SetMark(string studentName, int mark, int CpId)
        {
            SetMark(unit.Users.Find(u => u.FIO == studentName)
                              .FirstOrDefault().UserId, mark, CpId);
        }

        // False = Access denied
        public void SetMark(int userId, int mark, int CpId, DateTime pass = default(DateTime), int finalMark = -1)
        {
            var cpM = unit.ControlPointMarks
                          .Find(m => m.ControlPointId == CpId && m.UserId == userId)
                          .FirstOrDefault();

            if (cpM == null)
            {
                ControlPointMark cpm = new ControlPointMark();

                cpm.ControlPointId = CpId;
                cpm.Mark = mark;
                cpm.UserId = userId;

                cpm.PassDate = pass;
                if (finalMark != -1) cpm.FinalMark = finalMark.ToString();

                unit.ControlPointMarks.Create(cpm);
                unit.Save();
            }
            else
                this.UpdateMark(userId, mark, CpId, pass, finalMark);
        }

        public void UpdateMark(int userId, int mark = -1, int CpId = -1, DateTime pass = default(DateTime), int finalMark = -1)
        {
            ControlPointMark cpm = unit.ControlPointMarks.Get(CpId, userId, pass);

            if (cpm == null) this.SetMark(userId, mark, CpId, pass, finalMark);
            else
            {
                cpm.ControlPointId = CpId != -1 ? CpId : cpm.ControlPointId;
                cpm.Mark = mark != -1 ? mark : cpm.Mark;
                cpm.UserId = userId != -1 ? userId : cpm.UserId;

                cpm.FinalMark = finalMark != -1 ? finalMark.ToString() : cpm.FinalMark;

                unit.Save();
            }
        }

        public bool DeleteMark(int cPId, int userId, DateTime passDate)
        {
            if (unit.ControlPointMarks.Get(cPId, userId, passDate) != null)
            {
                unit.ControlPointMarks.Delete(cPId, userId, passDate);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public Int32 UpdateMarksList(ICollection<ControlPointMarkDTO> marks, Int32 userId)
        {
            Int32 denieded = 0;
            foreach (var mark in marks)
            {
                if (IsLecturer(mark.ControlPointId, userId))
                {
                    this.SetMark(mark.UserId, mark.Mark, mark.ControlPointId, mark.PassDate, Convert.ToInt32(mark.FinalMark));
                }
                else
                {
                    denieded++;
                    continue;
                }
            }

            return denieded;
        }

        private bool IsLecturer(Int32 dfyId, Int32 userId)
        {
            if (unit.LecturerForYears.Find(ldf => ldf.DisciplineForYearId == dfyId && ldf.LecturerId == userId).FirstOrDefault() == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}




