﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.Exceptions
{
    public class OneHundredHoursLeftException:Exception
    {
        public OneHundredHoursLeftException() :base()
        {

        }
    }
}
