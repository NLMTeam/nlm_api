﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class StudentDTO
    {
        public StudentDTO()
        {
        }

        internal StudentDTO(Student s)
        {
            this.UserId = s.UserId;
            this.GroupId = s.GroupNumber;
        }
        public int UserId { get; set; }
        public string GroupId { get; set; }
    }
}
