﻿using System;
using System.Collections.Generic;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class MainDTO
    {
        public IEnumerable<string> Disciplines { get; internal set; }
        public bool LogIn { get; internal set; }

    }
    public class StudentMarksDTO
    {
        public IEnumerable<string> Students { get; internal set; }
        public IEnumerable<string> ControlPointNames { get; internal set; }
        public IEnumerable<IEnumerable<int>> Marks { get; internal set; }
    }
}
