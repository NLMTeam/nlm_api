﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Models;

namespace NLM.BLL.DTObjects
{
    public class NewsDTO
    {
        public NewsDTO()
        {
            
        }

        internal NewsDTO(News n)
        {
            Id = n.Id;
            Theme = n.Theme;
            Text = n.Text;
            PublicationDateTime = n.PublicationDateTime;
            DateLimit = n.DateLimit;
            AuthorId = n.AuthorId;
        }

        public DateTime DateLimit { get; set; }
        public int Id { get; set; }
        public string Theme { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDateTime { get; set; }
        public int AuthorId { get; set; }
    }
}
