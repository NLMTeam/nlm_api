﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class StudentControlDTO
    {
        internal StudentControlDTO(StudentControl sc)
        {
            this.GroupControlId = sc.GroupControlId;
            this.UserId = sc.UserId;
            this.Mark = sc.Mark;
            this.PassDate = sc.PassDate;
        }
        public int GroupControlId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> Mark { get; set; }
        public System.DateTime PassDate { get; set; }
        public int StudentControlId { get; set; }
    }
}
