﻿using NLM.DAL.Models;

namespace NLM.BLL.DTObjects
{
    public class WorkTypeLightDTO
    {
            public WorkTypeLightDTO(WorkType wt)
            {
                this.Name = wt.Name;
                this.WorkTypeId = wt.WorkTypeId;
            }

        public WorkTypeLightDTO()
        {
                
        }

            public int WorkTypeId { get; set; }
            public string Name { get; set; }
        }
    }