﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.DAL.Models.Enums;

namespace NLM.BLL.DTO
{
    public class UserDTO
    {
        public UserDTO()
        {
        }

        internal UserDTO(User u)
        {
            this.FIO = u.FIO;
            this.Login = u.Login;
            this.Mail = u.Mail;
            this.Parol = u.Password;
            this.Type = u.Type.ToString();
            this.UserId = u.UserId;
            this.Image = u.Image;
            this.Saline = u.Saline;
        }
    
        public int UserId { get; set; }
        public string FIO { get; set; }
        public string Type { get; set; }
        public string Login { get; set; }
        public string Parol { get; set; }
        public string Mail { get; set; }
        public string Image { get; set; }
        public byte[] Saline { get; set; }
    }
}
