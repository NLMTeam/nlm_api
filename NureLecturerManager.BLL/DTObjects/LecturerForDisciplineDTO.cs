﻿using NLM.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.DTObjects
{
    public class LecturerForDisciplineDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }

        public LecturerForDisciplineDTO(LecturerForYear lect)
        {
            Id = lect.LecturerId;
            Name = lect.Lecturer.User.FIO;
        }

        public LecturerForDisciplineDTO(Assistant lect)
        {
            Id = lect.LecturerId;
            Name = lect.Lecturer.User.FIO;
        }

        public LecturerForDisciplineDTO()
        {

        }
    }
}
