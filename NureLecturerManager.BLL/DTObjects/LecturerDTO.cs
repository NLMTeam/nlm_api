﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class LecturerDTO
    {
        public LecturerDTO()
        {
        }

        internal LecturerDTO(Lecturer l)
        {
            this.Department = l.Department;

            this.FIO = l.User.FIO;
            this.Post = l.Post;
            this.UserId = l.User.UserId;
            this.Specialization = l.Specialization;
        }
        public int UserId { get; set; }
        public string FIO { get; set; }
        public string Post { get; set; }
        public string Department { get; set; }
        public String Specialization { get; set; }
    }
}
