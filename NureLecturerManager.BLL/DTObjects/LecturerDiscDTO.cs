﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.DTObjects
{
    public class LecturerDiscDTO
    {
        public String FIO { get; set; }
        public Int32 Id { get; set; }
    }
}
