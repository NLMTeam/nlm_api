﻿using NLM.DAL.Models;

namespace NLM.BLL.DTObjects
{
    public class QuestionDTO
    {
        public QuestionDTO()
        {
            
        }

        internal QuestionDTO(Question q)
        {
            Id = q.Id;
            Theme = q.Theme;
            Text = q.Text;
            if (q.AuthorId != null)
                AuthorId = q.AuthorId.Value;
            else
                AuthorId = null;
        }

        public string Theme { get; set; }

        public int Id { get; set; }

        public int? AuthorId { get; set; }
        
        public string Text { get; set; }
    }
}
