﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class ReferenceDTO
    {
        internal ReferenceDTO(Reference r)
        {
            this.LiteratureId = r.LiteratureId;
            this.PageFrom = r.PageFrom;
            this.PageTo = r.PageTo;
            this.WorkTypeId = r.WorkTypeId;
        }
        public int WorkTypeId { get; set; }
        public int LiteratureId { get; set; }
        public Nullable<int> PageFrom { get; set; }
        public Nullable<int> PageTo { get; set; }
    }
}
