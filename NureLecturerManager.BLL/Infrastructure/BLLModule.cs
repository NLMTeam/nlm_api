﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using NLM.BLL.Interfaces;
using NLM.BLL.Services;
using NLM.DAL.Interfaces;
using NLM.DAL.EF;

namespace NLM.BLL.Infrastructure
{
    public class BLLModule : Module
    {
        public string ConnectionString { get; set; }

        public BLLModule(string connectionString)
        {
            ConnectionString = connectionString;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new EFUnitOfWork(ConnectionString)).As<IUnitOfWork>();
            builder.Register(c => new AccountService(new EFUnitOfWork(ConnectionString))).As<IAccountService>();

            builder.Register(c => new GroupService(new EFUnitOfWork(ConnectionString))).As<IGroupService>();
            builder.Register(c => new StudentService(new EFUnitOfWork(ConnectionString))).As<IStudentService>();

            //builder.Register(c => new ControlPointService(new EFUnitOfWork(ConnectionString))).As<IControlPointService>();
            //builder.Register(c => new ControlService(new EFUnitOfWork(ConnectionString))).As<IControlService>();
            //builder.Register(c => new CourseProjectService(new EFUnitOfWork(ConnectionString))).As<ICourseProjectService>();
            builder.Register(c => new DisciplineService(new EFUnitOfWork(ConnectionString))).As<IDisciplineService>();
            //builder.Register(c => new HelpService(new EFUnitOfWork(ConnectionString))).As<IHelpService>();
        }
    }
}
