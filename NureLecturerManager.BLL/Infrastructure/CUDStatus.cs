﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.Infrastructure
{
    public class CUDStatus
    {
        public ActionStatus Status { get; set; }
        public string Message { get; set; }

        public CUDStatus(ActionStatus status)
        {
            Status = status;
        }

        public CUDStatus(ActionStatus status, String message)
        {
            Status = status;
            Message = message;
        }

    }

    public enum ActionStatus
    {
        Ok, 
        NotFound,
        AlreadyCreated,
        DALException
    }

}
