﻿using System;
using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.BLL.Interfaces
{
    public interface IAccountService
    {
        UserDTO LogIn(string login, string password);
        ICollection<UserDTO> SearchUser(int userId = -1, String FIO = null, String Login = null, String Password = null,
                                        String Type = null, String Email = null);
        bool Register(string FIO, string password, string type, string email, string image);
        bool Register(string FIO, string login, string password, string type, string email, string image);
        bool SendPassword(string login, string email);
        void SendChangedPass(string login, string pass, string email);
        void SendLogin(String login, String email);
        bool EditUser(int userId, String FIO = null, String Login = null, String password = null, String type = null,
                    String email = null, String Image = null);
        bool DeleteUser(int userId);

        ICollection<LecturerDTO> SearchLecturer(int userId = -1, string Department = null, 
                                                String Post = null, String spec = null);
        ICollection<LecturerDTO> GetAllLecturers();
        bool CreateLecturer(int userId, string post = "", string department = "", string specialization = null);
        bool UpdateLecturer(int userId, string post = "", string department = "", string spec = null);
        bool DeleteLecturer(int userId);

        ICollection<StudentDTO> SearchStudent(int userId = -1, String GroupNum = null);
        ICollection<StudentDTO> SearchStudent(String login, String GroupNum = null);
        bool CreateStudent(int userId, string GroupNum = "");
        bool AddStudentToGroup(int userId, string GroupNum);
        bool DeleteStudentFromGroup(int userId);
        bool DeleteStudent(int userId);

        //ICollection<StudentDTO> GetStudentsOfGroup(string groupName);
        //ICollection<GroupDTO> SearchGroup(String GroupNum = null, int captainId = -1);
        //bool CreateGroup(string GroupNum, int captainId = -1);
        //UserDTO GetCaptain(string groupNum);
        //bool ChangeCaptain(string GroupNum, int captainId);
        //bool DeleteGroup(string GroupNum);
        //ICollection<LecturerDiscDTO> GetLecturersNames(ICollection<int> lecturersIds);

        byte[] GetToken(Int32 id);
        byte[] GetTokenAndSaveToDB(Int32 userId);
        void DeleteTokenFromDB(byte[] token);
        Int32 GetUserIdByToken(byte[] token);
        String GetRoleByToken(byte[] token);
        byte[] DecodeToken(String hex);
        String GetUserGroupNameByToken(byte[] token);
    }
}
