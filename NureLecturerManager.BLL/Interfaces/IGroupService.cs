﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.Infrastructure;
using NLM.BLL.DTO;

namespace NLM.BLL.Interfaces
{
    public interface IGroupService
    {
        IEnumerable<GroupDTO> GetGroups();
        IEnumerable<GroupDTO> FindGroups(Func<GroupDTO, bool> condition);
        GroupDTO GetGroup(string groupName);

        IEnumerable<UserDTO> GetGroupStudents(string name);

        CUDStatus CreateGroup(GroupDTO group);
        CUDStatus UpdateGroup(GroupDTO group);
        CUDStatus DeleteGroup(string name);
    }
}
