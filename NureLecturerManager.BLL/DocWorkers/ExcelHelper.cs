﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GemBox.Spreadsheet;
using System.IO;

namespace NLM.BLL.DocWorkers
{
    public class ExcelHelper
    {
        static ExcelHelper()
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
        }

        public void GenerateEmptyFile(string fileName)
        {
            var file = new ExcelFile();
            file.Save(fileName);
        }

        public void FillFileContent(Dictionary<int, List<string>> values, string fileName = null)
        {
            ExcelFile file;
            if (fileName != null)
            {
                if (File.Exists(fileName))
                {
                    file = ExcelFile.Load(fileName);
                }
                else
                {
                    file = new ExcelFile();
                }
            }
            else
            {
                file = new ExcelFile();
            }

            ExcelWorksheet workSheet;
            if (file.Worksheets.Count > 0)
            {
                workSheet = file.Worksheets[0];
            }
            else
            {
                workSheet = file.Worksheets.Add("Marks");
            }

            for (int i = 0; i < values.Keys.Count; i++)
            {
                for (var j = 0; j < values[i].Count; j++)
                {
                    workSheet.Cells[i, j].Value = values[i][j];
                    var length = values[i][j].Length;
                    if (workSheet.Columns[j].Width < length * 256)
                    {
                        workSheet.Columns[j].Width = length * 256;
                    }
                }
            }

            file.Save(fileName);
        }

        public Dictionary<int, List<string>> ReadFileContent(string fileName)
        {
            var file = ExcelFile.Load(fileName);
            var result = new Dictionary<int, List<string>>();

            var workSheet = file.Worksheets[0];
            int i = 0;

            foreach (ExcelRow row in workSheet.Rows)
            {
                List<string> rowValues = new List<string>();
                foreach (ExcelCell cell in row.AllocatedCells)
                {
                    if (cell.Value != null)
                    {
                        rowValues.Add(cell.Value.ToString());
                    }
                    else
                    {
                        rowValues.Add("");
                    }
                }

                result.Add(i++, rowValues);
            }

            return result;
        }
    }
}
