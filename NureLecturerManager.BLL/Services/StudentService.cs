﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.BLL.DTO;
using NLM.BLL.Infrastructure;
using NLM.BLL.Interfaces;
using NLM.DAL.Interfaces;
using AutoMapper;
using NLM.DAL.Models;

namespace NLM.BLL.Services
{
    internal class StudentService : IStudentService
    {
        private IUnitOfWork _unit;
        private IMapper _mapper;
        private IAccountService _service;

        public StudentService(IUnitOfWork unit)
        {
            this._unit = unit;

            MapperConfiguration config = new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<StudentDTO, Student>();
                cfg.CreateMap<Student, StudentDTO>();

                cfg.CreateMap<UserDTO, User>();
                cfg.CreateMap<User, UserDTO>();
            });

            _mapper = config.CreateMapper();

            this._service = new AccountService(this._unit);
        }

        public CUDStatus CreateStudent(UserDTO user, string groupName)
        {
            if (_unit.Students.Get(user.UserId) != null)
            {
                return new CUDStatus(ActionStatus.AlreadyCreated, "Student with such id already exists.");
            }

            if (_unit.Users.Get(user.UserId) == null)
            {
                user.Type = "STUDENT";
                user.Parol = Guid.NewGuid().ToString().Substring(0, 10);

                try
                {
                    bool registerResult = _service.Register(user.FIO, user.Login, user.Parol, user.Type, user.Mail, null);

                    if (!registerResult)
                    {
                        return new CUDStatus(ActionStatus.DALException, "Cannot register user");
                    }
                }
                catch (Exception e)
                {
                    return new CUDStatus(ActionStatus.DALException, e.Message);
                }

                User dbUser = _unit.Users.Find(u => u.Login == user.Login).LastOrDefault();
                user = _mapper.Map<UserDTO>(dbUser);
            }

            try
            {
                _unit.Students.Create(new Student() { UserId = user.UserId, GroupNumber = groupName });
                _unit.Save();

            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);

        }

        public CUDStatus UpdateStudent(UserDTO user, string groupName)
        {
            if (_unit.Students.Get(user.UserId) == null)
            {
                return new CUDStatus(ActionStatus.NotFound);
            }

            if (_unit.Users.Get(user.UserId) == null)
            {
                try
                {
                    bool updateUserResult = _service.EditUser(user.UserId, user.FIO, user.Login, user.Parol, user.Type, user.Mail, user.Image);

                    if (!updateUserResult)
                    {
                        return new CUDStatus(ActionStatus.DALException, "Cannot edit user personal information");
                    }
                }
                catch (Exception e)
                {
                    return new CUDStatus(ActionStatus.Ok, e.Message);
                } 
            }

            try
            {
                _unit.Students.Update(new Student() { UserId = user.UserId, GroupNumber = groupName });
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public CUDStatus DeleteStudent(int userId)
        {
            if (_unit.Students.Get(userId) == null)
            {
                return new CUDStatus(ActionStatus.NotFound);
            }

            try
            {
                _unit.Students.Delete(userId);
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public IEnumerable<StudentDTO> FindStudents(Func<UserDTO, bool> condition)
        {
            return _unit.Students.Find(s => condition(_mapper.Map<UserDTO>(s.User))).Select(_mapper.Map<StudentDTO>);
        }

        public StudentDTO GetStudent(int id)
        {
            Student s = _unit.Students.Get(id);
            return _mapper.Map<StudentDTO>(s);
        }

        public IEnumerable<StudentDTO> GetStudents()
        {
            return _unit.Students.GetAll().Select(_mapper.Map<StudentDTO>);
        }
    }
}
