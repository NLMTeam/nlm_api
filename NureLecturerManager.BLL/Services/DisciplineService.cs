﻿using NLM.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO.DisciplinePart;
using NLM.BLL.Infrastructure;
using AutoMapper;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.DTO;
using NLM.BLL.DTObjects;
using AutoMapper.Mappers;

namespace NLM.BLL.Services
{
    public class DisciplineService : IDisciplineService
    {
        private IUnitOfWork _unit;
        private IMapper _mapper;

        public DisciplineService(IUnitOfWork unit)
        {
            _unit = unit;

            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<LecturerForYear, LecturerForDisciplineDTO>().ConstructUsing(t => new LecturerForDisciplineDTO(t));
                cfg.CreateMap<LecturerForDisciplineDTO, LecturerForYear>().ConvertUsing(l => new LecturerForYear()
                {
                    LecturerId = l.Id
                });

                cfg.CreateMap<Assistant, LecturerForDisciplineDTO>().ConstructUsing(t => new LecturerForDisciplineDTO(t));
                cfg.CreateMap<LecturerForDisciplineDTO, Assistant>().ConvertUsing(l => new Assistant()
                {
                    LecturerId = l.Id
                });

                cfg.CreateMap<DisciplineDTO, Discipline>();
                cfg.CreateMap<Discipline, DisciplineDTO>();

                cfg.CreateMap<DisciplineForYear, DisciplineForYearDTO>().ConstructUsing(x => new DisciplineForYearDTO(x));

                cfg.CreateMap<DisciplineForYearDTO, DisciplineForYear>()
                    .ConvertUsing(t => new DisciplineForYear()
                    {
                        DisciplineForYearId = t.DisciplineForYearId,
                        DisciplineId = t.DisciplineId,
                        Year = t.Year,
                        Semester = t.Semester
                    });

                cfg.CreateMap<DisciplineForYear, DisciplineForYearLightDTO>().ConstructUsing(x => new DisciplineForYearLightDTO(x));
            });

            _mapper = config.CreateMapper();
        }
        
        public CUDStatus CreateDFY(DisciplineForYearDTO dfy)
        {
            if (_unit.DisciplinesForYear.Find(df => df.DisciplineId == dfy.DisciplineId &&
                                                    df.Year == dfy.Year &&
                                                    df.Semester == dfy.Semester).Any())
            {
                return new CUDStatus(ActionStatus.AlreadyCreated);
            }

            try
            {
                _unit.DisciplinesForYear.Create(_mapper.Map<DisciplineForYear>(dfy));
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public CUDStatus CreateDiscipline(DisciplineDTO discipline)
        {
            if (_unit.Disciplines.Find(d => d.Name == discipline.Name).Any())
            {
                return new CUDStatus(ActionStatus.AlreadyCreated);
            }

            try
            {
                _unit.Disciplines.Create(_mapper.Map<Discipline>(discipline));
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public CUDStatus DeleteDFY(int dfyId)
        {
            if (!_unit.DisciplinesForYear.Find(dfy => dfy.DisciplineForYearId == dfyId)
                                            .Any())
            {
                return new CUDStatus(ActionStatus.NotFound);
            }

            try
            {
                _unit.DisciplinesForYear.Delete(dfyId);
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public CUDStatus DeleteDiscipline(Int32 id)
        {
            if (_unit.Disciplines.Get(id) == null)
            {
                return new CUDStatus(ActionStatus.NotFound);
            }

            try
            {
                _unit.Disciplines.Delete(id);
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public ICollection<DisciplineDTO> FindDiscipline(String name)
        {
            return _mapper.Map<ICollection<DisciplineDTO>>(_unit.Disciplines.Find(d => d.Name == name));
        }

        public IEnumerable<DisciplineForYearDTO> GetDFYs()
        {
            var discs = _unit.DisciplinesForYear.GetAll()
                                            .Select(_mapper.Map<DisciplineForYearDTO>).ToList();
            return discs;
        }

        public IEnumerable<DisciplineForYearDTO> FindDisciplineForYear(Int32 disciplineId, String year, Int32 semester)
        {
            return _unit.DisciplinesForYear.Find(dfy => dfy.DisciplineId == disciplineId && dfy.Year ==
                                                                year && dfy.Semester == semester).
                                                                Select(_mapper.Map<DisciplineForYearDTO>);
        }

        public IEnumerable<DisciplineDTO> GetDisciplines()
        {
            return _unit.Disciplines.GetAll()
                                .Select(_mapper.Map<DisciplineDTO>);
        }

        public IEnumerable<DisciplineForYearDTO> GetDFYsForDiscpline(Int32 disciplineId)
        {
            return _unit.DisciplinesForYear.Find(dfy => dfy.DisciplineId == disciplineId)
                    .Select(_mapper.Map<DisciplineForYearDTO>);
        }

        public IEnumerable<DisciplineForYearDTO> SearchDFYsForDiscipline(string discName)
        {
            return _unit.DisciplinesForYear.Find(dfy => dfy.Discipline.Name == discName)
                                .Select(_mapper.Map<DisciplineForYearDTO>);
        }

        public IEnumerable<DisciplineForYearLightDTO> SearchDFYsForYearAndGroup(String year, int semester, string groupName)
        {
            return _unit.DisciplinesForYear.Find(dfy => dfy.Year == year && dfy.Semester == semester &&
                                                    dfy.Groups.Any(g => g.GroupName == groupName))
                                                        .Select(_mapper.Map<DisciplineForYearLightDTO>);
        }

        public IEnumerable<DisciplineForYearLightDTO> SearchDFYsForYearAndLecturer(String year, int semester, int lecturerId)
        {
            return _unit.DisciplinesForYear.Find(dfy => dfy.Year == year && dfy.Semester == semester &&
                                                        (dfy.Lecturers.Any(g => g.LecturerId == lecturerId) ||
                                                        dfy.Assistants.Any(g => g.LecturerId == lecturerId)))
                                                          .Select(_mapper.Map<DisciplineForYearLightDTO>);
        }
    }
}
