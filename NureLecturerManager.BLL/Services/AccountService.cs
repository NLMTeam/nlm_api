﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.Email;
using NLM.BLL.DTO;
using System.Security.Cryptography;
using NLM.BLL.DTObjects;
using NLM.DAL.Models.Enums;
using NLM.BLL.Exceptions;

namespace NLM.BLL.Services
{
    public class AccountService : Interfaces.IAccountService
    {
        private IUnitOfWork unit;
        private const int SALINE_LENGTH = 50;

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        public AccountService(IUnitOfWork u)
        {
            unit = u;
        }

        // Методы пользователя
        public UserDTO LogIn(string LogIn, string password)
        {
            User user = null;
            user = unit.Users.Find(u => u.Login == LogIn).FirstOrDefault();

            if (user != null)
            {
                var service = SHA256.Create();

                var stringToEncode = Convert.ToBase64String(user.Saline) + password;

                string encoded = Convert.ToBase64String(service.ComputeHash(Encoding.UTF8.GetBytes(stringToEncode)));

                if (user.Password == encoded)
                {
                    return new UserDTO(user);
                }
                else
                {
                    throw new IncorrectPasswordException();
                }
            }

            return null;
        }

        public ICollection<UserDTO> SearchUser(int userId = -1, string FIO = null, string Login = null, 
                                                string Password = null, string type = null, string Email = null)
        {
            List<UserDTO> list = new List<UserDTO>();
            UserType searchUserType = 0;
            if (type != null)
            {
                searchUserType = (UserType)Enum.Parse(typeof(UserType), type);
            }

            foreach (var user in unit.Users
                                     .Find(u => (userId == -1 || u.UserId == userId) &&
                                          (FIO == null || u.FIO == FIO) &&
                                          (Login == null || u.Login == Login) &&
                                          (Password == null || u.Password == Password) &&
                                          (type == null || searchUserType == u.Type) &&
                                          (Email == null || u.Mail == Email)).ToList())
            {
                list.Add(new UserDTO(user));
            }

            return list;
        }

        public bool Register(string FIO, string password, string type, string email, string image)
        {
            User u = new User();

            if (unit.Users.Find(us => us.Mail == email).FirstOrDefault() != null)
            {
                // User already exists
                return false;
            }

            u.FIO = FIO;
            u.Login = Guid.NewGuid().ToString();
            u.Type = (UserType)Enum.Parse(typeof(UserType), type);
            u.Mail = email;

            if (image == null)
            {
                u.Image = "/Content/images/1.jpg";
            }
            else
            {
                u.Image = image;
            }
            u.Image = image;
            u.Saline = getSaline();

            var encoder = SHA256.Create();

            var stringToEncode = Convert.ToBase64String(u.Saline) + password;

            u.Password = Convert.ToBase64String(encoder.ComputeHash(Encoding.UTF8.GetBytes(stringToEncode)));

            if (type != STUDENT_TYPE && type != LECTURER_TYPE && type != ADMIN_TYPE)
                throw new NotExpectedUserTypeException();

            while (unit.Users.Find(o => o.Login == u.Login).FirstOrDefault() != null)
                u.Login = Guid.NewGuid().ToString();

            unit.Users.Create(u);
            unit.Save();

            var user = new User();
            user.FIO = u.FIO;
            user.Image = u.Image;
            user.Login = u.Login;
            user.Mail = u.Mail;
            user.Password = password;
            user.Type = u.Type;
            SendInvitation(user);
            return true;
        }

        private byte[] getSaline()
        {
            RNGCryptoServiceProvider service = new RNGCryptoServiceProvider();

            byte[] data = new byte[SALINE_LENGTH];
            service.GetBytes(data);

            return data;
        }

        // old version but currently used by student service
        public bool Register(string FIO, string login, string password, string type, string email, string image)
        {
            User u = new User();

            u.FIO = FIO;
            u.Login = login;
            u.Type = (UserType)Enum.Parse(typeof(UserType), type);
            u.Mail = email;
            u.Image = image;
            u.Saline = getSaline();

            var encoder = SHA256.Create();

            var stringToEncode = Convert.ToBase64String(u.Saline) + password;

            u.Password = Convert.ToBase64String(encoder.ComputeHash(Encoding.UTF8.GetBytes(stringToEncode)));

            if (type != STUDENT_TYPE && type != LECTURER_TYPE && type != ADMIN_TYPE)
                throw new NotExpectedUserTypeException();

            if (unit.Users.Find(o => o.Login == u.Login && o.FIO == u.FIO && o.Password == password && o.Mail == u.Mail && o.Type == u.Type).FirstOrDefault() == null)
            {
                unit.Users.Create(u);
                unit.Save();

                var user = new User();
                user.FIO = u.FIO;
                user.Image = u.Image;
                user.Login = u.Login;
                user.Mail = u.Mail;
                user.Password = password;
                user.Type = u.Type;
                SendInvitation(user);
                return true;
            }
            else
            {
                // User already exists
                return false;
            }
        }

        public bool EditUser(int userId, String FIO = null, String Login = null, String password = null, String type = null,
                            String email = null, string image = null)
        {
            User Db = unit.Users.Get(userId);

            if (type != STUDENT_TYPE && type != LECTURER_TYPE && type != null && type != ADMIN_TYPE)
            {
                throw new NotExpectedUserTypeException();
            }

            if (Db == null)
            {
                return false;
            }

            if (Login == null || unit.Users.Find(l => l.Login == Login).FirstOrDefault() == null)
            {
                Db.FIO = FIO != null ? FIO : Db.FIO;
                Db.Login = Login != null ? Login : Db.Login;
                Db.Type = type != null ? (UserType)Enum.Parse(typeof(UserType), type) : Db.Type;
                Db.Mail = email != null ? email : Db.Mail;
                Db.Image = image != null ? image : Db.Image;

                if (password != null)
                {
                    Db.Saline = getSaline();

                    var service = SHA256.Create();
                    var stringToEncode = Convert.ToBase64String(Db.Saline) + password;

                    Db.Password = Convert.ToBase64String(service.ComputeHash(Encoding.UTF8.GetBytes(stringToEncode)));
                }

                unit.Users.Update(Db);
                unit.Save();
            }
            else
            {
                return false;
            }

            return true;
        }

        public bool DeleteUser(int userId)
        {
            if (unit.Users.Get(userId) != null)
            {
                unit.Users.Delete(userId);
            }
            else
            {
                return false;
            }

            unit.Save();
            return true;
        }

        // Методы преподавателя
        public ICollection<LecturerDTO> SearchLecturer(int userId = -1, string Department = null, 
                                                        string Post = null, string spec = null)
        {
            List<LecturerDTO> list = new List<LecturerDTO>();
            foreach (var lect in unit.Lecturers
                                     .Find(l => (userId == -1 || l.User.UserId == userId) &&
                                                (Department == null || Department == l.Department) &&
                                                (Post == null || l.Post == Post) &&
                                                (spec == null || l.Specialization == spec)))
            {
                list.Add(new LecturerDTO(lect));
            }

            return list;
        }

        public bool CreateLecturer(int userId, string post = "", string department = "", string specialization = null)
        {
            Lecturer l = new Lecturer() { 
                UserId = userId,
                Post = post,
                Department = department,
                Specialization = specialization
            };
            Lecturer lecturer = unit.Lecturers.Get(l.UserId);
            if (lecturer == null)
            {
                unit.Lecturers.Create(l);
                unit.Save();
                return true;
            }

            return false;

        }

        public bool UpdateLecturer(int userId, string post = "", string department = "", string spec = null)
        {
            Lecturer l = unit.Lecturers.Get(userId);
            if (l != null)
            {
                l.Post = post;
                l.Department = department;
                l.Specialization = spec;
                unit.Lecturers.Update(l);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteLecturer(int userId)
        {
            if (unit.Users.Get(userId) != null && unit.Lecturers.Get(userId) != null)
            {
                unit.Lecturers.Delete(userId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<LecturerDTO> GetAllLecturers()
        {
            var lst = unit.Lecturers.GetAll();
            List<LecturerDTO> lecturers = new List<LecturerDTO>();
            foreach (var l in lst)
                lecturers.Add(new LecturerDTO(l));

            return lecturers;
        }

        // Методы STUDENTа

        public ICollection<StudentDTO> SearchStudent(int userId = -1, string GroupNum = null)
        {
            List<StudentDTO> list = new List<StudentDTO>();
            ICollection<Student> students = unit.Students
                                        .Find(s => (userId == -1 || s.UserId == userId) &&
                                                    (GroupNum == null || s.GroupNumber == GroupNum));

            foreach (var student in students)
            {
                list.Add(new StudentDTO(student));
            }

            return list;
        }

        public ICollection<StudentDTO> SearchStudent(string login = null, string GroupNum = null)
        {
            List<StudentDTO> list = new List<StudentDTO>();
            ICollection<Student> students = unit.Students
                                        .Find(s => (login == null || s.User.Login == login) &&
                                                    (GroupNum == null || s.GroupNumber == GroupNum));

            foreach (var student in students)
            {
                list.Add(new StudentDTO(student));
            }

            return list;
        }

        public bool CreateStudent(int userId, string GroupNum = null)
        {
            Student s = new Student() {
                UserId = userId,
                GroupNumber = GroupNum
            };

            if (unit.Students.Get(s.UserId) != null)
            {
                return false;
            }

            if (GroupNum != null && unit.Groups.Get(GroupNum) == null)
            {
                s.GroupNumber = null;
            }
            
            unit.Students.Create(s);
            unit.Save();
            return true;
        }

        public bool AddStudentToGroup(int userId, string GroupNum)
        {
            Student student = unit.Students.Get(userId);
            Group group = unit.Groups.Get(GroupNum);
            if (student == null || group == null)
            {
                return false;
            }

            if (student.GroupNumber == GroupNum)
            {
                return true;
            }

            student.GroupNumber = GroupNum;
            unit.Students.Update(student);
            unit.Save();

            return true;
        }

        public bool DeleteStudentFromGroup(int userId)
        {
            Student s = unit.Students.Get(userId);
            if (s != null)
            {
                s.GroupNumber = null;
                unit.Students.Update(s);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteStudent(int userId)
        {
            if (unit.Users.Get(userId) != null && unit.Students.Get(userId) != null)
            {
                unit.Students.Delete(userId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<LecturerDiscDTO> GetLecturersNames(ICollection<int> lecturersIds)
        {
            var col = new List<LecturerDiscDTO>();
            if (lecturersIds != null)
            {
                foreach (int id in lecturersIds.Distinct())
                {
                    Lecturer l = unit.Lecturers.Get(id);
                    col.Add(l != null ? new LecturerDiscDTO() {
                        FIO = l.User.FIO,
                        Id = l.UserId
                    }
                    : null);
                }
                return col.Where(cc => cc != null).ToList();
            }
            return null;
        }

        private void SendInvitation(User u)
        {
            MailMessage mess = new MailMessage(Sender.SenderName, u.Mail);
            mess.Subject = getSubject();
            mess.Body = getBody(u);
            mess.IsBodyHtml = true;

            Sender.EnableSsl = true;
            Sender.Send(mess);
        }

        public bool SendPassword(string login, string email)
        {
            // Create new password
            string newPass = Guid.NewGuid().ToString();

            // Update password in DB
            User user = unit.Users.Find(u => u.Login == login && u.Mail == email).FirstOrDefault();

            if (user == null)
            {
                return false;
            }

            user.Password = newPass;
            unit.Users.Update(user);
            unit.Save();


            //// Send message with new password

            MailMessage mess = new MailMessage(Sender.SenderName, email);
            mess.Subject = getPassSubject();
            mess.Body = this.getPassword(login, newPass);
            mess.IsBodyHtml = true;

            Sender.EnableSsl = true;
            Sender.Send(mess);
            return true;
        }

        public void SendLogin(string login, string email)
        {
            //// Send message with new login

            MailMessage mess = new MailMessage(Sender.SenderName, email);
            mess.Subject = getLogSubject();
            mess.Body = this.getLogin(login);
            mess.IsBodyHtml = true;

            Sender.EnableSsl = true;
            Sender.Send(mess);
        }

        public void SendChangedPass(string login, string pass, string email)
        {
            //// Send message with new password

            MailMessage mess = new MailMessage(Sender.SenderName, email);
            mess.Subject = getPasswordChangeSubject();
            mess.Body = this.getPasswordChange(login, pass);
            mess.IsBodyHtml = true;

            Sender.EnableSsl = true;
            Sender.Send(mess);
        }

        public byte[] GetToken(Int32 id)
        {
            String datetime = DateTime.Now.ToUniversalTime().ToString() + id;
            // переводим строку в байт - массив
            byte[] bytes = Encoding.UTF8.GetBytes(datetime);

            //создаем объект для получения средств шифрования  
            MD5CryptoServiceProvider CSP = new MD5CryptoServiceProvider();

            //вычисляем хеш-представление в байтах  
            return CSP.ComputeHash(bytes);
        }

        public byte[] GetTokenAndSaveToDB(Int32 userId)
        {
            byte[] token = null;
            token = GetToken(userId);

            unit.Tokens.Create(new Token()
            {
                TokenValue = token,
                UserId = userId
            });
            unit.Save();

            return token;
        }

        public void DeleteTokenFromDB(byte[] token)
        {
            unit.Tokens.Delete(token);
            unit.Save();
        }

        public Int32 GetUserIdByToken(byte[] token)
        {
            Token tk = unit.Tokens.Get(token);
            if (tk != null)
            {
                return tk.UserId;
            }
            else
            {
                return -1;
            }
        }

        public String GetRoleByToken(byte[] token)
        {
            Token tk = unit.Tokens.Get(token);
            if (tk != null)
            {
                return tk.User.Type.ToString();
            }
            else
            {
                return null;
            }
        }

        public byte[] DecodeToken(String hex)
        {
            int NumberChars = hex.Length;
            byte[] token = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                token[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);

            return token;
        }

        public String GetUserGroupNameByToken(byte[] token)
        {
            return unit.Tokens.Get(token).User.Student.GroupNumber;
        }

        // Кое-кто очень полюбил JS
        private string getBody(User u)
        {
            string html = String.Format(@"<h3 style='text-align:center; color: darkblue;'> Вы были зарегистрированы на сайте <a href='http://stas-mihaylov.ru/' target='_blank'>stas-mihaylov.ru</a>! </h3> <br />
                            <h3 style='text-align:center; color: darkblue;'> Спешим заметить, что Стас Михайлов - король рока!</h3> <br />
                            <p style='text-align: center; color:black;'>Ваш логин: <i> {0} </i></p> <p style='text-align: center; color:black;'>Ваш пароль: <i> {1} </i></p>
                            <p style='text-align: center; color:black;'>Добро пожаловать!</p>", u.Login,u.Password);
            return html;
        }

        private string getPassword(string log, string pass)
        {
            string html = String.Format(@"<h3 style='text-align:center; color: darkblue;'> Вы запросили восстановление пароля на сайте <a href='http://stas-mihaylov.ru/' target='_blank'>stas-mihaylov.ru</a> !</h3> <br />
                            <p style='text-align: center; color:black;'>Ваш логин: <i> {0} </i></p> <p style='text-align:center; color:black;'>Ваш новый пароль: <i> {1} </i></p>
                            <p style='text-align: center; color:black;'>Приятной работы!</p>", log, pass);

            return html;
        }

        private string getPasswordChange(string log, string pass)
        {
            string html = String.Format(@"<h3 style='text-align:center; color: darkblue;'> Вы изменили пароль на сайте <a href='http://stas-mihaylov.ru/' target='_blank'>stas-mihaylov.ru</a> !</h3> <br />
                            <p style='text-align: center; color:black;'>Ваш логин: <i> {0} </i></p> <p style='text-align:center; color:black;'>Ваш новый пароль: <i> {1} </i></p>
                            <p style='text-align: center; color:black;'>Приятной работы!</p>", log, pass);

            return html;
        }

        private string getSubject()
        {
            return "Регистрация на сайте управления дисциплинами Мазуровой О.А";
        }

        private string getPassSubject()
        {
            return "Запрос на восстановление пароля";
        }

        private String getLogSubject()
        {
            return "Смена логина";
        }

        private String getPasswordChangeSubject()
        {
            return "Смена пароля";
        }

        private String getLogin(String log)
        {
            string html = String.Format(@"<h3 style='text-align:center; color: darkblue;'> Вы изменили логин на сайте <a href='http://stas-mihaylov.ru/' target='_blank'>stas-mihaylov.ru</a> !</h3> <br />
                            <p style='text-align: center; color:black;'>Ваш новый логин: <i> {0} /<i></p> <p style='text-align: center; color:black;'>Приятной работы!</p>", log);

            return html;
        }
    }
}
