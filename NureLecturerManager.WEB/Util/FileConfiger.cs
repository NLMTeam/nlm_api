﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace NLM.WEB.Util
{
    public class FileConfiger
    {
        private string _filePath;

        public FileConfiger(string path)
        {
            _filePath = path;
        }

        public string Year
        {
            get
            {
                string year = (string)HttpContext.Current.Session["Year"];

                if (year == null)
                {
                    this.RestoreFromFile();
                    return (string)HttpContext.Current.Session["Year"];
                }
                else
                {
                    return year;
                }
            }
            private set
            {

            }
        }

        public string Semestr
        {
            get
            {
                string semestr = (string)HttpContext.Current.Session["Semestr"];

                if (semestr == null)
                {
                    this.RestoreFromFile();
                    return (string)HttpContext.Current.Session["Semestr"];
                }
                else
                {
                    return semestr;
                }
            }
            private set
            {

            }
        }

        public void RestoreFromFile()
        {
            using (StreamReader sr = new StreamReader(_filePath))
            {
                HttpContext.Current.Session["Year"] = sr.ReadLine();
                HttpContext.Current.Session["Semester"] = sr.ReadLine();
            }
        }
    }
}