﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using NLM.WEB.Models.Lecturer;
//using NLM.BLL.DTO;
//using NLM.BLL.Interfaces;
//using NLM.BLL.Exceptions;
//using NLM.WEB.Util.Exceptions;
//using NLM.WEB.Util.Comparators;
//using NLM.WEB.Util.Helpers.Permissions;

//namespace NLM.WEB.Util.Helpers.Groups
//{
//    public class GroupHelper : IGroupHelper
//    {
//        private IAccountService accountService;
//        private IControlService controlService;
//        private IDisciplineService discService;

//        private Helpers.IGroupHelper groupHelper;
//        private IPermissionHelper permisHelper;

//        public GroupHelper(IDependencyResolver dependencyResolver)
//        {
//            accountService = dependencyResolver.GetService<IAccountService>();
//            controlService = dependencyResolver.GetService<IControlService>();
//            discService = dependencyResolver.GetService<IDisciplineService>();

//            groupHelper = dependencyResolver.GetService<Helpers.IGroupHelper>();
//            permisHelper = dependencyResolver.GetService<IPermissionHelper>();
//        }

//        public void ExecuteControlChanges(KeyValuePair<GroupDTO, IEnumerable<GroupControlDTO>> data)
//        {
//            var group = data.Key;
//            var controls = new List<GroupControlDTO>(data.Value);
//            var notFound = new List<GroupControlDTO>();

//            foreach (var control in controlService.SearchGroupControls(GroupNum: group.GroupId))
//            {
//                if (controls.RemoveAll(gc => gc.GroupControlId == control.GroupControlId) == 0)
//                {
//                    notFound.Add(control);
//                }
//            }
//            foreach (var control in controls)
//            {
//                // Error

//                //controlService.CreateGroupControl(control.WorkTypeId, control.GroupNumber, control.PassDate.Value, control.Type, control.PassPlace);
//            }

//            foreach (var control in notFound)
//            {
//                controlService.DeleteGroupControl(control.GroupControlId);
//            }
//        }

//        public void ExecuteGroupChanges(IEnumerable<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>> groupStudents)
//        {
//            foreach (var pair in groupStudents)
//            {
//                // find new and deleted students

//                GroupDTO group = null;
//                try
//                {
//                    group = accountService.SearchGroup(pair.Key.GroupId).SingleOrDefault();
//                }
//                catch (NotFoundException)
//                {
//                    throw new ExcelDataException("Группа " + pair.Key.GroupId + "не найдена в базе.");
//                }

//                var students = new List<UserDTO>(accountService.GetStudentsOfGroup(group.GroupId).Select(s => accountService.SearchUser(s.UserId).SingleOrDefault()));

//                var notFoundStudents = new List<UserDTO>();

//                foreach (var student in pair.Value)
//                {
//                    var user = students.Find(u => u.FIO == student.FIO && u.Mail == student.Mail);
//                    if (user != null)
//                    {
//                        students.Remove(user);
//                    }
//                    else
//                    {
//                        notFoundStudents.Add(student);
//                    }
//                }

//                // delete all students that doesn't exists in file

//                foreach (var student in students)
//                {
//                    try
//                    {
//                        accountService.DeleteStudent(student.UserId);
//                        accountService.DeleteUser(student.UserId);
//                    }
//                    catch (NotFoundException)
//                    {

//                    }
//                }

//                // create every new student

//                foreach (var student in notFoundStudents)
//                {
//                    var login = Guid.NewGuid().ToString().Substring(0, 8);
//                    var password = Guid.NewGuid().ToString().Substring(0, 8);

//                    accountService.Register(student.FIO, login, password, "Студент", student.Mail, "");

//                    var user = accountService.SearchUser(FIO: student.FIO, Login: login, Password: password, Email: student.Mail).LastOrDefault();

//                    accountService.CreateStudent(user.UserId, group.GroupId);
//                }
//            }
//        }

//        public IEnumerable<GroupControlViewModel> GetAllControls(int year)
//        {
//            // get all groups -> get all group controls (distinct)

//            // all groups for current year
//            var groups = accountService.SearchGroup().Where(g => groupHelper.CheckGroupForYear(g.GroupId, year));

//            // get all group controls and distinct them
//            var groupControls = controlService.SearchGroupControls().Where(gc => groups.Contains(new GroupDTO()
//            {
//                GroupId = gc.GroupNumber
//            }, new GroupEqualityComparer())).Distinct(new GroupControlComparer());

//            // transform to GroupControlViewModel before returning
//            return groupControls.Select(gc => new GroupControlViewModel()
//            {
//                GroupControlId = gc.GroupControlId,
//                FormatDate = gc.PassDate.Value.ToShortDateString(),
//                PassDate = gc.PassDate,
//                Place = gc.PassPlace,
//                Type = gc.Type,
//                WorkType = controlService.SearchWorkType(gc.WorkTypeId).Single().Name
//            });
//        }

//        public IEnumerable<GroupControlViewModel> GetGroupControls(GroupDTO group)
//        {
//            var groupControls = controlService.SearchGroupControls(GroupNum: group.GroupId).Select(gc => new GroupControlViewModel()
//            {
//                GroupControlId = gc.GroupControlId,
//                PassDate = gc.PassDate,
//                Place = gc.PassPlace,
//                FormatDate = gc.PassDate.Value.ToShortDateString(),
//                Type = gc.Type,
//                WorkType = controlService.SearchWorkType(WorkTypeId: gc.WorkTypeId).FirstOrDefault()?.Name,
//            });
//            return groupControls;
//        }

//        public IEnumerable<UserDTO> GetGroupCreateFillModel()
//        {
//            List<UserDTO> users = new List<UserDTO>();
//            var allUsers = accountService.SearchUser();

//            foreach (var user in allUsers)
//            {
//                if (user.Type != "Студент")
//                    continue;

//                StudentDTO student = new StudentDTO();

//                try
//                {
//                    student = accountService.SearchStudent(user.UserId).SingleOrDefault();
//                }
//                catch (NotFoundException)
//                {
//                    users.Add(user);
//                }

//                if (student.GroupId == null)
//                {
//                    users.Add(user);
//                }
//            }

//            return users;
//        }

//        public GroupEditViewModel GetGroupEditFillModel(string groupId)
//        {
//            GroupEditViewModel model = new GroupEditViewModel();

//            GroupDTO group = accountService.SearchGroup(groupId).FirstOrDefault();
//            // searching group students
//            IEnumerable<StudentDTO> students = accountService.SearchStudent(GroupNum: groupId);

//            List<UserDTO> users = new List<UserDTO>();

//            // taking user dto objects for students
//            foreach (var student in students)
//            {
//                try
//                {
//                    users.Add(accountService.SearchUser(userId: student.UserId).First());
//                }
//                catch (NotFoundException)
//                {
//                    // must be some error log
//                }
//            }

//            UserDTO captain = null;

//            // searching for captain
//            if (group.CaptainId.HasValue && group.CaptainId.Value > 0)
//            {
//                try
//                {
//                    captain = accountService.SearchUser(group.CaptainId.Value).SingleOrDefault();
//                }
//                catch (NotFoundException)
//                {
//                    captain = null;
//                }
//            }

//            // place to model
//            model.Students = users;
//            model.Captain = captain;

//            return model;
//        }

//        public IEnumerable<StudentViewModel> GetGroupStudents(GroupDTO group)
//        {
//            var students = groupHelper.GetGroupStudents(group.GroupId);

//            return students.Select(s => new StudentViewModel()
//            {
//                UserId = s.UserId,
//                FIO = s.FIO,
//                Email = s.Mail,
//                IsCaptain = s.UserId == group.CaptainId.Value
//            });
//        }

//        public GroupMainViewModel GetModel()
//        {
//            GroupMainViewModel model = new GroupMainViewModel();

//            // get years using special method
//            model.Years = groupHelper.GetYears();

//            // select first as default year
//            var year = model.Years.First();

//            // search groups in db
//            model.Groups = groupHelper.GetYearGroups(Convert.ToInt32(year)).Select(s => accountService.SearchGroup(s).Single());

//            // select default first group
//            var group = model.Groups.First();

//            // using special method for this act
//            model.GroupStudents = this.GetGroupStudents(group);

//            // using special method to get group controls
//            model.GroupControls = this.GetGroupControls(group);

//            return model;
//        }

//        public GroupMainViewModel GetStudentModel(StudentDTO student)
//        {
//            GroupMainViewModel model = new GroupMainViewModel();

//            // get student group
//            var group = accountService.SearchGroup(GroupNum: student.GroupId).First();

//            // create year list to place step by step there years
//            var yearList = new List<string>();

//            // firstly place student group year
//            var year = groupHelper.GetYearOfGroup(group.GroupId);
//            yearList.Add(year);

//            // then all other years from db
//            yearList.AddRange(groupHelper.GetYears().Where(s => s != year));

//            model.Years = yearList;

//            // now let's take form by the same rules groups
//            // create group List
//            var groups = new List<GroupDTO>();
            
//            // place there student group
//            groups.Add(group);

//            // take all other groups from db
//            groups.AddRange(groupHelper.GetYearGroups(Convert.ToInt32(year)).Where(g => g != group.GroupId).Select(s => accountService.SearchGroup(s).First()));

//            // place the result to model
//            model.Groups = groups;

//            // get group students using special method
//            model.GroupStudents = this.GetGroupStudents(group);

//            // get group controls using special method of this object
//            model.GroupControls = this.GetGroupControls(group);

//            return model;
//        }

//        public IEnumerable<GroupDTO> GetYearGroups(int year)
//        {
//            return groupHelper.GetYearGroups(year).Select(s => accountService.SearchGroup(s).First());
//        }

//        public IEnumerable<string> GetYears()
//        {
//            return groupHelper.GetYears();
//        }
//    }
//}