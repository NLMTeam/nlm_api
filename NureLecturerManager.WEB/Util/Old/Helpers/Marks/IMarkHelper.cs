﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Helpers.Marks
{
    public interface IMarkHelper
    {
        MarksMainViewModel GetStudentModel(StudentDTO student);
        MarksMainViewModel GetModel();
        IEnumerable<string> GetYears();
        IEnumerable<string> GetYearGroups(int year);
        IEnumerable<DisciplineDTO> GetGroupDisciplines(String year, string groupNum);
        MarksViewModel GetStudentsMarks(string disciplineName, string groupNum, string year);
        void UpdateDataBase(KeyValuePair<IEnumerable<Category>, IEnumerable<StudentMarkViewModel>> model);
    }
}
