﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Mvc;
//using NLM.BLL.DTO;
//using NLM.BLL.Interfaces;
//using NLM.WEB.Models.Lecturer;
//using NLM.BLL.Exceptions;
//using System.Web;
//using NLM.WEB.Util.Exceptions;

//namespace NLM.WEB.Util.Helpers.Marks
//{
//    public class MarksHelper : IMarkHelper
//    {
//        private IControlPointService controlPointService;
//        private IControlService controlService;
//        private IAccountService accountService;
//        private IDisciplineService discService;

//        private IGroupHelper groupHelper;

//        public MarksHelper(IDependencyResolver resolver)
//        {
//            controlPointService = resolver.GetService<IControlPointService>();
//            controlService = resolver.GetService<IControlService>();
//            accountService = resolver.GetService<IAccountService>();
//            discService = resolver.GetService<IDisciplineService>();

//            groupHelper = resolver.GetService<IGroupHelper>();
//        }

//        private MarksViewModel getStudentMarks(string defaultGroup, DisciplineDTO defaultDiscipline, string year)
//        {
//            MarksViewModel model = new MarksViewModel();
//            List<Category> markCategorys = new List<Category>();
//            List<StudentMarkViewModel> marks = new List<StudentMarkViewModel>();

//            try
//            {
//                List<StudentDTO> students = new List<StudentDTO>(accountService.GetStudentsOfGroup(defaultGroup));

//                foreach (var student in students)
//                {
//                    StudentMarkViewModel studModel = new StudentMarkViewModel();
//                    var user = accountService.SearchUser(userId: student.UserId).First();

//                    studModel.FIO = user.FIO;
//                    studModel.UserId = user.UserId;

//                    marks.Add(studModel);
//                }
            
//                var disciplineForYear = discService.SearchDisciplineForYear(discId: defaultDiscipline.DisciplineId, year: year).First();

//                var defaultDisciplinePart = discService.SearchPartOfDiscipline(disciplineYId: disciplineForYear.DisciplineForYearId, GroupNum: defaultGroup).First();

//                var controlPoints = controlPointService.SearchControlPoints(discYId: disciplineForYear.DisciplineForYearId);

//                foreach (var controlPoint in controlPoints)
//                {
//                    foreach (var workType in controlService.GetWorkTypesOfControlPoint(controlPoint.Name))
//                    {
//                        markCategorys.Add(new Category(workType));

//                        try
//                        {
//                            var groupControl = controlService.SearchGroupControls(workTypeId: workType.WorkTypeId,
//                                                GroupNum: defaultGroup).First();

//                            for (int i = 0; i < students.Count; i++)
//                            {
//                                StudentControlDTO studentControl;
//                                try
//                                {
//                                    studentControl = controlService.SearchStudentcontrols(userId: students[i].UserId,
//                                        GroupControlId: groupControl.GroupControlId).First();
//                                }
//                                catch (NotFoundException)
//                                {
//                                    if (marks[i].Marks == null) marks[i].Marks = new Dictionary<Category, string>();
//                                    marks[i].Marks.Add(new Category(workType), "");
//                                    continue;
//                                }
//                                if (marks[i].Marks == null) marks[i].Marks = new Dictionary<Category, string>();
//                                marks[i].Marks.Add(new Category(workType), studentControl.Mark.ToString());
//                            }
//                        }
//                        catch (NotFoundException)
//                        {
//                            continue;
//                        }
//                    }

//                    markCategorys.Add(new Category(controlPoint));

//                    for (int i = 0; i < students.Count; i++)
//                    {
//                        ControlPointMarkDTO studentMark;
//                        try
//                        {
//                            studentMark = controlPointService.SearchControlPointMarks(userId: students[i].UserId,
//                                                   CpId: controlPoint.ControlPointId).First();
//                        }
//                        catch (NotFoundException)
//                        {
//                            if (marks[i].Marks == null) marks[i].Marks = new Dictionary<Category, string>();
//                            marks[i].Marks.Add(new Category(controlPoint), "");
//                            continue;
//                        }
//                        string mark = (!String.IsNullOrEmpty(studentMark.FinalMark)) ? studentMark.FinalMark : studentMark.Mark.ToString();
//                        if (marks[i].Marks == null) marks[i].Marks = new Dictionary<Category, string>();
//                        marks[i].Marks.Add(new Category(controlPoint), mark);
//                    }

//                }
//            }
//            catch (NotFoundException)
//            {
//                model.Categorys = markCategorys;
//                model.Marks = marks;
//                return model;
//            }

//            model.Categorys = markCategorys;
//            model.Marks = marks;

//            return model;
//        }

//        public IEnumerable<DisciplineDTO> GetGroupDisciplines(string year, string groupNum)
//        {
//            return groupHelper.GetGroupDisciplines(year, groupNum);
//            throw new NotImplementedException();
//        }

//        public MarksMainViewModel GetModel()
//        {
//            MarksMainViewModel model = new MarksMainViewModel();

//            // get disciplines for years and select from them years
//            var disciplineForYears = discService.SearchDisciplineForYear().AsEnumerable();
//            model.Years = disciplineForYears.Select(discYear => discYear.Year);

//            // get default year from config file
//            var config = new FileConfiger(HttpContext.Current.Server.MapPath("~/Files/Config.txt"));
//            string year = config.Year.ToString();

//            // chose defaulr discipline for year
//            var discForYear = disciplineForYears.Where(discYear => discYear.Year == year).First();

//            // taking discipline parts and selecting groupId from them
//            var discParts = discService.SearchPartOfDiscipline(discForYear.DisciplineForYearId);
//            var groups = discParts.Select(part => part.GroupId);
//            model.Groups = groups;

//            // select first (default) group
//            var group = model.Groups.First();

//            // select disciplines using stadart method from shared group helper
//            var disciplines = groupHelper.GetGroupDisciplines(year, group);

//            // select first (default) discipline
//            var discipline = disciplines.First();

//            // and now get marks using private method in this class
//            var marks = this.getStudentMarks(group, discipline, year);

//            // place marks to model
//            model.Worktypes = marks.Categorys;
//            model.Marks = marks.Marks;

//            return model;
//        }

//        public MarksMainViewModel GetStudentModel(StudentDTO student)
//        {
//            MarksMainViewModel model = new MarksMainViewModel();
//            var studGroup = accountService.SearchGroup(student.GroupId).First();
//            var discParts = discService.SearchPartOfDiscipline(GroupNum: studGroup.GroupId).AsEnumerable();

//            // get disciplines for years and select first
//            var disciplinesForYear = discService.SearchDisciplineForYear();
//            var years = disciplinesForYear.Select(part => part.Year).Distinct().OrderBy(s => s);

//            // must be chaged algorythm for chosing default year. must be careful use of config file
//            // must  be mentioned that it's student whose can have 0 disciplines for year in the current year
//            // from config file

//            var yearList = new List<string>();

//            string defaultYear = null;
//            foreach (var year in years)
//            {
//                if (discParts.Where(part => part.Year == year).Count() > 0)
//                {
//                    defaultYear = year;
//                    break;
//                }
//            }

//            yearList.Add(defaultYear);
//            yearList.AddRange(years.Where(y => y != defaultYear));

//            model.Years = yearList;

//            // filter discipline Parts for this year
//            discParts = discParts.Where(part => part.Year == defaultYear);

//            // select disciplines and place them to model 
//            var disciplines = discParts.Select(part => discService.SearchDiscipline(part.DisciplineId).Single());

//            // select first default discipline
//            var defaultDiscipline = disciplines.First();

//            model.Disciplines = disciplines;

//            // select groups and place student group at first default place
//            var groups = new List<string>();

//            groups.Add(studGroup.GroupId);
//            groups.AddRange(discService.SearchPartOfDiscipline(Year: defaultYear).Select(part => part.GroupId).Where(s => s != studGroup.GroupId).Distinct());

//            model.Groups = groups;

//            // using special method to get marks and worktypes
//            var marks = getStudentMarks(studGroup.GroupId, defaultDiscipline, defaultYear);

//            model.Worktypes = marks.Categorys;
//            model.Marks = marks.Marks;

//            return model;
//        }

//        public MarksViewModel GetStudentsMarks(string disciplineName, string groupNum, string year)
//        {
//            var discipline = discService.SearchDiscipline(Name: disciplineName).First();
//            return this.getStudentMarks(groupNum, discipline, year);
//        }

//        public IEnumerable<string> GetYearGroups(int year)
//        {
//            return groupHelper.GetYearGroups(year);
//        }

//        public IEnumerable<string> GetYears()
//        {
//            return groupHelper.GetYears();
//        }

//        public void UpdateDataBase(KeyValuePair<IEnumerable<Category>, IEnumerable<StudentMarkViewModel>> model)
//        {
//            // Update DB using marks object

//            var categorys = new List<Category>(model.Key);
//            var studentMarks = new List<StudentMarkViewModel>(model.Value);

//            foreach (var studentMark in studentMarks)
//            {
//                var student = accountService.SearchStudent(studentMark.UserId).SingleOrDefault();
//                foreach (var category in categorys)
//                {
//                    KeyValuePair<Category, string> mark = new KeyValuePair<Category, string>();
//                    try
//                    {
//                        mark = studentMark.Marks.First(pair => pair.Key == category);
//                        if (mark.Value == "")
//                        {
//                            throw new InvalidOperationException();
//                        }
//                    }
//                    catch (InvalidOperationException)
//                    {
//                        // delete if it is not exists in file
//                        if (category.ControlPoint == null)
//                        {
//                            GroupControlDTO groupControl = null;
//                            try
//                            {
//                                groupControl = controlService.SearchGroupControls(workTypeId: category.WorkType.WorkTypeId, GroupNum: student.GroupId).SingleOrDefault();
//                            }
//                            catch (NotFoundException)
//                            {
//                                continue;
//                            }

//                            try
//                            {
//                                var studentControl = controlService.SearchStudentcontrols(userId: student.UserId, GroupControlId: groupControl.GroupControlId).SingleOrDefault();

//                                controlService.DeleteMark(studentControl.StudentControlId);
//                            }
//                            catch (NotFoundException)
//                            {
//                                continue;
//                            }
//                        }
//                        else
//                        {
//                            try
//                            {
//                                var cpMark = controlPointService.SearchControlPointMarks(student.UserId, CpId: category.ControlPoint.ControlPointId).SingleOrDefault();

//                                controlPointService.DeleteMark(cpMark.ControlPointId, cpMark.UserId, cpMark.PassDate);
//                            }
//                            catch (NotFoundException)
//                            {
//                                continue;
//                            }

//                        }
//                        continue;
//                    }
//                    if (mark.Key.ControlPoint == null)
//                    {
//                        var groupControl = controlService.SearchGroupControls(workTypeId: mark.Key.WorkType.WorkTypeId, GroupNum: student.GroupId).SingleOrDefault();
//                        try
//                        {
//                            var studentControl = controlService.SearchStudentcontrols(userId: student.UserId, GroupControlId: groupControl.GroupControlId).SingleOrDefault();

//                            var intMarkValue = Convert.ToInt32(mark.Value);
//                            if ((!studentControl.Mark.HasValue) || studentControl.Mark.Value != intMarkValue)
//                            {
//                                controlService.SetMark(groupControl.GroupControlId, student.UserId, intMarkValue);
//                            }
//                        }
//                        catch (FormatException)
//                        {
//                            throw new ExcelDataException(mark.Value + " не является числом.");
//                        }
//                        catch (NotFoundException)
//                        {
//                            int intMarkValue = 0;
//                            try
//                            {
//                                intMarkValue = Convert.ToInt32(mark.Value);
//                            }
//                            catch (FormatException)
//                            {
//                                throw new ExcelDataException(mark.Value + " не является числом.");
//                            }
//                            controlService.SetMark(groupControl.GroupControlId, student.UserId, intMarkValue);
//                        }
//                    }
//                    else
//                    {
//                        try
//                        {
//                            var controlpointMark = controlPointService.SearchControlPointMarks(student.UserId, CpId: mark.Key.ControlPoint.ControlPointId).SingleOrDefault();
//                            var intMarkValue = Convert.ToInt32(mark.Value);

//                            if (controlpointMark.Mark != intMarkValue)
//                            {
//                                controlPointService.SetMark(student.UserId, intMarkValue, mark.Key.ControlPoint.ControlPointId);
//                            }
//                        }
//                        catch (FormatException)
//                        {
//                            throw new ExcelDataException(mark.Value + " не является числом.");
//                        }
//                        catch (NotFoundException)
//                        {
//                            int intMarkValue = 0;
//                            try
//                            {
//                                intMarkValue = Convert.ToInt32(mark.Value);
//                            }
//                            catch (FormatException)
//                            {
//                                throw new ExcelDataException(mark.Value + " не является числом.");
//                            }
//                            controlPointService.SetMark(student.UserId, intMarkValue, mark.Key.ControlPoint.ControlPointId);
//                        }
//                    }
//                }

//            }
//        }
//    }
//}