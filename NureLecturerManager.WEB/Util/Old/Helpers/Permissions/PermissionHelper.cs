﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Mvc;
//using NLM.BLL.DTO;
//using NLM.BLL.Interfaces;
//using NLM.BLL.Exceptions;
//using System.Web;

//namespace NLM.WEB.Util.Helpers.Permissions
//{
//    public class PermissionHelper : IPermissionHelper
//    {
//        private IAccountService accountService;
//        private ICourseProjectService cpService;
//        private IDisciplineService discService;
//        private IControlService controlService;

//        private const string ADMIN_TYPE = "ADMIN";
//        private const string LECTURER_TYPE = "LECTURER";
//        private const string STUDENT_TYPE = "STUDENT";

//        // configers
//        private FileConfiger configer;

//        public PermissionHelper(IDependencyResolver dependecyResolver)
//        {
//            accountService = dependecyResolver.GetService<IAccountService>();
//            cpService = dependecyResolver.GetService<ICourseProjectService>();
//            discService = dependecyResolver.GetService<IDisciplineService>();
//            controlService = dependecyResolver.GetService<IControlService>();

//            configer = new FileConfiger(HttpContext.Current.Server.MapPath("/Files/Config.txt"));
//        }

//        public bool AllowConsolidationEdit(string year, int courseProjectId, string groupId, UserDTO user)
//        {
//            // firstly everything clear if it's student or admin
//            if (user.Type == STUDENT_TYPE)
//            {
//                return false;
//            }

//            if (user.Type == ADMIN_TYPE)
//            {
//                return true;
//            }

//            // now it's lector
//            // take course project -> discipline -> for year -> check userId
//            CourseProjectDTO courseProject = cpService.SearchCourseProjects(courseProjectId: courseProjectId).First();

//            // discipline
//            var discipline = discService.SearchDiscipline(courseProject.DisciplineId).First();

//            // Discipline for year
//            var disciplineForYear = discService.SearchDisciplineForYear(discId: discipline.DisciplineId, year: year).First();

//            // Part of discipline
//            var part = discService.SearchPartOfDiscipline(disciplineForYear.DisciplineForYearId, groupId).First();
//            if (disciplineForYear.LecturerIds.Contains(user.UserId))
//            {
//                return true;
//            }
//            return part.LecturerIds.Contains(user.UserId);
//        }

//        public bool AllowConsolidationEdit(string year, string disciplineName, UserDTO user)
//        {
//            // everything clear if it's student or admin
//            if (user.Type == STUDENT_TYPE)
//            {
//                return false;
//            }
//            if (user.Type == ADMIN_TYPE)
//            {
//                return true;
//            }

//            // take discipline by name -> discipline for year (year + DisciplineId) -> check lecturerIds for containing userId
//            var discipline = discService.SearchDiscipline(Name: disciplineName).First();

//            var disciplineForYear = discService.SearchDisciplineForYear(year: year).First();

//            return disciplineForYear.LecturerIds.Contains(user.UserId);
//        }

//        public bool AllowConsolidationDelete(int consolidationId, UserDTO user)
//        {
//            var consolidation = cpService.SearchConsolidation(consolidationId).First();

//            var discForYear = discService.SearchDisciplineForYear(consolidation.DisciplineForYearId).First();

//            if (discForYear.LecturerIds.Contains(user.UserId))
//            {
//                return true;
//            }

//            // check for assistant if he can edit student 
//            var student = accountService.SearchStudent(consolidation.UserId).First();

//            var discPart = discService.SearchPartOfDiscipline(discForYear.DisciplineForYearId, student.GroupId).First();

//            return discPart.LecturerIds.Contains(user.UserId);
//        }

//        public bool AllowProjectEdit(string year, string disciplineName, UserDTO user)
//        {
//            return this.AllowConsolidationEdit(year, disciplineName, user);
//        }

//        public bool AllowProjectDelete(int courseProjectId, UserDTO user)
//        {
//            var courseProject = cpService.SearchCourseProjects(courseProjectId: courseProjectId).First();

//            var discipline = discService.SearchDiscipline(courseProject.DisciplineId).First();

//            var disciplineForYear = discService.SearchDisciplineForYear(discId: discipline.DisciplineId, year: configer.Year, semester: configer.Semestr != null ? Convert.ToInt32(configer.Semestr) : -1).First();

//            if (disciplineForYear.LecturerIds.Contains(user.UserId))
//            {
//                return true;
//            }
//            var parts = discService.SearchPartOfDiscipline(disciplineForYear.DisciplineForYearId)
//                                    .Where(part => part.LecturerIds.Contains(user.UserId));

//            return parts.Count() > 0;
//        }

//        public bool AllowGroupControlEdit(int groupControlId, UserDTO user)
//        {
//            // everything clear if it's student or admin
//            if (user.Type == STUDENT_TYPE)
//            {
//                return false;
//            }
//            if (user.Type == ADMIN_TYPE)
//            {
//                return true;
//            }

//            // take group control -> discipline part -> discipline for year
//            var groupControl = controlService.SearchGroupControls(groupControlId).First();

//            // take discipline part dto 
//            var discPart = discService.SearchPartOfDiscipline(partId: groupControl.PartId.Value).First();

//            // take discipline for year dto
//            var discForYear = discService.SearchDisciplineForYear(discPart.DisciplineForYearId).First();

//            return discForYear.LecturerIds.Contains(user.UserId);
//        }

//        public bool AllowMarksEdit(string year, string disciplineName, UserDTO user)
//        {
//            // everything clear if it's student or admin
//            if (user.Type == STUDENT_TYPE)
//            {
//                return false;
//            }
//            if (user.Type == ADMIN_TYPE)
//            {
//                return true;
//            }
//            // discipline -> discipline for year -> check user id

//            var discipline = discService.SearchDiscipline(Name: disciplineName).First();
//            var disciplineForYear = discService.SearchDisciplineForYear(discId: discipline.DisciplineId, year: year).First();

//            return disciplineForYear.LecturerIds.Contains(user.UserId);
//        }
//    }
//}