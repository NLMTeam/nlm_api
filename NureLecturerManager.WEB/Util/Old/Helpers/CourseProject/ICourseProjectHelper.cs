﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.DTO;
using NLM.WEB.Models.CourseProjects;
using NLM.WEB.Models;

namespace NLM.WEB.Util.Helpers.CourseProject
{
    public interface ICourseProjectHelper
    {
        // initial data models
        CourseProjectMainViewModel GetModel(UserDTO user);
        CourseProjectMainStudentModel GetStudentModel(StudentDTO student);
        CourseProjectMainViewModel GetArchievedModel(UserDTO lecturer);

        // collections that loads during filtering data
        DisciplineDataModel GetDisciplineInfo(int disciplineId);
        ICollection<string> GetGroupsOfYear(string year, int disciplineId);
        ICollection<CourseProjectDTO> GetCourseProjectsForDiscipline(int disciplineId);
        string GetStudentYear(int disciplineId, UserDTO user);
        ConsolidationDTO GetStudentConsolidation(int disciplineId, UserDTO user);

        // model with wide information about project
        CourseProjectViewModel GetProjectViewModel(int courseProjectId);

        // marks of course project
        ICollection<ConsolidationViewModel> GetMarksForDisciplineAndGroup(string year, string disciplineName, string groupNum);
        ICollection<ConsolidationViewModel> GetMarksOfCourseProject(int courseProjectId, string year);

        // consolidation create | edit | delete
        InitialFillConsolidationViewModel GetConsolidationFillModel(string year);
        InitialFillConsolidationViewModel GetYearInfo(string year);
        CreateConsolidationViewModel GetConsolidationDataModel(int consolidationId);

        // courseProjects create | edit | delete
        InitialCourseProjectFill GetCourseProjectFillModel();
        CreateCourseProjectViewModel GetCourseProjectDataModel(int courseProjectId);
        ICollection<UserDTO> GetStudentsOfGroup(string groupId); 
    }
}
