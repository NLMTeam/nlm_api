﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Util.Exceptions
{
    public class ExcelFormatException : Exception
    {
        public ExcelFormatException() : base() { }

        public ExcelFormatException(string message) : base(message) { }
    }
}