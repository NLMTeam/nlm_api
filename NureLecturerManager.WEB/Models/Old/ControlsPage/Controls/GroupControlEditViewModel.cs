﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.ControlsPage.Controls
{ 
    public class GroupControlEditViewModel
    {
        // Id
        public Int32 GroupControlId { get; set; }

        // Editable properites
        public int WorkTypeId { get; set; }
        public Nullable<System.DateTime> PassDate { get; set; }
        public string Type { get; set; }
        public string PassPlace { get; set; }

        // Info properites
        public int DisciplineId { get; set; }
        public String Year { get; set; }
        public Int32 Semester { get; set; }
    }
}
