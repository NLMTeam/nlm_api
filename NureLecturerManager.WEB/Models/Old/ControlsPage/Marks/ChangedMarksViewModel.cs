﻿using NLM.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.ControlsPage.Marks
{
    public class ChangedMarksViewModel
    {
        public Int32 DFYId { get; set; }
        public ICollection<ControlPointMarkDTO> ControlPointMarks { get; set; }
        public ICollection<StudentControlDTO> StudentControlsMarks { get; set; }
    }
}
