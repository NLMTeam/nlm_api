﻿using System.Collections.Generic;
using NLM.BLL.DTO;
using NLM.BLL.DTObjects;

namespace NLM.WEB.Models.Lecturer
{
    public class LiteratureMainViewModel
    {
        public LiteratureMainViewModel()
        {
            Disciplines = new List<DisciplineDTO>();
            Literature = new Dictionary<string, IEnumerable<LiteratureDTO>>();
            Materials = new Dictionary<string, IEnumerable<SharedMaterialDTO>>();
        }

        public IEnumerable<DisciplineDTO> Disciplines { get; set; }

        // Key is disciplineId
        public Dictionary<string, IEnumerable<LiteratureDTO>> Literature { get; set; }

        //// Key is worktypeId
        public Dictionary<string, IEnumerable<SharedMaterialDTO>> Materials { get; set; }
    }
}