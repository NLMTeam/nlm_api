﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;


namespace NLM.WEB.Models.Lecturer
{
    public class MarksMainViewModel
    {
        public ICollection<string> Years { get; set; }
        public ICollection<string> Groups { get; set; }
        public ICollection<DisciplineDTO> Disciplines { get; set; } 

        public ICollection<Category> Worktypes { get; set; }
        public ICollection<StudentMarkViewModel> Marks { get; set; }
    }

    public class MarksEqualityComparer : IEqualityComparer<DisciplineDTO>
    {
        public bool Equals(DisciplineDTO x, DisciplineDTO y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(DisciplineDTO obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}