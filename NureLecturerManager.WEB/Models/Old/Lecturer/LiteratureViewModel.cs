﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class LiteratureViewModel
    {
        public int DisciplineId { get; set; }
        public string Name { get; set; }
    }
}