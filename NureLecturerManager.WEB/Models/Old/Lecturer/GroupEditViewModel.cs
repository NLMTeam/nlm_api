﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class GroupEditViewModel
    {
        public ICollection<UserDTO> Students { get; set; }
        public UserDTO Captain { get; set; }
    }
}