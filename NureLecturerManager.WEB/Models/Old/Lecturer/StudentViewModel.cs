﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class StudentViewModel
    {
        public int UserId { get; set; }
        public string FIO { get; set; }
        public string Email { get; set; }
        public bool IsCaptain { get; set; }
    }
}