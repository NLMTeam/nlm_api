﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class MarksViewModel
    {
        public ICollection<Category> Categorys { get; set; }
        public ICollection<StudentMarkViewModel> Marks { get; set; }
        public bool AllowEdit { get; set; }
    }
}