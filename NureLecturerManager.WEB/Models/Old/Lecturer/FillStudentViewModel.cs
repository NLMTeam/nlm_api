﻿using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class FillStudentViewModel
    {
        public IEnumerable<UserDTO> Users { get; set; }
        public IEnumerable<GroupDTO> Groups { get; set; }
    }
}