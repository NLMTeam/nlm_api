﻿using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class WorkTypeMaterialNewViewModel
    {
        public List<WorkTypeDTO> WorkTypes { get; set; }
    }
}