﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class CreateLecturerViewModel
    {
        public string FIO { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Post { get; set; }
        public String Specialization { get; set; }
    }
}
