﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class DisciplineViewModel
    {
        public ICollection<DisciplineDTO> Disciplines { get; set; }

        // Key is disciplineId
        public Dictionary<string, ICollection<DisciplineForYearsStudentViewModel>> DFYs { get; set; }

        // Key is disciplineForYearId
        public Dictionary<string, ICollection<ControlPointDTO>> ControlPoints { get; set; }
        
        // Key is controlPointId
        public Dictionary<string, ICollection<WorkTypeDTO>> WorkTypes { get; set; }
    }
}