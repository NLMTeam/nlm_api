﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class DisciplineLitLectViewModel
    {
        public bool IsAdmin { get; set; }
        public int DisciplineId { get; set; }
        public string Name { get; set; }
        public bool IsCourseProject { get; set; }
    }
}