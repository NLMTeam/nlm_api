﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class EditCourseProjectViewModel : CreateCourseProjectViewModel
    {
        public int CourseProjectId { get; set; }
    }
}