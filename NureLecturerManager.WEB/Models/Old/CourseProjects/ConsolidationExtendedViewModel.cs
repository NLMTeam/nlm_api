﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class ConsolidationExtendedViewModel
    {
        public ICollection<ConsolidationViewModel> Consolidations { get; set; }
        public bool AllowEdit { get; set; }

        public ConsolidationExtendedViewModel()
        {
        }

        public ConsolidationExtendedViewModel(ICollection<ConsolidationViewModel> consolidations, bool allowEdit)
        {
            this.Consolidations = consolidations;
            this.AllowEdit = allowEdit;
        }
    }
}