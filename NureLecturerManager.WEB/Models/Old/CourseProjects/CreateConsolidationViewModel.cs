﻿using System.Collections.Generic;
using NLM.BLL.DTO;
using System;

namespace NLM.WEB.Models.Lecturer
{
    public class CreateConsolidationViewModel
    {
        public int UserId { get; set; }
        public int CourseProjectId { get; set; }
        public string GroupId { get; set; }
        public int DisciplineId { get; set; }
        public string Year { get; set; }
        public string PassDate { get; set; }
        public int Mark { get; set; }
    }

    public class InitialFillConsolidationViewModel
    {
        public ICollection<CourseProjectDTO> CourseProjects { get; set; }
        public ICollection<string> Groups { get; set; }
        public ICollection<DisciplineDTO> Disciplines { get; set; }
        public ICollection<string> Years { get; set; }
        public ICollection<UserDTO> Students { get; set; }
    }
}