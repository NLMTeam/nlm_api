﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class CourseProjectExtendedModel
    {
        public ICollection<CourseProjectDTO> CourseProjects { get; set; }
        public bool AllowEdit { get; set; }

        public CourseProjectExtendedModel()
        {

        }

        public CourseProjectExtendedModel(ICollection<CourseProjectDTO> courseProjects, bool allowEdit)
        {
            this.CourseProjects = courseProjects;
            this.AllowEdit = allowEdit;
        }
    }
}