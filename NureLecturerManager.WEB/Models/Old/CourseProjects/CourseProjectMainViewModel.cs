﻿using NLM.BLL.DTO;
using System.Collections.Generic;

namespace NLM.WEB.Models.Lecturer
{
    public class CourseProjectMainViewModel
    {
        public ICollection<DisciplineDTO> Disciplines { get; set; }
        public ICollection<CourseProjectDTO> CourseProjects { get; set; }

        public ICollection<string> Years { get; set; }
        public ICollection<string> Groups { get; set; }

        public ICollection<ConsolidationViewModel> Consolidations { get; set; }
    }
}