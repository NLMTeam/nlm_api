﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;
using NLM.WEB.Models.Lecturer;

namespace NLM.WEB.Models.CourseProjects
{
    public class DisciplineDataModel
    {
        public ICollection<CourseProjectDTO> CourseProjects { get; set; }
        public ICollection<string> Years { get; set; }

    }
}