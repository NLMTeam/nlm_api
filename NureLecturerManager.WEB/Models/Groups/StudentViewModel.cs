﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Groups
{
    public class StudentViewModel
    {
        public int UserId { get; set; }
        public string Mail { get; set; }
        public string Login { get; set; }
        public string FIO { get; set; }
    }
}
