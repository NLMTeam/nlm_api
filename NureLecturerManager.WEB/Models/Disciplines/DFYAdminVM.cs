﻿using NLM.BLL.DTO;
using NLM.BLL.DTObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Disciplines
{
    public class DFYAdminVM
    {
        public Int32 DisciplineId { get; set; }
        public Int32 DisciplineForYearId { get; set; }
        public ICollection<LecturerForDisciplineDTO> Lecturers { get; set; }
        public ICollection<LecturerForDisciplineDTO> Assistants { get; set; }
        public String Year { get; set; }
        public Int32 Semester { get; set; }
    }
}
