﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Account
{
    public class PasswordRestorationViewModel
    {
        [Required]
        public string Login { get; set; }
        [Required]
        [EmailAddress]
        public string Mail { get; set; }
    }
}