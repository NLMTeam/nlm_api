﻿using AutoMapper;
using NLM.BLL.DTO.DisciplinePart;
using NLM.BLL.Infrastructure;
using NLM.BLL.Interfaces;
using NLM.WEB.Filters.API;
using NLM.WEB.Models.Disciplines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace NLM.WEB.Controllers.API.DisciplinesPart
{
    public class DisciplinesForYearController : ApiController
    {
        private IDisciplineService _discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IAccountService _accountService = null;
        private IMapper _mapper;

        public DisciplinesForYearController()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DFYAddVM, DisciplineForYearDTO>();
                cfg.CreateMap<DisciplineForYearDTO, DFYAdminVM> ();
            });

            _mapper = config.CreateMapper();
        }


        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpGet]
        public IEnumerable<DFYAdminVM> GetDFYs(Int32? id)
        {
            if (id.HasValue)
            {
                return _mapper.Map<IEnumerable<DFYAdminVM>>(_discService.GetDFYsForDiscpline(id.Value));
            }
            else
            {
                return _mapper.Map<IEnumerable<DFYAdminVM>>(_discService.GetDFYs());
            }
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddDfy(DFYAddVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CUDStatus result = _discService.CreateDFY(_mapper.Map<DisciplineForYearDTO>(model));

            if (result.Status == ActionStatus.AlreadyCreated)
            {
                return Conflict();
            }

            if (result.Status == ActionStatus.Ok)
            {
                    DisciplineForYearDTO dfy = _discService.
                            FindDisciplineForYear(model.DisciplineId, model.Year, model.Semester)
                            .LastOrDefault();

                return Created("api/disciplines/" + dfy.DisciplineId, dfy);
            }
            else
            {
                return result.Message != null ? BadRequest(result.Message) as IHttpActionResult
                    : BadRequest() as IHttpActionResult;
            }
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpDelete]
        public IHttpActionResult DeleteDfy(Int32 id)
        {
            CUDStatus result = _discService.DeleteDFY(id);

            if (result.Status == ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
    }
}
