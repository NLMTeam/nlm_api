﻿using AutoMapper;
using NLM.BLL.DTO.DisciplinePart;
using NLM.BLL.Infrastructure;
using NLM.BLL.Interfaces;
using NLM.BLL.Services;
using NLM.WEB.Filters.API;
using NLM.WEB.Models.Disciplines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace NLM.WEB.Controllers.API.DisciplinesPart
{
    public class DisciplinesController : ApiController
    {
        private IDisciplineService _discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IAccountService _accountService = null;
        private IMapper _mapper;

        public DisciplinesController()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DisciplineAddVM, DisciplineDTO>();
            });

            _mapper = config.CreateMapper();
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpGet]
        public IEnumerable<DisciplineDTO> GetDisciplines()
        {
            return _discService.GetDisciplines();
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddDiscipline(DisciplineAddVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CUDStatus result = _discService.CreateDiscipline(_mapper.Map<DisciplineDTO>(model));

            if (result.Status == ActionStatus.AlreadyCreated)
            {
                return Conflict();
            }

            if (result.Status == ActionStatus.Ok)
            {
                DisciplineDTO disc = _discService.
                                            FindDiscipline(model.Name).LastOrDefault();
                return Created("api/disciplines/" + disc.DisciplineId, disc);
            }
            else
            {
                return result.Message != null ? BadRequest(result.Message) as IHttpActionResult
                    : BadRequest() as IHttpActionResult;
            }
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpDelete]
        public IHttpActionResult DeleteDiscipline(Int32 id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CUDStatus result = _discService.DeleteDiscipline(id);

            if (result.Status == ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
    }
}
