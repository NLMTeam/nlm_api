﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using NLM.BLL.Interfaces;
using NLM.BLL.DTO;
using NLM.BLL.Infrastructure;
using NLM.WEB.Models.Groups;
using AutoMapper;
using NLM.WEB.Filters.API;

namespace NLM.WEB.Controllers.API
{
    public class GroupsController : ApiController
    {
        private IGroupService _groupService = DependencyResolver.Current.GetService<IGroupService>();
        private IAccountService _accountService = null;
        private IMapper _mapper;

        public GroupsController()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GroupViewModel, GroupDTO>();
            });

            _mapper = config.CreateMapper();
        }

        private string getUserNameById(int userId)
        {
            if (_accountService == null)
            {
                _accountService = DependencyResolver.Current.GetService<IAccountService>();
            }

            UserDTO user = _accountService.SearchUser(userId).FirstOrDefault();
            return user?.FIO;
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<GroupViewModel> GetGroups()
        {
            return _groupService.GetGroups().Select(group => new GroupViewModel()
            {
                GroupId = group.GroupId,
                CaptainId = group.CaptainId,
                CaptainName = group.CaptainId.HasValue ? getUserNameById(group.CaptainId.Value) : null
            });
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult CreateGroup(GroupViewModel group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CUDStatus result = _groupService.CreateGroup(_mapper.Map<GroupDTO>(group));

            if (result.Status == ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.Created);
            }
            else
            {
                return result.Message != null ? BadRequest(result.Message) as IHttpActionResult 
                    : BadRequest() as IHttpActionResult;
            }
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpPatch]
        public IHttpActionResult UpdateGroup(GroupViewModel group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CUDStatus result = _groupService.UpdateGroup(_mapper.Map<GroupDTO>(group));

            if (result.Status == ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.OK);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }

        [APIAuthorizeFilter("ADMIN")]
        [System.Web.Http.HttpDelete]
        public IHttpActionResult DeleteGroup(string groupId)
        {
            if (String.IsNullOrWhiteSpace(groupId))
            {
                ModelState.AddModelError("GroupId", "Group number mustn't be empty or whitespce");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CUDStatus result = _groupService.DeleteGroup(groupId);

            if (result.Status == ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
    }
}
