﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLM.BLL.Interfaces;
using NLM.BLL.DTO;
using System.Web.Mvc;
using NLM.BLL.Exceptions;
using System.Text;
using NLM.WEB.Models.Account;

namespace NLM.WEB.Controllers.API.Account
{
    public class UserController : ApiController
    {
        private IAccountService _service = DependencyResolver.Current.GetService<IAccountService>();

        private byte[] getToken(string view)
        {
            int NumberChars = view.Length;
            byte[] token = new byte[NumberChars / 2];

            for (int i = 0; i < NumberChars; i += 2)
            {
                token[i / 2] = Convert.ToByte(view.Substring(i, 2), 16);
            }

            return token;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/user/login")]
        public IHttpActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UserDTO user = null;

            try
            {
                user = _service.LogIn(model.Login, model.Password);
            }
            catch (IncorrectPasswordException)
            {
                return BadRequest("Incorrect login or password");
            }

            if (user == null)
            {
                return BadRequest("Incorrect login or password");
            }

            byte[] token = _service.GetTokenAndSaveToDB(user.UserId);

            StringBuilder hex = new StringBuilder(token.Length * 2);
            foreach (byte b in token)
                hex.AppendFormat("{0:x2}", b);

            return Ok(hex.ToString());
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/user/restore")]
        public IHttpActionResult RestorePassword(PasswordRestorationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_service.SearchUser(Login: model.Login, Email: model.Mail).SingleOrDefault() == null)
            {
                return BadRequest("No user with such login and mail");
            }

            bool sendResult = _service.SendPassword(model.Login, model.Mail);

            if (!sendResult)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }

            return Ok();
        }
        
        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/user/logout")]
        public IHttpActionResult Logout(String hex)
        {
            int NumberChars = hex.Length;
            byte[] token = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                token[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);

            _service.DeleteTokenFromDB(token);

            return Ok();
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/user/getrole")]
        public string GetUserRole(string tokenView)
        {
            byte[] token = getToken(tokenView);

            int userId = _service.GetUserIdByToken(token);
            UserDTO user = _service.SearchUser(userId).FirstOrDefault();

            return user.Type;
        }
    }
}
