﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.DTO;
using NLM.BLL.DTObjects;
using NLM.BLL.Exceptions;
using NLM.BLL.Interfaces;
using NLM.WEB.Filters;
using NLM.WEB.Filters.MVC;

namespace NLM.WEB.Controllers
{
    [AuthenticationFilter]
    [OnExceptionFilter]
    public class LiteratureController : Controller
    {
        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IHelpService helpService = DependencyResolver.Current.GetService<IHelpService>();
        private IControlService controlService = DependencyResolver.Current.GetService<IControlService>();
        private IControlPointService controlPointService = DependencyResolver.Current.GetService<IControlPointService>();
        private IAccountService _accountService = DependencyResolver.Current.GetService<IAccountService>();

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        // GET: Literature
        public ActionResult Index()
        {
            Session["Placement"] = "Literature";

            if ((string)Session["Type"] == STUDENT_TYPE)
                return RedirectToAction("StudentIndex");
            if ((string)Session["Type"] == LECTURER_TYPE)
                return RedirectToAction("LecturerIndex");
            if ((string)Session["Type"] == ADMIN_TYPE)
                return RedirectToAction("AdminIndex");

            throw new NotExpectedUserTypeException();
        }

        [HttpGet]
        public ActionResult StudentIndex()
        {
            if ((string)Session["Type"] != STUDENT_TYPE)
                return RedirectToAction("Index");
            else
                return View();
        }

        [HttpGet]
        public ActionResult AdminIndex()
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
                return RedirectToAction("Index");
            else
                return View();
        }

        [HttpGet]
        public ActionResult LecturerIndex()
        {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return RedirectToAction("Index");
            else
                return View();
        }

        public ActionResult GetAdminLiterature()
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LiteratureMainViewModel model = new LiteratureMainViewModel();
            ICollection<DisciplineDTO> disciplines = new List<DisciplineDTO>();
            UserDTO user = (UserDTO)Session["User"];

            if (user.Type == ADMIN_TYPE)
            {
                disciplines = discService.GetAllDisciplines(); // Disciplines
                if (disciplines.Count == 0)
                {
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            else {
                int lecturerId = user.UserId;
                disciplines = discService.GetDisciplinesForLecturer(lecturerId); // Disciplines
                if (disciplines.Count == 0)
                {
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }

            model.Disciplines = disciplines;
            ICollection<LiteratureDTO> literature = null;
            ICollection<SharedMaterialDTO> materials = null;
            bool err = false;

            foreach (var disc in model.Disciplines)
            {
                literature = helpService.GetLiteratureForDiscipline(disc.DisciplineId);
                if (literature.Count == 0)
                {
                    err = true;
                }

                if (!err)
                    model.Literature.Add(disc.DisciplineId.ToString(), literature);
                else
                    err = false;

                // Shared materials section

                materials = helpService.GetSharedMaterialsForDiscipline(disc.DisciplineId);
                if (materials.Count == 0)
                {
                    continue;
                }

                model.Materials.Add(disc.DisciplineId.ToString(), materials);

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentsLiterature()
        {
            if ((string)Session["Type"] != STUDENT_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LiteratureMainViewModel model = new LiteratureMainViewModel();

            UserDTO user = (UserDTO)Session["User"];
            String year = (String)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            string groupId = _accountService.SearchStudent(user.UserId).Single().GroupId;

            ICollection<DisciplineDTO> disciplines = discService.GetDisciplinesForStudent(groupId, year, semester); // Disciplines
            if (disciplines.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            model.Disciplines = disciplines;
            ICollection<LiteratureDTO> literature = null;
            ICollection<SharedMaterialDTO> materials = null;
            bool err = false;

            foreach (var disc in model.Disciplines)
            {
                literature = helpService.GetLiteratureForDiscipline(disc.DisciplineId);
                if (literature.Count == 0)
                {
                    err = true;
                }

                if (!err)
                    model.Literature.Add(disc.DisciplineId.ToString(), literature);
                else
                    err = false;

                // Shared materials section

                materials = helpService.GetSharedMaterialsForDiscipline(disc.DisciplineId);
                if (materials.Count == 0)
                {
                    continue;
                }

                model.Materials.Add(disc.DisciplineId.ToString(), materials);

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetArchievedStudentsLiterature()
        {
            if ((string)Session["Type"] != STUDENT_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LiteratureMainViewModel model = new LiteratureMainViewModel();

            UserDTO user = (UserDTO)Session["User"];
            String year = (String)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            string groupId = _accountService.SearchStudent(user.UserId).Single().GroupId;

            ICollection<DisciplineDTO> disciplines = discService.GetArchievedDisciplinesForStudent(groupId, year, semester); // Disciplines
            if (disciplines.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            model.Disciplines = disciplines;
            ICollection<LiteratureDTO> literature = null;
            ICollection<SharedMaterialDTO> materials = null;
            bool err = false;

            foreach (var disc in model.Disciplines)
            {
               literature = helpService.GetLiteratureForDiscipline(disc.DisciplineId);
                if (literature.Count == 0)
                {
                    err = true;
                }

                if (!err)
                    model.Literature.Add(disc.DisciplineId.ToString(), literature);
                else
                    err = false;

                // Shared materials section
                materials = helpService.GetSharedMaterialsForDiscipline(disc.DisciplineId);
                if (materials.Count == 0)
                {
                    continue;
                }

                model.Materials.Add(disc.DisciplineId.ToString(), materials);

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetLecturersLiterature()
        {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LiteratureLectMainViewModel model = new LiteratureLectMainViewModel();

            UserDTO user = (UserDTO)Session["User"];
            String year = (String)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            ICollection<DisciplineDTO> disciplines = discService.GetDisciplinesForLecturer(user.UserId, year, semester);  // Disciplines
            if (disciplines.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            model.Disciplines = disciplines.Select(t => new DisciplineLitLectViewModel() {
                DisciplineId = t.DisciplineId,
                IsCourseProject = t.IsCourseProject,
                Name = t.Name,
                IsAdmin = discService.IsAdminForDiscipline(t.DisciplineId, user.UserId, year, semester)
            });

            ICollection<LiteratureDTO> literature = null;
            ICollection<SharedMaterialDTO> materials = null;
            bool err = false;

            foreach (var disc in model.Disciplines)
            {
                literature = helpService.GetLiteratureForDiscipline(disc.DisciplineId);
                if (literature.Count == 0)
                {
                    err = true;
                }

                if (!err)
                    model.Literature.Add(disc.DisciplineId.ToString(), literature);
                else
                    err = false;

                // Shared materials section

                materials = helpService.GetSharedMaterialsForDiscipline(disc.DisciplineId);
                if (materials.Count == 0)
                {
                    continue;
                }

                model.Materials.Add(disc.DisciplineId.ToString(), materials);

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetLecturersArchievedLiterature()
        {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LiteratureLectMainViewModel model = new LiteratureLectMainViewModel();

            UserDTO user = (UserDTO)Session["User"];
            String year = (String)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            ICollection<DisciplineDTO> disciplines = discService.GetDisciplinesForLecturerArchieved(user.UserId, year, semester);  // Disciplines
            if (disciplines.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            model.Disciplines = disciplines.Select(t => new DisciplineLitLectViewModel()
            {
                DisciplineId = t.DisciplineId,
                IsCourseProject = t.IsCourseProject,
                Name = t.Name,
                IsAdmin = discService.IsAdminForDiscipline(t.DisciplineId, user.UserId, year, semester)
            });

            ICollection<LiteratureDTO> literature = null;
            ICollection<SharedMaterialDTO> materials = null;
            bool err = false;

            foreach (var disc in model.Disciplines)
            {
                literature = helpService.GetLiteratureForDiscipline(disc.DisciplineId);
                if(literature.Count == 0)
                {
                    err = true;
                }

                if (!err)
                    model.Literature.Add(disc.DisciplineId.ToString(), literature);
                else
                    err = false;

                // Shared materials section

                materials = helpService.GetSharedMaterialsForDiscipline(disc.DisciplineId);
                if (materials.Count == 0)
                {
                    continue;
                }

                model.Materials.Add(disc.DisciplineId.ToString(), materials);

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [NonAction]
        private bool IsWorkTypeForDiscipline(WorkTypeDTO workType, DisciplineDTO discipline)
        {
            var disciplineForYear = discService.SearchDisciplineForYear(dyId: workType.DisciplineForYearId).SingleOrDefault();
            

            return disciplineForYear.DisciplineId == discipline.DisciplineId;
        }



        #region Literature Create, Update, Delete

        [HttpPost]
        public ActionResult CreateBook(int disciplineId, string name)
        { 
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                if (!discService.IsAdminForDiscipline(disciplineId, user.UserId, year, semester))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                    return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            if (name.Trim() == "")
                return Json("Некорректное значение названия дисциплины.", JsonRequestBehavior.AllowGet);

            bool createRes = helpService.CreateLiterature(name, disciplineId);
            if (!createRes)
            {
                return Json("Такая книга уже существует в базе.", JsonRequestBehavior.AllowGet);
            }

            LiteratureDTO lit = helpService.SearchLiterature(Name: name, disciplineId: disciplineId).LastOrDefault();
            if(lit == null)
            {
                return Json("Созданная книга не обнаружена в базе. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }
            return Json(lit, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditBook(int bookId, string Name)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                LiteratureDTO lit = helpService.SearchLiterature(bookId).SingleOrDefault();
                if (lit == null)
                {
                    return Json("Книга не найдена. Скорее всего, она была удалена другим пользователем. Перезагрузите страницу.", JsonRequestBehavior.AllowGet);
                }

                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                if (!discService.IsAdminForDiscipline(lit.DisciplineId, user.UserId, year, semester))
                {
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
                }
            }
            else
                if (type != ADMIN_TYPE)
                {
                    return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);
                }

            if (Name.Trim() == "")
            {
                return Json("Некорректное значение названия дисциплины.", JsonRequestBehavior.AllowGet);
            }

            if (bookId < 0)
            {
                return Json("Непредвиденная ошибка. Id равен нулю. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }

            bool updateRes = false;
            try
            {
                 updateRes = helpService.UpdateLiterature(bookId, Name);
            }
            catch (AlreadyRegisteredItemException)
            {
                return Json("Такая книга уже существует в системе.", JsonRequestBehavior.AllowGet);
            }

            if (!updateRes)
            {
                return Json("Книга не найдена. Возможно, она уже была удалена другим пользователем. Перезагрузите страницу.", JsonRequestBehavior.AllowGet);
            }

            return Json("Книга успешно отредактирована.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteBook(int bookId)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                LiteratureDTO lit = helpService.SearchLiterature(bookId).SingleOrDefault();
                if (lit == null)
                {
                    return Json("Книга не найдена. Скорее всего, она была удалена другим пользователем. Перезагрузите страницу.", JsonRequestBehavior.AllowGet);
                }

                if (!discService.IsAdminForDiscipline(lit.DisciplineId, user.UserId, year, semester))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            bool delRes = helpService.DeleteBook(bookId);
            if (!delRes)
            {
                return Json("Книга не найдена.", JsonRequestBehavior.AllowGet);
            }

            return Json("Книга успешно удалена.", JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckBooksForEmpty(string data)
        {
            if (data == null || data == "")
                return Json("Список литературы пуст.", JsonRequestBehavior.AllowGet);
            else
                return GetBooks(data);
        }

        public FileResult GetBooks(string data)
        {
            data = data.Replace("*8*--*-_*", Environment.NewLine);
            UnicodeEncoding uniEncoding = new UnicodeEncoding();
            byte[] str = uniEncoding.GetBytes(data);
            return File(str, "text/plain", "Список литературы " + DateTime.Now.ToString() + ".txt");
        }

        //public ActionResult GetChanges(string name, int discId)
        //{

        //}


        [HttpPost]
        public ActionResult LoadLiteratureFromFile()
        {
            if ((string)Session["Type"] != ADMIN_TYPE && (string)Session["Type"] != LECTURER_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            // Getting data
            var file = Request.Files[0];
            Int32 discId = Convert.ToInt32(Request.Form["DisciplineId"]);

            String type = (string)Session["Type"];

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                if (!discService.IsAdminForDiscipline(discId, user.UserId, year, semester))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            // Checking
            if (file.FileName.Split('.').Last() != "txt")
                return Json("Некорректный формат файла. Требуется .txt", JsonRequestBehavior.AllowGet);

            string[] result = null;

            using (StreamReader stream = new StreamReader(file.InputStream, Encoding.Default))
            {

                result = stream.ReadToEnd().Split(new char[] { '\n' });

                if (result.Length == 0)
                    return Json("Загруженный файл пуст.", JsonRequestBehavior.AllowGet);
            }

            if (result == null)
                return Json("Загруженный файл пуст.", JsonRequestBehavior.AllowGet);

            // Deleting
            var literature = helpService.GetLiteratureForDiscipline(discId);
            bool delRes = false;
            foreach (var lit in literature)
            {
                delRes = helpService.DeleteBook(lit.LiteratureId);
                if (!delRes)
                {
                    continue;
                }
            }

            // Filling
            bool addRes = false;
            foreach (var str in result)
            {
                addRes = helpService.CreateLiterature(str, discId);
                if (!addRes)
                {
                    continue;
                }
            }

            return Json(helpService.GetLiteratureForDiscipline(discId), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SharedMaterials Create, Update, Delete, Get
        [HttpPost]
        public JsonResult CreateMaterial()
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            Int32 disciplineId = Convert.ToInt32(Request.Form["disciplineId"]);

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                if (!discService.IsAdminForDiscipline(disciplineId, user.UserId, year, semester))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            string fileName = "";
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];

                if (upload != null)
                {
                    // получаем имя файла
                    fileName = System.IO.Path.GetFileName(upload.FileName);
                    // сохраняем файл в папку Files в проекте
                    //upload.SaveAs(Server.MapPath("~/Files/" + fileName));

                    // Check file extension

                    string ext = Path.GetExtension(fileName);
                    if (ext != ".txt" && ext != ".pdf" && ext != ".zip" && ext != ".doc" && ext != ".docx" && ext != ".xls" && ext != ".xlsx" &&
                        ext != ".ppt" && ext != ".pptx" && ext != "pps" && ext != "ppsx" && ext != "rtf")
                        return Json("Неподдерживаемое расширение файла!", JsonRequestBehavior.AllowGet);

                    // Check repeating file
                    Boolean repeats = true;
                    if(!helpService.SearchSharedMaterials(disciplinesId: disciplineId, type: fileName).Any())
                    {
                        repeats = false;
                    }

                    if (repeats)
                        return Json("Такой материал уже существует.", JsonRequestBehavior.AllowGet);

                    // Uploading file

                    var directoryPath = Server.MapPath("~/Files/SharedMaterials/");
                    DirectoryInfo directory = new DirectoryInfo(directoryPath);

                    string filePath = "";
                    do
                    {
                        filePath = Guid.NewGuid().ToString() + Path.GetExtension(fileName);
                    }
                    while (directory.GetFiles().SingleOrDefault(f => f.Name == fileName) != null);

                    string location = directoryPath + filePath;

                    upload.SaveAs(location);


                    // Registration file in DB
                    helpService.CreateSharedMaterial(fileName, location, disciplineId);

                    // Getting just created material

                    SharedMaterialDTO material = helpService.SearchSharedMaterials(disciplinesId: disciplineId, type: fileName).LastOrDefault();

                    if (material == null)
                    {
                        System.IO.File.Delete(location);
                        return Json("Ошибка загрузки файла на сервер. Обратитесь в техподдержку", JsonRequestBehavior.AllowGet);
                    }

                    // Returning new material
                    return Json(material, JsonRequestBehavior.AllowGet);
                }
            }

            // Не возникнет из-за уникальности пути к файлу

            return Json("Файл не был загружен. Пустой запрос. Возможна проблема сети. Повторите попытку.", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileResult GetMaterial(string filePath, string fileName)
        {
            // Путь к файлу
            string file_path = filePath;
            // Тип файла - content-type
            string file_type = "application/octet-stream";
            // Имя файла - необязательно
            string file_name = fileName;
            return File(file_path, file_type, file_name);
        }


        [HttpPost]
        public JsonResult EditMaterial(int materialId, string name)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            SharedMaterialDTO mat = helpService.SearchSharedMaterials(materialId).SingleOrDefault();
            if (mat == null)
            {
                return Json("Редактируемый материал не найден. Скорее всего он был удалён другим пользователем. Попробуйте перезагрузить страницу.",
                                    JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                if (!discService.IsAdminForDiscipline(mat.DisciplinesId, user.UserId, year, semester))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (name.Trim() == "")
                return Json("Некорректное значение названия файла.", JsonRequestBehavior.AllowGet);

            if (materialId < 0)
                return Json("Неверный id материала. Возможно, нарушена вёрстка страницы. Пожалуйста, перезагрузите её и обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);

            if (name == String.Empty)
                return Json("Пустой параметр имя. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);

            helpService.UpdateSharedMaterial(sharedMaterialsId: materialId, type: name);
            return Json("Материал успешно переименован.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMaterial(int materialId)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            SharedMaterialDTO material = null;

            material = helpService.SearchSharedMaterials(materialId).SingleOrDefault();
            if (material == null)
            {
                return Json("Не найден материал.", JsonRequestBehavior.AllowGet);
            }

            if (material == null)
            {
                return Json("Не найден материал.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];
                String year = (string)Session["Year"];
                Int32 semester = Convert.ToInt32(Session["Semester"]);

                if (!discService.IsAdminForDiscipline(material.DisciplinesId, user.UserId, year, semester))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            if (material.MaterialDoc != null)
            {
                System.IO.File.Delete(material.MaterialDoc);
            }

            helpService.DeleteSharedMaterials(materialId);

            return Json("Материал успешно удален.", JsonRequestBehavior.AllowGet);

        }
        #endregion
    }
}