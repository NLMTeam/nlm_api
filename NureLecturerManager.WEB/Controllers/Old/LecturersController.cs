﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLM.BLL.Exceptions;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.DTO;
using NLM.BLL.Interfaces;
using NLM.WEB.Filters;
using NLM.BLL.DTObjects;
using NLM.WEB.Filters.MVC;

namespace NLM.WEB.Controllers
{
    [AuthenticationFilter]
    [OnExceptionFilter]
    public class LecturersController : Controller
    {
        private IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();
        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
        // GET: Lecturers

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        public ActionResult Index()
        {
            Session["Placement"] = "Lecturers";

            if ((string)Session["Type"] == ADMIN_TYPE)
                return RedirectToAction("AdminIndex");


            if ((string)Session["Type"] == STUDENT_TYPE || (string)Session["Type"] == LECTURER_TYPE)
                return RedirectToAction("StudAndLectIndex");

            throw new NotExpectedUserTypeException();
        }

        [HttpGet]
        public ActionResult AdminIndex()
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
                return RedirectToAction("Index");
            else
                return View();
        }

        [HttpGet]
        public ActionResult StudAndLectIndex()
        {
            if ((string)Session["Type"] == ADMIN_TYPE)
                return RedirectToAction("Index");
            else
                return View(this.GetModel());
        }

        [HttpGet]
        public JsonResult GetAllLecturers()
        {
            var model = GetModel();
            if (model != null)
                return Json(model, JsonRequestBehavior.AllowGet);
            else
                return Json("Ошибка в проектировании БД. Лектор не содержит ссылки на пользователя... Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public IEnumerable<LecturersMainViewModel> GetModel()
        {
            List<LecturersMainViewModel> model = new List<LecturersMainViewModel>();
            String year = (string)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            var lecturers = accountService.SearchLecturer();
            if (lecturers.Count == 0)
            {
                return model;
            }

            foreach (var lecturer in lecturers)
            {
                LecturersMainViewModel lecturerModel = new LecturersMainViewModel();

                lecturerModel.Department = lecturer.Department;  // Department
                lecturerModel.Post = lecturer.Post; // Post
                lecturerModel.Specialization = lecturer.Specialization;

                UserDTO userLecturer = null;

                userLecturer = accountService.SearchUser(userId: lecturer.UserId).SingleOrDefault();

                if (userLecturer == null)
                {
                    return null;
                }

                lecturerModel.User = userLecturer; // User

                // I should get disciplines where he is lecturer
                // Should get disciplines where he is assistant
                // Should get groups where he is assistant

                lecturerModel.Disciplines = discService.GetDisciplinesForLecturer(lecturerId: lecturer.UserId, year: year, semester: semester)
                                                            .Select(d => d.Name).Distinct();

                lecturerModel.User.Parol = "";
                model.Add(lecturerModel);
            }
            return model;
        }

        [HttpPost]
        public ActionResult CreateLecturer(CreateLecturerViewModel model)
        {
            if ((String)Session["Type"] != ADMIN_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции", JsonRequestBehavior.AllowGet);
            }

            string password = Guid.NewGuid().ToString().Substring(0, 8);
            if (model.FIO.Trim() == "" || model.Email.Trim() == "" ||
                model.Post.Trim() == "" || model.Department.Trim() == "")
                return Json("Некорректный пользовательский ввод!", JsonRequestBehavior.AllowGet);

            if (!model.Email.Contains("@"))
                return Json("Некорректный адрес электронной почты.", JsonRequestBehavior.AllowGet);

            bool regRes = accountService.Register(model.FIO, password, LECTURER_TYPE, model.Email, @"/Content/images/1.jpg");
            if (!regRes)
            {
                return Json("Пользователь с таким логином или e-mail уже существует.", JsonRequestBehavior.AllowGet);
            }

            UserDTO user = accountService.SearchUser(FIO: model.FIO.Trim(), Email: model.Email.Trim()).SingleOrDefault();
            if (user == null)
            {
                return Json("Пользователь не отобразился в БД. Обратитесь в техподдержку", JsonRequestBehavior.AllowGet);
            }

            bool lectCreateRes = accountService.CreateLecturer(user.UserId, model.Post.Trim(), model.Department.Trim(), model.Specialization);
            if (!lectCreateRes)
            {
                return Json("Такой преподаватель уже существует.", JsonRequestBehavior.AllowGet);
            }

            LecturersMainViewModel newModel = new LecturersMainViewModel();

            newModel.User = accountService.SearchUser(FIO: model.FIO, Type: LECTURER_TYPE, Email: model.Email).FirstOrDefault();
            
            if (newModel.User == null)
            {
                return Json("Преподаватель не отобразился в базе. Что-то пошло не так. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }

            newModel.Post = model.Post;
            newModel.Department = model.Department;
            newModel.Specialization = model.Specialization;

            //IEnumerable<PartOfDisciplineDTO> lecturerParts = null;
            //try
            //{
            //    lecturerParts =
            //        discService.SearchPartOfDiscipline(LecturersIds: newModel.User.UserId);
            //}
            //catch (NotFoundException)
            //{
            //    newModel.Groups = null;
            //    newModel.Disciplines = null;
            //}

            //if (lecturerParts != null)
            //{
            //    newModel.Groups = lecturerParts.Select(part => part.GroupId); // Groups
            //    newModel.Disciplines =
            //        lecturerParts.Select(
            //            part => discService.SearchDiscipline(part.DisciplineId).SingleOrDefault().Name);
            //    //Disciplines
            //}

            return Json(newModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditLecturer(EditLecturerViewModel model)
        {
            if ((String)Session["Type"] != ADMIN_TYPE)
                return Json("Недостаточно прав для выполнения операции", JsonRequestBehavior.AllowGet);

            if (model.User.FIO.Trim() == "" || model.Lecturer.Post.Trim() == "" ||
                model.Lecturer.Department.Trim() == "")
                return Json("Некорректный пользовательский ввод!", JsonRequestBehavior.AllowGet);

            try
            {
                int testId = Convert.ToInt32(model.User.UserId);
            }
            catch (Exception)
            {
                return
                    Json(
                        "Некорректный id пользователя. Скорее всего вы манипулировали с кодом страницы. Если это так, то перезагрузите страницу. Иначе обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);

            }

            UserDTO user = model.User;
            bool editRes = accountService.EditUser(user.UserId, user.FIO);

            if (!editRes)
            {
                return Json("Такой логин уже существует в системе.", JsonRequestBehavior.AllowGet);
            }


            LecturerDTO lecturer = model.Lecturer;
            accountService.UpdateLecturer(lecturer.UserId, lecturer.Post, lecturer.Department, lecturer.Specialization);

            return Json("Информация о преподавателе успешно отредактирована.", JsonRequestBehavior.AllowGet);
        }
    

        [HttpPost]
        public ActionResult DeleteLecturer(int userId)
        {
            if ((String)Session["Type"] != ADMIN_TYPE)
                return Json("Недостаточно прав для выполнения операции", JsonRequestBehavior.AllowGet);
            try
                {
                    int testId = Convert.ToInt32(userId);
                }
                catch (Exception)
                {
                    return
                        Json(
                            "Некорректный id пользователя. Если вы не производили никаких манипуляций со страницой, то обратитесь в техподдержку. Иначе перезагрузите её.", JsonRequestBehavior.AllowGet);
                }

                if (!accountService.DeleteLecturer(userId))
                {
                    return
                        Json(
                            "Не найден преподаватель. Если вы не производили никаких манипуляций со страницей, то обратитесь в техподдержку. Иначе перезагрузите страницу.",
                            JsonRequestBehavior.AllowGet);
                }

                return Json("Преподаватель успешно удален.", JsonRequestBehavior.AllowGet);
            }
    }
}