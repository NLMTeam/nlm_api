﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using NLM.BLL.DTO;
//using NLM.WEB.Filters;
////using NLM.WEB.Models.Lecturer;
////using NLM.BLL.Interfaces;
////using NLM.BLL.Exceptions;
////using NLM.WEB.Filters;
////using Newtonsoft.Json;
////using System.IO;
////using NLM.WEB.Util;
////using NLM.WEB.Util.Exceptions;
////using NLM.WEB.Util.Helpers.Marks;
////using NLM.WEB.Util.Helpers.Permissions;

//namespace NLM.WEB.Controllers
//{
//    [AuthorizeFilter]
//    [OnExceptionFilter]
//    public class MarksController : Controller
//    {
//        //        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
//        //        private IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();
//        //        private IControlPointService controlPointService = DependencyResolver.Current.GetService<IControlPointService>();
//        //        private IControlService controlService = DependencyResolver.Current.GetService<IControlService>();

//        //        private IMarkHelper markHelper = DependencyResolver.Current.GetService<IMarkHelper>();
//        //        private IPermissionHelper permisHelper = DependencyResolver.Current.GetService<IPermissionHelper>();

//        private const string ADMIN_TYPE = "Админ";
//        private const string LECTURER_TYPE = "Лектор";
//        private const string STUDENT_TYPE = "Студент";

//        // GET: Marks
//        public ActionResult Index()
//        {
//            var user = (UserDTO)Session["User"];
//            Session["Placement"] = "Marks";

//            if (user == null)
//            {
//                return RedirectToAction("Login", "Account");
//            }

//            if (user.Type == STUDENT_TYPE)
//            {
//                return RedirectToAction("StudentIndex");
//            }

//            if (user.Type == LECTURER_TYPE || user.Type == ADMIN_TYPE)
//            {
//                return RedirectToAction("LecturerAdminIndex");
//            }

//            return View();
//        }

//        public ActionResult StudentIndex()
//        {
//            return View();
//        }

//        public ActionResult LecturerAdminIndex()
//        {
//            return View();
//        }

//        //        public ActionResult GetStudentModel()
//        //        {
//        //            var user = (UserDTO)Session["User"];

//        //            if (user == null)
//        //            {
//        //                return HttpNotFound();
//        //            }

//        //            StudentDTO student = null;
//        //            if (user.Type != LECTURER_TYPE)
//        //            {
//        //                student = accountService.SearchStudent(user.UserId).Single();
//        //            }
//        //            else
//        //            {
//        //                student = accountService.SearchStudent().First();
//        //            }

//        //            MarksMainViewModel model = new MarksMainViewModel();

//        //            try
//        //            {
//        //                model = markHelper.GetStudentModel(student);
//        //            }
//        //            catch (NotFoundException)
//        //            {
//        //                model.Years = model.Years != null ? model.Years : new List<string>();
//        //                model.Disciplines = model.Disciplines != null ? model.Disciplines : new List<DisciplineDTO>();
//        //                model.Groups = model.Groups != null ? model.Groups : new List<string>();
//        //                model.Worktypes = model.Worktypes != null ? model.Worktypes : new List<Category>();
//        //                model.Marks = model.Marks != null ? model.Marks : new List<StudentMarkViewModel>();
//        //             }

//        //            return Json(new { Years = model.Years, Disciplines = model.Disciplines, Groups = model.Groups, Worktypes = JsonConvert.SerializeObject(model.Worktypes), Marks = JsonConvert.SerializeObject(model.Marks) }, JsonRequestBehavior.AllowGet);
//        //        }

//        //        public ActionResult GetYears()
//        //        {
//        //            IEnumerable<string> years = new List<string>();
//        //            try
//        //            {
//        //                years = markHelper.GetYears();
//        //            }
//        //            catch (NotFoundException)
//        //            {
//        //                return Json(years, JsonRequestBehavior.AllowGet);
//        //            }

//        //            return Json(years, JsonRequestBehavior.AllowGet);
//        //        }

//        //        public ActionResult GetYearGroups(int year)
//        //        {
//        //            IEnumerable<string> groups = null;
//        //            try
//        //            {
//        //                groups = markHelper.GetYearGroups(year);
//        //            }
//        //            catch (NotFoundException)
//        //            {
//        //                return Json("Группы не найдены.", JsonRequestBehavior.AllowGet);
//        //            }

//        //            return Json(groups, JsonRequestBehavior.AllowGet);
//        //        }

//        //        public ActionResult GetGroupDisciplines(string year, string groupNum)
//        //        {
//        //            IEnumerable<DisciplineDTO> disciplines;
//        //            try
//        //            {
//        //                disciplines = markHelper.GetGroupDisciplines(year, groupNum);
//        //            }
//        //            catch (NotFoundException)
//        //            {
//        //                return Json(new List<DisciplineDTO>(), JsonRequestBehavior.AllowGet);
//        //            }

//        //            return Json(disciplines, JsonRequestBehavior.AllowGet);
//        //        }

//        //        public ActionResult GetStudentMarks(string disciplineName, string groupNum, string year)
//        //        {
//        //            UserDTO user = (UserDTO)Session["User"];
//        //            if (user == null)
//        //            {
//        //                return HttpNotFound();
//        //            }

//        //            var marks = markHelper.GetStudentsMarks(disciplineName, groupNum, year);
//        //            marks.AllowEdit = permisHelper.AllowMarksEdit(year, disciplineName, user);

//        //            Session["StudentMarks"] = marks;

//        //            string serialized = JsonConvert.SerializeObject(marks);

//        //            return Json(serialized, JsonRequestBehavior.AllowGet);
//        //        }

//        //        public ActionResult EditMark(List<string> values)
//        //        {
//        //            return PartialView();
//        //        }

//        //        [HttpPost]
//        //        public ActionResult EditMark(int id, List<string> values)
//        //        {
//        //            UserDTO user;
//        //            StudentDTO student;
//        //            try
//        //            {
//        //                user = accountService.SearchUser(id).First();
//        //            }
//        //            catch (NotFoundException)
//        //            {
//        //                return Json(new { Title="Ошибка", Text = "Не найден пользователь." }, JsonRequestBehavior.AllowGet);
//        //            }
//        //            try
//        //            {
//        //                student = accountService.SearchStudent(id).First();
//        //            }
//        //            catch (NotFoundException)
//        //            {
//        //                return Json(new { Title="Ошибка", Text = "Не найден студент." }, JsonRequestBehavior.AllowGet);
//        //            }

//        //            KeyValuePair<List<Category>, List<StudentMarkViewModel>> marks =
//        //                (KeyValuePair<List<Category>, List<StudentMarkViewModel>>)Session["StudentMarks"];

//        //            List<Category> categoryList = marks.Key;
//        //            List<StudentMarkViewModel> studentsMarks = marks.Value;

//        //            StudentMarkViewModel studentMarks = studentsMarks.Where(s => s.UserId == id).SingleOrDefault();

//        //            if (studentMarks == null)
//        //                return Json(new { Title="Ошибка", Text = "Не найдены оценки студент" }, JsonRequestBehavior.AllowGet);

//        //            for (int i = 0; i < categoryList.Count; i++)
//        //            {
//        //                if ((studentMarks.Marks == null || !studentMarks.Marks.ContainsKey(categoryList[i]) || studentMarks.Marks[categoryList[i]] != values[i]) && values[i] != "" && values[i] != null)
//        //                {
//        //                    string value = values[i];
//        //                    Category category = categoryList[i];

//        //                    if (category.ControlPoint == null)
//        //                    {
//        //                        var workType = category.WorkType;

//        //                        GroupControlDTO groupcontrol = null;
//        //                        try
//        //                        {
//        //                            groupcontrol = controlService.SearchGroupControls(workTypeId: workType.WorkTypeId, GroupNum: student.GroupId).First();
//        //                        }
//        //                        catch (NotFoundException)
//        //                        {
//        //                            // maybe try to create by own the group control when it's needed 
//        //                            //controlService.CreateGroupControl(workType.WorkTypeId, student.GroupId, default(DateTime), "", "");

//        //                            return Json(new { Title = "Ошибка", Text = String.Format("Не найден контроль группы {0}, по виду работы - {1}", student.GroupId, workType.Name) }, JsonRequestBehavior.AllowGet);
//        //                        }
//        //                        int mark;
//        //                        try
//        //                        {
//        //                            mark = Convert.ToInt32(values[i]);
//        //                        }
//        //                        catch (FormatException)
//        //                        {
//        //                            continue;
//        //                        }
//        //                        controlService.SetMark(groupcontrol.GroupControlId, user.UserId, mark, date: DateTime.Now);
//        //                    }
//        //                    else
//        //                    {
//        //                        int mark;
//        //                        try
//        //                        {
//        //                            mark = Convert.ToInt32(values[i]);
//        //                        }
//        //                        catch (FormatException)
//        //                        {
//        //                            continue;
//        //                        }
//        //                        controlPointService.SetMark(user.FIO, mark, category.ControlPoint.ControlPointId);
//        //                    }
//        //                }
//        //            }

//        //            return Json(new { Title="Действие выполнено успешно", Text = "Оценка успешно отредактирована" }, JsonRequestBehavior.AllowGet);

//        //        }

//        //        public ActionResult GenerateMarkFile(string discipline, string group, string year)
//        //        {
//        //            var marks = markHelper.GetStudentsMarks(discipline, group, year);

//        //            var directory = new DirectoryInfo(Server.MapPath("~/Files/Excel"));

//        //            string name = Guid.NewGuid().ToString().Substring(0, 10) + ".xls";

//        //            while (directory.GetFiles().Where(f => f.Name == name).Count() > 0)
//        //            {
//        //                name = Guid.NewGuid().ToString().Substring(0, 10) + ".xls";
//        //            }

//        //            ExcelWorker.GenerateMarkExcelFile(Path.Combine(directory.FullName, name), marks.Categorys, marks.Marks);

//        //            return Json("/Files/Excel/" + name, JsonRequestBehavior.AllowGet);
//        //        }

//        //        [HttpPost]
//        //        public ActionResult UploadExcelFile(HttpPostedFileBase file)
//        //        {
//        //            var fileContent = Request.Files["file"];
//        //            if (fileContent != null && fileContent.ContentLength > 0)
//        //            {
//        //                // get a stream
//        //                var stream = fileContent.InputStream;
//        //                // and optionally write the file to disk
//        //                var fileName = Guid.NewGuid().ToString().Substring(0, 9) + ".xls";
//        //                while (Directory.EnumerateFiles(Server.MapPath("~/Files/Excel")).Contains(fileName))
//        //                {
//        //                    fileName = Guid.NewGuid().ToString().Substring(0, 9) + ".xls";
//        //                }
//        //                var path = Path.Combine(Server.MapPath("~/Files/Excel"), fileName);
//        //                using (var fileStream =  System.IO.File.Create(path))
//        //                {
//        //                    stream.CopyTo(fileStream);
//        //                }

//        //                try
//        //                {
//        //                    var marks = ExcelWorker.ReadMarksFromFile(path);

//        //                    markHelper.UpdateDataBase(marks);
//        //                }
//        //                catch (ExcelDataException e)
//        //                {
//        //                    return Json(new { Title = "Ошибка", Text = e.Message }, JsonRequestBehavior.AllowGet);
//        //                }
//        //                catch (ExcelFormatException e)
//        //                {
//        //                    return Json(new { Title = "Ошибка", Text = e.Message }, JsonRequestBehavior.AllowGet);
//        //                }
//        //            }
//        //            return Json(new { Title= "Действие выполнено успешно", Text = "База данных синхронизирована с загруженнным файлом." }, JsonRequestBehavior.AllowGet);
//        //        }

//    }
//}