﻿using NLM.WEB.Filters;
using NLM.WEB.Filters.MVC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NLM.WEB.Controllers
{
    [OnExceptionFilter]
    [AuthenticationFilter]
    public class ConfigController : Controller
    {
        private const String ADMIN_TYPE = "ADMIN";
        internal static object threadLock = new object();
        // GET: Config
        public ActionResult Admin()
        {
            if ((String)Session["Type"] == ADMIN_TYPE)
                return View();
            else
                return RedirectToAction("Index", "Discipline", null);
        }

        [HttpPost]
        public ActionResult SaveSettings(String Year, String Semester)
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);

            Session["Year"] = Year;
            Session["Semester"] = Semester;

            lock(threadLock){
                using (StreamWriter stW = new StreamWriter(Server.MapPath("/Files/Config.txt")))
                {
                    stW.WriteLine(Year);
                    stW.WriteLine(Semester);
                }
            }
                return RedirectToAction("SettingsWereSaved");
        }

        [HttpGet]
        public ActionResult SettingsWereSaved()
        {
            return View("SettingsWereSaved");
        }
    }
}