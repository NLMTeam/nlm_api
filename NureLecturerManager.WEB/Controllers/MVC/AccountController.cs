﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLM.BLL.DTO;
using NLM.BLL.Exceptions;
using NLM.BLL.Interfaces;
using NLM.WEB.Models.Account;
using NLM.WEB.Filters;
using System.Security.Cryptography;
using NLM.WEB.Util;
using System.Text;
using System.Web.Security;

using NLM.WEB.Filters.MVC;

namespace NLM.WEB.Controllers
{
    [OnExceptionFilter]
    public class AccountController : Controller
    {
        // GET: Account
        private IAccountService userService = DependencyResolver.Current.GetService<IAccountService>();
        // GET: Account

        [HttpGet]
        public ActionResult Login()
        {
            if (Session["User"] != null)
                return RedirectToAction("Index", "Discipline");
            else
                return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string key)
        {
            UserDTO user = null;

            var login = EncryptStringSample.StringCipher.BitDecode(model.Login, key);
            var password = EncryptStringSample.StringCipher.BitDecode(model.Password, key);

            if (!ModelState.IsValid) return View("Index");
            try
            {
                user = userService.LogIn(login, password);

                if (user == null)
                {
                    model.Login = login;
                    ModelState.AddModelError("", "Неверный логин или пароль!");
                    ViewBag.Login = login;
                    return View("Index");
                }
            }
            catch (IncorrectPasswordException)
            {
                model.Login = login;
                ModelState.AddModelError("", "Неверный логин или пароль!");
                ViewBag.Login = login;
                return View("Index");
            }

            Session["User"] = user;
            Session["Type"] = user.Type;

            // Getting current year and semester

            lock (ConfigController.threadLock)
            {
                using (StreamReader stR = new StreamReader(Server.MapPath("/Files/Config.txt")))
                {
                    Session["Year"] = stR.ReadLine();
                    Session["Semester"] = stR.ReadLine();
                }
            }

            // Token section
            byte[] token = userService.GetTokenAndSaveToDB(user.UserId);
            Session["Token"] = token;
            writeCookies(token);

            // /token

            return RedirectToAction("Index", "Discipline");
        }

        [HttpGet]
        public ActionResult Restore()
        {
            return View("PasswordRestoration");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Restore(PasswordRestorationViewModel model)
        {
            if (!ModelState.IsValid) return View("PasswordRestoration");

            if (userService.SearchUser(Login: model.Login, Email: model.Mail).SingleOrDefault() == null)
            {
                ModelState.AddModelError("", "Неверный логин или email!");
                return View("PasswordRestoration");
            }

            bool sendResult = userService.SendPassword(model.Login, model.Mail);
            
            if (!sendResult)
            { 
                ModelState.AddModelError("", "Что-то пошло не так. Обратитесь в техподдержку.");
                return View("PasswordRestoration");
            }

            return RedirectToAction("NotePaswordRestored");
        }

        public ActionResult NotePaswordRestored()
        {
            return View();
        }

        [HttpGet]
        [AuthenticationFilter]
        public ActionResult UserPage()
        {
            UserDTO user = (UserDTO)Session["User"];
            ViewBag.Login = user.Login;
            ViewBag.Image = user.Image;

            return View();
        }

        [HttpPost]
        [AuthenticationFilter]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeLogin(LoginChangeViewModel model, string key)
        {
            UserDTO user = null;
            UserDTO sessionUser = (UserDTO)Session["User"];
            model.Login = EncryptStringSample.StringCipher.BitDecode(model.Login, key);

            if (!ModelState.IsValid)
            {
                return PartialView(model: "Проверьте правильность ввода полей!");
            }

            if (userService.SearchUser(Login: sessionUser.Login, Email: model.Mail).SingleOrDefault() == null)
            {
                return PartialView(model: "Неверный email!");
            }

            bool editRes = userService.EditUser(user.UserId, Login: model.Login, email: model.Mail);

            if (!editRes)
            {
                return PartialView(model: "Такой логин уже занят");
            }

            userService.SendLogin(model.Login, sessionUser.Mail);

            sessionUser.Login = model.Login;
            Session["User"] = sessionUser;
            return PartialView("SuccessLoginChangeAjax");
        }

        [HttpPost]
        [AuthenticationFilter]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(PasswordChangeViewModel model, string key)
        {
            UserDTO user = null;
            UserDTO sessionUser = (UserDTO)Session["User"];

            model.Password = EncryptStringSample.StringCipher.BitDecode(model.Password, key);
            model.NewPassword1 = EncryptStringSample.StringCipher.BitDecode(model.NewPassword1, key);
            model.NewPassword2 = EncryptStringSample.StringCipher.BitDecode(model.NewPassword2, key);

            if (!ModelState.IsValid) return PartialView(model: "Проверьте правильность ввода полей!");
            if (model.NewPassword1 != model.NewPassword2)
            {
                return PartialView(model: "Новый пароль и его подтверждение не совпадают!");
            }

            UserDTO userInDB = userService.SearchUser(Login: sessionUser.Login, Email: model.Mail).SingleOrDefault();

            if (userInDB == null)
            {
                return PartialView(model: "Неверный email!");
            }

            var service = SHA256.Create();

            var stringToEncode = Convert.ToBase64String(user.Saline) + model.Password;

            string encoded = Convert.ToBase64String(service.ComputeHash(Encoding.UTF8.GetBytes(stringToEncode)));

            if (userInDB.Parol != encoded)
            {
                return PartialView(model: "Неверный текущий пароль!");
            }

            bool res = userService.EditUser(user.UserId, password: model.NewPassword1);

            if (!res)
            {
                return PartialView(model: "Пользователь не найден. Обратитесь в техподдержку.");
            }

            userService.SendChangedPass(sessionUser.Login, model.NewPassword1, sessionUser.Mail);
            ModelState.Clear();

            return PartialView("SuccessPasswordChangeAjax");
        }

        [HttpPost]
        [AuthenticationFilter]
        public ActionResult ChangeImage()
        {
            UserDTO session = (UserDTO)Session["User"];
            String fileName = null;
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];
                if (upload != null)
                {
                    // получаем имя файла
                    fileName = System.IO.Path.GetFileName(upload.FileName);
                    // сохраняем файл в папку Files в проекте
                    //upload.SaveAs(Server.MapPath("~/Files/" + fileName));

                    // Check file extension

                    string ext = Path.GetExtension(fileName);
                    if (ext != ".jpg" && ext != ".png" && ext != ".gif" && ext != ".bmp")
                        return Json("Неподдерживаемое расширение файла!", JsonRequestBehavior.AllowGet);


                    // Uploading file

                    var directoryPath = Server.MapPath("~/Content/images/");
                    DirectoryInfo directory = new DirectoryInfo(directoryPath);

                    string filePath = "";
                    do
                    {
                        filePath = Guid.NewGuid().ToString() + Path.GetExtension(fileName);
                    }

                    while (directory.GetFiles().SingleOrDefault(f => f.Name == fileName) != null);

                    string location = directoryPath + filePath;

                    upload.SaveAs(location);

                    // Getting current image from DB
                    UserDTO user = userService.SearchUser(session.UserId).Single();

                    // Deleting old image
                    if (user.Image != "/Content/images/1.jpg")
                    {
                        String path = Server.MapPath(user.Image);
                        System.IO.File.Delete(path);
                    }

                    // Registering new image in DB
                    bool editRes = userService.EditUser(user.UserId, Image: "/Content/images/" + filePath);

                    if (!editRes)
                    {
                        return Json("Ваш пользователь не найден... Обратитесь в техподдержку");
                    }

                    session.Image = "/Content/images/" + filePath;
                    Session["User"] = session;
                    break;
                }
            }

            return View("ImageWasChanged");
        }

        [HttpGet]
        public ActionResult ImageWasChanged()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SuccessLoginChange()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SuccessPasswordChange()
        {
            return View();
        }

        [NonAction]
        private void writeCookies(byte[] token)
        {
            StringBuilder hex = new StringBuilder(token.Length * 2);
            foreach (byte b in token)
                hex.AppendFormat("{0:x2}", b);

            Response.Cookies.Add(new HttpCookie("pass1", Guid.NewGuid().ToString().Replace('-', '8').Substring(0, hex.Length)));
            Response.Cookies.Add(new HttpCookie("pass2", Guid.NewGuid().ToString().Replace('-', 'k').Substring(0, hex.Length)));
            Response.Cookies.Add(new HttpCookie("pass3", hex.ToString()));
            Response.Cookies.Add(new HttpCookie("pass4", Guid.NewGuid().ToString().Replace('-', 'o').Substring(0, hex.Length)));

        }

        public ActionResult LogOff()
        {
            String hex = Request.Cookies["pass3"].Value;

            //int NumberChars = hex.Length;
            //byte[] token = new byte[NumberChars / 2];
            //for (int i = 0; i < NumberChars; i += 2)
            //    token[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);

            userService.DeleteTokenFromDB((byte[])Session["Token"]);
            //byte[] sessionToken = (byte[])Session["Token"];
            //return Json(token == sessionToken, JsonRequestBehavior.AllowGet);

            if (Request.Cookies["pass2"] != null)
            {
                var cookie = new HttpCookie("pass2")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(cookie);
            }

            if (Request.Cookies["pass1"] != null)
            {
                var cookie = new HttpCookie("pass1")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(cookie);
            }

            if (Request.Cookies["pass3"] != null)
            {
                var cookie = new HttpCookie("pass3")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(cookie);
            }

            if (Request.Cookies["pass4"] != null)
            {
                var cookie = new HttpCookie("pass4")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(cookie);
            }


            Session["User"] = null;
            Session["pass3"] = null;

            return RedirectToAction("Login");
        }
    }
}