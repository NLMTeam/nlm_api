﻿using System.Web;
using System.Web.Optimization;

namespace NLM.WEB
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Old

            //// Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            //// используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.min.css", "~/Content/site.css", "~/Content/CourseProject.css"));

            //bundles.Add(new StyleBundle("~/Content/oi").Include("~/Content/oi-select.min.css"));

            //bundles.Add(new StyleBundle("~/Content/scroll").Include("~/Content/Scroll.css"));

            //bundles.Add(new ScriptBundle("~/bundles/angular").Include("~/Scripts/angular.min.js"));

            //// Lecturers page


            //bundles.Add(new ScriptBundle("~/bundles/lecturersAdmin").Include("~/Scripts/pv_scripts/angular/LecturersPage/lecturersApp.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/controllers/lecturersMainController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/directives/lecturerArea.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/services/lecturersModel.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/services/selectedLecturer.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/services/responseService.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/controllers/lecturersAddModalController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/controllers/lecturersDeleteModalController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/controllers/lecturersEditModalController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/services/editLecturerService.js",
            //                                                                "~/Scripts/pv_scripts/angular/LecturersPage/lecturers.js"));

            //// Literature page

            //// Admin section

            //bundles.Add(new ScriptBundle("~/bundles/literatureAdmin").Include("~/Scripts/pv_scripts/angular/modules/literatureAdminApp.js",
            //                                                                 "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/controllers/literatureMainAdminController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/directives/disciplineArea.js",
            //                                                                 "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/directives/literatureItem.js",
            //                                                                 "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/literatureModel.js",
            //                                                                 "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/selectedDiscipline.js",
            //                                                                 "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/selectedLiterature.js",
            //                                                                 "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/responseService.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/controllers/literatureAddModalController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/controllers/litDelModalController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/controllers/litEditModalController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/editLiterature.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/controllers/litTxtLoadController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/controllers/materialSubController.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/directives/material.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/values/materialId.js",
            //                                                                "~/Scripts/pv_scripts/angular/LiteraturePage/Admin/services/values/editMaterial.js"));

            //bundles.Add(new ScriptBundle("~/bundles/literatureFocuses")
            //              .Include("~/Scripts/pv_scripts/angular/LiteraturePage/Admin/focuses.js"));


            //// Lecturer section
            //bundles.Add(new ScriptBundle("~/bundles/literatureLect").Include("~/Scripts/pv_scripts/angular/modules/literatureLectApp.js",
            //                                                                            "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/values/trackingValue.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/controllers/literatureMainController.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/controllers/literatureAddModalController.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/controllers/litDelModalController.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/controllers/litEditModalController.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/controllers/litTxtLoadController.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/controllers/materialSubController.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/directives/disciplineArea.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/directives/literatureItem.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/literatureModel.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/archievedLiterature.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/currentLiterature.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/selectedDiscipline.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/selectedLiterature.js",
            //                                                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/responseService.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/editLiterature.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/directives/material.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/values/materialId.js",
            //                                                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/services/values/editMaterial.js"));

            //// Student section
            //bundles.Add(new ScriptBundle("~/bundles/literatureStudent").Include("~/Scripts/pv_scripts/angular/modules/literatureStudApp.js",
            //                                          "~/Scripts/pv_scripts/angular/LiteraturePage/Student/controllers/literatureMainController.js",
            //                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Student/directives/literatureItem.js",
            //                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Student/directives/disciplineArea.js",
            //                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Student/directives/material.js",
            //                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Student/services/literatureModel.js",
            //                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Student/services/archievedLiterature.js",
            //                                           "~/Scripts/pv_scripts/angular/LiteraturePage/Student/services/currentLiterature.js",
            //                                            "~/Scripts/pv_scripts/angular/LiteraturePage/Student/services/selectedDiscipline.js",
            //                                            "~/Scripts/pv_scripts/angular/LiteraturePage/Student/services/responseService.js"));


            //// Discipline page
            //// Admin section

            //bundles.Add(new ScriptBundle("~/bundles/disciplineMainAdmin").Include(
            //        "~/Scripts/pv_scripts/angular/modules/disciplinesLectApp.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/years.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/disciplineMainController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/disciplineSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/dfySubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/partsSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/cpSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/workTypeSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/controllers/materialSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/disciplinePageModel.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/selectedDiscipline.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/selectedCP.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/selectedDFY.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/directives/discipline.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/directives/DFY.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/directives/cp.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/directives/workType.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/directives/material.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/editDiscipline.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/editDFY.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/editCP.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/editWorkType.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/values/workTypeId.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/values/materialId.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/values/editMaterial.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Admin/services/responseService.js",
            //        "~/Scripts/pv_scripts/angular/oi-select.min.js"
            //    ));

            //bundles.Add(new ScriptBundle("~/bundles/disciplineAdminFocus")
            //        .Include("~/Scripts/pv_scripts/angular/DisciplinePage/Admin/focuses.js"));

            //// Lecturer section

            //bundles.Add(new ScriptBundle("~/bundles/disciplineMainLecturer").Include(
            //        "~/Scripts/pv_scripts/angular/modules/disciplinesLectApp.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/years.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/disciplineMainController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/materialSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/disciplinePageModel.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/archievedDisciplines.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/currentDisciplines.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/futureDisciplines.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/selectedDFY.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/selectedCP.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/dfySubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/partsSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/cpSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/workTypeSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/controllers/materialSubController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/discipline.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/cp.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/workType.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/material.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/workTypeId.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/materialId.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/editMaterial.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/tracking.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/responseService.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/editCP.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/editWorkType.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/workTypeId.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/materialId.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/services/values/editMaterial.js",
            //        "~/Scripts/pv_scripts/angular/oi-select.min.js"
            //    ));

            //bundles.Add(new ScriptBundle("~/bundles/disciplineLecturerFocus")
            //    .Include("~/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/focuses.js"));

            //// Student section

            //bundles.Add(new ScriptBundle("~/bundles/disciplineMainStudent").Include(
            //        "~/Scripts/pv_scripts/angular/modules/disciplinesStudApp.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/controllers/disciplineMainController.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/disciplinePageModel.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/selectedDiscipline.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/selectedDFY.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/selectedCP.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/discipline.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/dfy.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/cp.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/workType.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/material.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/responseService.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/archievedDisciplines.js",
            //        "~/Scripts/pv_scripts/angular/DisciplinePage/Student/services/currentDisciplines.js"
            //    ));

            //// Other pages/sections

            //bundles.Add(new ScriptBundle("~/bundles/angularFileUploader").Include("~/Scripts/pv_scripts/angular/angular-file-upload/angular-file-upload.js"));

            //bundles.Add(new ScriptBundle("~/bundles/userPage").Include("~/Scripts/pv_scripts/UserPage.js"));

            #endregion

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.min.js"));
        }
    }
}
