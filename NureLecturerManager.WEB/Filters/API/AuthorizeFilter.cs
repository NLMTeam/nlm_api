﻿using NLM.BLL.Interfaces;
using System;
using System.Linq;
using System.Web.Http.Filters;
using NLM.BLL.DTO;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.Net;

namespace NLM.WEB.Filters.API
{
    public class APIAuthorizeFilter : AuthorizationFilterAttribute
    {
        private string[] allowedRoles = null;
        private IAccountService userService = (IAccountService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IAccountService));

        public APIAuthorizeFilter(params string[] roles)
        {
            allowedRoles = roles;
        }

        public override void OnAuthorization(HttpActionContext context)
        {
            int userId = -1;
            // not sure here
            string hex = context.Request.Headers.Authorization?.ToString();

            if (hex != null)
            {
                byte[] token = getToken(hex);
                userId = userService.GetUserIdByToken(token);
            }

            if (userId == -1)
            {
                context.Response = context.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            else
            {
                UserDTO user = userService.SearchUser(userId).FirstOrDefault();

                if (user == null)
                {
                    context.Response = context.Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
                else
                {
                    if (!this.allowedRoles.Contains(user.Type))
                    {
                        context.Response = context.Request.CreateResponse(HttpStatusCode.Forbidden);
                    }
                }
            }

            base.OnAuthorization(context);
        }

        private byte[] getToken(string view)
        {
            int NumberChars = view.Length;
            byte[] token = new byte[NumberChars / 2];

            for (int i = 0; i < NumberChars; i += 2)
            {
                token[i / 2] = Convert.ToByte(view.Substring(i, 2), 16);
            }

            return token;
        }

    }
}
