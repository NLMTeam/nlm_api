﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NLM.WEB.Util;
using NLM.BLL.Interfaces;
using NLM.BLL.Exceptions;
using System.Web.Http;

namespace NLM.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            AutofacConfig.ConfigContainer();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
