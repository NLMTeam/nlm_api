﻿var lecturerApp = angular.module('lecturersApp');

lecturerApp.controller('lecturersMainController', function ($scope, lecturersModel, selectedLecturer, responseService, editLecturerService) {

    $scope.ready = false;

    var lecturers = lecturersModel.getLecturers();

    if (typeof (lecturers.success) != "undefined") {
        lecturers.success(function (data) {
            $scope.lecturerCollection = data;
        });
    }
    else {
        $scope.lecturerCollection = lecturers;
    }

    $scope.ready = true;

    // Selected lecturer

    $scope.selLect = selectedLecturer.getSelectedLecturer();

    // Response

    $scope.response = responseService.getResponse();
   
    // editLecturer

    $scope.editLecturer = editLecturerService.getEditLecturer();

    $scope.editClick = function()
    {
        if ($scope.selLect.lecturerBody == null)
            return;
        editLecturerService.setEditLecturer({
            User: {
                UserId: $scope.selLect.lecturerBody.User.UserId,
                FIO: $scope.selLect.lecturerBody.User.FIO
            },
            Post: $scope.selLect.lecturerBody.Post,
            Department: $scope.selLect.lecturerBody.Department,
            Specialization: $scope.selLect.lecturerBody.Specialization
        });

        $('#edit-modal').modal();
    };

    // Modals
    $scope.addModal = function () {
        $('#add-modal').modal('show');
    };

    $scope.delModal = function () {
        if ($scope.selLect.lecturerBody == null)
            return;
        $('#del-modal').modal();
    };
});