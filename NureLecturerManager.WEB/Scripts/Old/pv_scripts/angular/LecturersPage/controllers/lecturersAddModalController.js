﻿lecturerApp.controller('lecturersAddModalController', function($scope, lecturersModel, $http, responseService) {
    $scope.newLecturer = {
        FIO: '',
        Email: '',
        Post: '',
        Department: '',
        Specialization: ''
    };

    $scope.save = function (e) {
        e.preventDefault();
            $('#add-modal').modal('hide');
            $http.post('/Lecturers/CreateLecturer', $scope.newLecturer).success(function (data) {

                console.info(data);
                console.info(lecturersModel.getLecturers());
                if (data.Post != undefined && data.User != undefined && data.User.UserId != undefined) {
                        lecturersModel.addLecturer(data);
                        responseService.setResponse('Преподаватель успешно добавлен.');
                        $('#response-modal').modal('show');
                        $scope.newLecturer.FIO = '';
                        $scope.newLecturer.Email = '';
                        $scope.newLecturer.Post = '';
                        $scope.newLecturer.Department = '';
                        $scope.newLecturer.Specialization = '';
                }
                else {
                    responseService.setResponse(data);
                    $('#response-modal').modal('show');
                }
            })
            .error(function (err){
                responseService.setResponse(err);
                $('#response-modal').modal('show');
            });
    }
});
