﻿lecturerApp.controller('lDelController', function ($scope, $http, selectedLecturer, lecturersModel, responseService) {

    $scope.delete = function () {
        var delLect = selectedLecturer.getSelectedLecturer();

        if (delLect == undefined || delLect == null)
            return;

        console.log(selectedLecturer.getSelectedLecturer());

        var userId = delLect.lecturerBody.User.UserId;

        $('#del-modal').modal('hide');
        $http.post('/Lecturers/DeleteLecturer?userId=' + userId).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Преподаватель успешно удален.') {
                lecturersModel.deleteLecturer(userId);
                selectedLecturer.setSelectedLecturer({ User: {}});
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
        });
    }
});
