﻿lecturersApp.factory('editLecturerService', function () {
    editLecturer = {
        User: {
            UserId: '',
            FIO: ''
        },
        Post: '',
        Department: '',
        Spec: ''
    };

    return {
        getEditLecturer: function () {
            return editLecturer;
        },
        setEditLecturer: function (value) {
            editLecturer.User.UserId = value.User.UserId;
            editLecturer.User.FIO = value.User.FIO;
            editLecturer.Post = value.Post;
            editLecturer.Department = value.Department;
            editLecturer.Spec = value.Specialization;
        }
    }
});