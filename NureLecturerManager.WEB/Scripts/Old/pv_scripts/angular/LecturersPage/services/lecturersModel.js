﻿lecturersApp.factory('lecturersModel', function ($http) {
        var lecturersCollection = null;

        return {
            
            getLecturers: function () {
                if (lecturersCollection == undefined || lecturersCollection == null)
                    return $http.get('/Lecturers/GetAllLecturers').success(function (data) {
                        lecturersCollection = data;
                    })
                    .error(function (err) {
                        responseService.setResponse('Ошибка соединения. Исправьте её и перезагрузите страницу.');
                        $('#response-modal').modal();
                    });

                else
                    return lecturersCollection;
            },

            setLecturers: function(value){
                lecturersCollection = value;
            },

            addLecturer: function (lecturer) {
                lecturersCollection.push(lecturer);
            },

            deleteLecturer: function (id) {
                for (var i=0; i<lecturersCollection.length; i++)
                    if (lecturersCollection[i].User.UserId == id)
                        lecturersCollection.splice(i, 1);
            }
    };
});