﻿lecturersApp.directive('lecturer', function (selectedLecturer, lecturersModel) {
    return {
        link: function (scope, element, attrs) {
            scope.select = function () {
                // Find old selected lecturer and deselect it
                var lect = selectedLecturer.getSelectedLecturer().lecturerBody;

                if (lect != null) {
                    lect.activity = "";
                }

                // Find new selected lecturer and select it
                var id = scope.lecturer.User.UserId;
                var model = lecturersModel.getLecturers();


                for (var i = 0; i < model.length; i++)
                    if (id == model[i].User.UserId) {
                        model[i].activity = "lecturer-activated";
                        selectedLecturer.setSelectedLecturer(model[i]);
                    }
                //angular.element(element.children()[0]).addClass('lecturer-activated');
            };
        
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/LecturersPage/directives/lecturerArea.html',
        scope: {
          lecturer: '='  
        }
    };
});




