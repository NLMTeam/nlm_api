﻿disciplinesApp.controller('disciplineMainController', function ($scope, $http,
                            disciplinePageModel, selectedDFY,
                            selectedCP, responseService, editCP, editWorkType, archievedDisciplines,
                            currentDisciplines, futureDisciplines, tracking, years) {


    $scope.ready = false;
    $scope.tracking = tracking;

    var modelInit = currentDisciplines.getModel();

    var selectInit = function () {
        selectedDFY.setDFY($scope.model.body.Disciplines[0]);
        $scope.currentDiscipline = selectedDFY.getDFY();


        if ($scope.model.body.Disciplines[0] != undefined &&
                $scope.model.body.Disciplines[0].length != 0) {

            if ($scope.model.body.ControlPoints[$scope.currentDiscipline.body.DFYId] &&
                $scope.model.body.ControlPoints[$scope.currentDiscipline.body.DFYId].length != 0) {

                selectedCP.setCP($scope.model.body.ControlPoints[$scope.currentDiscipline.body.DFYId][0]);
            }
            else {
                selectedCP.setCP(null);
            }
            $scope.currentCP = selectedCP.getCP();
        }
        else {
            selectedDFY.setDFY(null);
            $scope.currentDiscipline = selectedDFY.getDFY();
        }
    }

    years.init();

    tracking.status= 'now';
    if (typeof (modelInit.success) != "undefined") {
        modelInit.success(function (data) {
            disciplinePageModel.setModel(data);
            $scope.model = disciplinePageModel.getModel();
            // selection services initialization
            if ($scope.model.body.Disciplines.length > 0) {
                selectInit();
            }
            else {
                selectedDFY.setDFY(null);
                $scope.currentDiscipline = selectedDFY.getDFY();
            }

            $scope.ready = true;
        });
    }
    else {
        disciplinePageModel.setModel(modelInit);
        $scope.model = disciplinePageModel.getModel();

        //selectedDiscipline.setDiscipline($scope.model.body.Disciplines.length > 0 ? $scope.model.body.Disciplines[0] : null);
        //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
        selectInit();
    }

    $scope.prolongYears = years.getYearsForProlong();

    $scope.response = responseService.getResponse();

    // DFY modals

    //$scope.addDFYModal = function () {
    //    $('#dfy-add-modal').modal('show');
    //}

    $scope.delDFYModal = function () {
        $('#dfy-del-modal').modal('show');
    }

    $scope.continueDFYClick = function () {
        $('#dfy-continue-modal').modal('show');
    }

    // CP modals 

    $scope.addCPModal = function () {
        $('#cp-add-modal').modal('show');
    }

    $scope.delCPModal = function () {
        $('#cp-del-modal').modal('show');
    }

    // Work types modals 

    $scope.addWorkTypeModal = function () {
        $('#workType-add-modal').modal('show');
    }

    $scope.delWorkTypeModal = function () {
        $('#workType-del-modal').modal('show');
    }

    $scope.editCPClick = function () {
        editCP.setEditCP({
            Id: $scope.currentCP.body.ControlPointId,
            Number: $scope.currentCP.body.Number,
            Name: $scope.currentCP.body.Name,
            Descr: $scope.currentCP.body.Description,
            Date: $scope.currentCP.body.Date,
            MinMark: $scope.currentCP.body.MinMark,
            MaxMark: $scope.currentCP.body.MaxMark
        });


        $('#cp-edit-modal').modal();
    };

    $scope.response = responseService.getResponse();
    $scope.getArchievedModel = function (e) {
        tracking.status= 'past';

        var model = archievedDisciplines.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                disciplinePageModel.setModel(data);

                // selection services initialization
                if ($scope.model.body.Disciplines.length > 0) {
                    selectInit();
                }

                else {
                    selectedDFY.setDFY(null);
                    selectInit();
                }

            });
        }
        else {
            disciplinePageModel.setModel(model);

            //selectedDiscipline.setDiscipline($scope.model.body.Disciplines.length > 0 ? $scope.model.body.Disciplines[0] : null);
            //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectInit();
        }
    }

    $scope.getCurrentModel = function () {
        tracking.status= 'now';

        var model = currentDisciplines.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                disciplinePageModel.setModel(data);

                // selection services initialization
                if ($scope.model.body.Disciplines.length > 0) {
                    selectInit();
                }
                else {
                    selectedDFY.setDFY(null);
                    $scope.currentDiscipline = selectedDFY.getDFY();
                }
            });
        }
        else {
            disciplinePageModel.setModel(model);

            //selectedDiscipline.setDiscipline($scope.model.body.Disciplines.length > 0 ? $scope.model.body.Disciplines[0] : null);
            //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectInit();
        }
    }

    $scope.getFutureModel = function () {
        tracking.status= 'future';

        var model = futureDisciplines.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                disciplinePageModel.setModel(data);

                // selection services initialization
                if ($scope.model.body.Disciplines.length > 0) {
                    selectInit();
                }
                else {
                    selectedDFY.setDFY(null);
                    $scope.futureDiscipline = selectedDFY.getDFY();
                }
            });
        }
        else {
            disciplinePageModel.setModel(model);

            //selectedDiscipline.setDiscipline($scope.model.body.Disciplines.length > 0 ? $scope.model.body.Disciplines[0] : null);
            //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectInit();
        }
    }

    var now = new Date();


    $http.get('/Discipline/GetGroups').success(function (data) {
        $scope.groups = data;

        $http.get('/Discipline/GetLecturers').success(function (data) {
            $scope.lecturers = data;
        });

    }).error(function (err) {
        responseService.setResponse('Проверьте соединение');
        $('#responseModal').modal('show');
    });
});