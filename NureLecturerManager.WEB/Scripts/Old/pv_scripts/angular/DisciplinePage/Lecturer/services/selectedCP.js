﻿disciplinesApp.factory('selectedCP', function () {
    CP = { body: null };

    return {
        getCP: function () {
            return CP;
        },

        setCP: function (value) {

            // Deselecting old value
            if (CP.body != null) {
                CP.body.activity = "";
            }

            // Changing current value
            CP.body = value;
            if (value != null) {
                CP.body.activity = "active";
            }
        }
    }
});