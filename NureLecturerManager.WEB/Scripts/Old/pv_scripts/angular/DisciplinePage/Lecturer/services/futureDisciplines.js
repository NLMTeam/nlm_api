﻿disciplinesApp.factory('futureDisciplines', function ($http, responseService) {
    futModel = null;

    return {
        getModel: function () {
            if (futModel == undefined || futModel == null)
                return $http.get('/Discipline/GetLecturersFutureModel').success(function (data) {
                    futModel = data;
                    for (var cp in futModel.ControlPoints) {
                        if (cp.length == undefined) {
                            break;
                        }
                        else {
                            for (var i = 0; i < futModel.ControlPoints[cp].length; i++) {
                                var dateStr = futModel.ControlPoints[cp][i].Date.split('/');
                                var dateStr2 = futModel.ControlPoints[cp][i].Date.split('.');
                                if (dateStr.length == 3)
                                    futModel.ControlPoints[cp][i].Date = new Date(dateStr[2], dateStr[0] * 1 - 1, dateStr[1]);
                                else
                                    futModel.ControlPoints[cp][i].Date = new Date(dateStr[2], dateStr[1] * 1 - 1, dateStr[0]);
                            }
                        }
                    }
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });

            else {
                return futModel;
            }
        }
    };
});




