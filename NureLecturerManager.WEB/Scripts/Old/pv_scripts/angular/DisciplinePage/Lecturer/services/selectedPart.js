﻿disciplinesApp.factory('selectedPart', function (selectedCP) {
    part = { body: null };

    return {
        getPart: function () {
            return part;
        },

        setPart: function (value) {

            // Deselecting old value
            if (part.body != null) {
                part.body.activity = "";
            }

            // Changing current value
            part.body = value;
            if (value != null)
                part.body.activity = "active";
        }
    }
});