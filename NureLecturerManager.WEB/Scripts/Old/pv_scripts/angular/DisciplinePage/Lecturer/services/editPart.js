﻿disciplinesApp.factory('editPart', function () {
    var editPart = {
        PartId: '',
        GroupNum: '',
        Lecturers: [],
        ControlForm:''
    };

    return {
        getEditPart: function () {
            return editPart;
        },
        setEditPart: function (value) {
            editPart.PartId = value.PartId;
            editPart.GroupNum = value.GroupNum,
            editPart.Lecturers = [];
            for (var i = 0; i < value.Lecturers.length; i++)
                editPart.Lecturers.push(value.Lecturers[i].Id);
            editPart.ControlForm = value.ControlForm
        }
    }
});
