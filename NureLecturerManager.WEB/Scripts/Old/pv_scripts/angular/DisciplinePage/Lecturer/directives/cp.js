﻿disciplinesApp.directive('cp', function (disciplinePageModel, selectedDFY, selectedCP) {
    return {
        link: function (scope, element, attrs) {
            scope.select = function (e) {
                e.preventDefault();
                var cp = selectedCP.getCP();

                // Find new selected DFY and select it
                var id = scope.cp.ControlPointId;
                if (cp.body == null || id != cp.body.CPId) {
                    var model = disciplinePageModel.getModel().body;
                    var Discipline = selectedDFY.getDFY().body;

                    for (var i = 0; i < model.ControlPoints[Discipline.DFYId].length; i++) {
                        if (id == model.ControlPoints[Discipline.DFYId][i].ControlPointId) {
                            selectedCP.setCP(model.ControlPoints[Discipline.DFYId][i]);
                            break;
                        }
                    }
                }
            };

        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/cp.html',
        scope: {
            cp: '='
        }
    };
});