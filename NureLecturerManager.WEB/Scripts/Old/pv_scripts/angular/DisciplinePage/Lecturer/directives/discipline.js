﻿disciplinesApp.directive('discipline', function (disciplinePageModel, selectedDFY, selectedCP) {
// it is dfy
    return {
        link: function (scope, element, attrs) {
            scope.select = function (e) {
                e.preventDefault();
                var disc = selectedDFY.getDFY();

                // Find new selected DFY and select it. Deselection is in setDFY
                var id = scope.discipline.DFYId;  
                if (disc.body == null || id != disc.body.DFYId) {
                    var model = disciplinePageModel.getModel().body;

                    for (var i = 0; i < model.Disciplines.length; i++) {
                        if (id == model.Disciplines[i].DFYId) {
                            selectedDFY.setDFY(model.Disciplines[i]);
                            selectedCP.setCP(null);
                            break;
                        }
                    }
                }
            };

        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/discipline.html',
        scope: {
            discipline: '='
        }
    };
});