﻿disciplinesApp.directive('discipline', function (disciplinePageModel, selectedDiscipline, selectedDFY) {
    return {
        link: function (scope, element, attrs) {
            scope.select = function (e) {
                e.preventDefault();
                var disc = selectedDiscipline.getDiscipline();

                // Find new selected discipline and select it. Deselection is in setDiscipline
                var id = scope.discipline.DisciplineId;  
                if (disc.body == null || id != disc.body.DisciplineId) {
                    var model = disciplinePageModel.getModel();

                    for (var i = 0; i < model.Disciplines.length; i++) {
                        if (id == model.Disciplines[i].DisciplineId) {
                            selectedDiscipline.setDiscipline(model.Disciplines[i]);
                            selectedDFY.setDFY(null);
                            break;
                        }
                    }
                }
            };

        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/discipline.html',
        scope: {
            discipline: '='
        }
    };
});