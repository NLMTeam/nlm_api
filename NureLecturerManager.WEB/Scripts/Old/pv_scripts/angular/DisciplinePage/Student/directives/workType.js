﻿disciplinesApp.directive('workType', function (disciplinePageModel, selectedCP) {
    return {
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/workType.html',
        scope: {
            workType: '=',
            model: '='
        }
    };
});