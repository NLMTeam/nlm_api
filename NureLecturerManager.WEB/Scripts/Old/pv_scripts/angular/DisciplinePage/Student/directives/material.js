﻿disciplinesApp.directive('material', function (disciplinePageModel, selectedCP) {
    return {
        link: function(scope, element, attrs){
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/material.html',
        scope: {
            material: '='
        }
    };
});