﻿disciplinesApp.factory('disciplinePageModel', function ($http, selectedDiscipline, selectedDFY, selectedCP) {
    model = null;

    return {

        getModel: function () {
            return model;
        },

        setModel: function (value) {
            model = value;
        }
    };
});