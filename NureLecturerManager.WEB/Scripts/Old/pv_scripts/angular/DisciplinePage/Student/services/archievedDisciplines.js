﻿disciplinesApp.factory('archievedDisciplines', function ($http) {
    archModel = null;

    return {

        getModel: function () {
            if (archModel == undefined || archModel == null) {
                return $http.get('/Discipline/GetStudentsAchievedModel').success(function (data) {
                    archModel = data;
                    for (var cp in archModel.ControlPoints) {
                        if (cp.length == undefined) {
                            break;
                        }
                        else {
                            for (var i = 0; i < archModel.ControlPoints[cp].length; i++) {
                                var dateStr = archModel.ControlPoints[cp][i].Date.split('/');
                                var dateStr2 = archModel.ControlPoints[cp][i].Date.split('.');
                                if (dateStr.length == 3)
                                    archModel.ControlPoints[cp][i].Date = new Date(dateStr[2], dateStr[0] * 1 - 1, dateStr[1]);
                                else
                                    archModel.ControlPoints[cp][i].Date = new Date(dateStr2[2], dateStr2[1] * 1 - 1, dateStr2[0]);
                            }
                        }
                    }
                })
                .error(function (err) {
                });
            }
            else {
                return archModel;
            }
        },
    };
});