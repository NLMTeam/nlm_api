﻿$(document).ready(function () {
    
    // response focuse

    $('#response-modal').on('shown.bs.modal', function () {
        $('#response-ok').focus();
    });

    // discipline focuses

    $('#disc-add-modal').on('shown.bs.modal', function () {
        $('#add-disc').focus();
    });

    $('#disc-del-modal').on('shown.bs.modal', function () {
        $('#del-cancel-btn').focus();
    });

    $('#disc-edit-modal').on('shown.bs.modal', function () {
        $('#edit-disc').focus();
    });

    // dfy focuses

    $('#dfy-add-modal').on('shown.bs.modal', function () {
        $('#dfy-year').focus();
    });

    $('#dfy-del-modal').on('shown.bs.modal', function () {
        $('#del-dfy-cancel-btn').focus();
    });

    $('#dfy-edit-modal').on('shown.bs.modal', function () {
        $('#edit-dfy-save').focus();
    });

    $('#dfy-continue-modal').on('shown.bs.modal', function () {
        $('#con-year').focus();
    });
    // part focuses

    $('#part-add-modal').on('shown.bs.modal', function () {
        $('#group-list').focus();
    });

    $('#part-del-modal').on('shown.bs.modal', function () {
        $('#part-del-cancel-btn').focus();
    });

    $('#part-edit-modal').on('shown.bs.modal', function () {
        $('#edit-group-list').focus();
    });

    // cp focuses

    $('#cp-add-modal').on('shown.bs.modal', function () {
        $('#cp-number').focus();
    });

    $('#cp-del-modal').on('shown.bs.modal', function () {
        $('#cp-del-cancel-btn').focus();
    });

    $('#cp-edit-modal').on('shown.bs.modal', function () {
        $('#edit-cp-number').focus();
    });

    // wt focuses

    $('#workType-add-modal').on('shown.bs.modal', function () {
        $('#worktype-description').focus();
    });

    $('#workType-del-modal').on('shown.bs.modal', function () {
        $('#wt-del-cancel-btn').focus();
    });

    $('#workType-edit-modal').on('shown.bs.modal', function () {
        $('#edit-worktype-description').focus();
    });

    // material focuses

    $('#mat-add-modal').on('shown.bs.modal', function () {
        $('#mat-file-load').focus();
    });

    $('#mat-del-modal').on('shown.bs.modal', function () {
        $('#mat-del-cancel-btn').focus();
    });

    $('#mat-edit-modal').on('shown.bs.modal', function () {
        $('#newMatName').focus();
    });
});