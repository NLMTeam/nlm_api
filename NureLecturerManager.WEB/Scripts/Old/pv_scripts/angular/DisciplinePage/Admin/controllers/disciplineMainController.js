﻿disciplinesApp.controller('disciplineMainController', function ($scope, $http,
                            disciplinePageModel, selectedDiscipline, selectedDFY,
                            selectedCP, editDiscipline, editCP, responseService, editDfy, years) {


    $scope.ready = false;
        
    years.init();
    var model = disciplinePageModel.getModel();

    if (typeof (model.success) != "undefined") {
        model.success(function (data) {
            $scope.model = data;

            // selection services initialization
            if ($scope.model.Disciplines.length > 0){
                selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                
                if ($scope.model.DFYs[$scope.currentDiscipline.body.DisciplineId] != undefined &&
                        $scope.model.DFYs[$scope.currentDiscipline.body.DisciplineId].length != 0) {
                        
                    selectedDFY.setDFY($scope.model.DFYs[$scope.currentDiscipline.body.DisciplineId][0]);

                                    $scope.currentDFY = selectedDFY.getDFY();

                    if ($scope.model.ControlPoints[$scope.currentDFY.body.DFYId] &&
                            $scope.model.ControlPoints[$scope.currentDFY.body.DFYId].length != 0) {

                        selectedCP.setCP($scope.model.ControlPoints[$scope.currentDFY.body.DFYId][0]);
                    }
                    else {
                        selectedCP.setCP(null);
                    }
                }
                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                $scope.currentDFY = selectedDFY.getDFY();
                $scope.currentCP = selectedCP.getCP();
            }
            else{
                selectedDiscipline.setDiscipline(null);
                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
            }
            
            $scope.ready = true;
        });
    }
    else {
        $scope.model = model;

        selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
        $scope.currentDiscipline = selectedDiscipline.getDiscipline();
    }
    
    // Discipline modals

    $scope.addModal = function () {
        $('#disc-add-modal').modal('show');
    }
       
    $scope.delModal = function () {
        $('#disc-del-modal').modal('show');
    }

    // DFY modals

    $scope.addDFYModal = function () {
        $('#dfy-add-modal').modal('show');
    }

    $scope.delDFYModal = function () {
        $('#dfy-del-modal').modal('show');
    }

    $scope.continueDFYClick = function () {
        $('#dfy-continue-modal').modal('show');
    }

    // CP modals 

    $scope.addCPModal = function () {
        $('#cp-add-modal').modal('show');
    }

    $scope.delCPModal = function () {
        $('#cp-del-modal').modal('show');
    }

    // Work types modals 

    $scope.addWorkTypeModal = function () {
        $('#workType-add-modal').modal('show');
    }

    $scope.delWorkTypeModal = function () {
        $('#workType-del-modal').modal('show');
    }
    
    $scope.editDiscClick = function () {
        editDiscipline.setEditDiscipline({
            DisciplineId: $scope.currentDiscipline.body.DisciplineId,
            Name: $scope.currentDiscipline.body.Name,
        });

        $('#disc-edit-modal').modal();
    };

    $scope.editDFYClick = function () {
        editDfy.setEditDFY({
            DFYId: $scope.currentDFY.body.DFYId,
            Lecturers: $scope.currentDFY.body.LecturerNames,
            Assistants: $scope.currentDFY.body.AssistantsNames,
            Year: $scope.currentDFY.body.Year,
            Semester: $scope.currentDFY.body.Semester,
        });
        $('#dfy-edit-modal').modal();
    };

    $scope.editCPClick = function () {
        editCP.setEditCP({
            Id: $scope.currentCP.body.ControlPointId,
            Number: $scope.currentCP.body.Number,
            Name: $scope.currentCP.body.Name,
            Date: $scope.currentCP.body.Date,
            MinMark: $scope.currentCP.body.MinMark,
            MaxMark: $scope.currentCP.body.MaxMark
        });


        $('#cp-edit-modal').modal();
    };

    $scope.response = responseService.getResponse();

    var now = new Date();
    var currentYear = now.getFullYear();
    $scope.years = [{ Pair: currentYear - 2 + "-" + (currentYear - 1) },
                    { Pair: currentYear - 1 + "-" + currentYear },
                    { Pair: currentYear + "-" + (currentYear + 1) },
                    { Pair: currentYear + 1 + "-" + (currentYear + 2) }];


    $http.get('/Discipline/GetGroups').success(function (data) {
        $scope.groups = data;

        $http.get('/Discipline/GetLecturers').success(function (data) {
            $scope.lecturers = data;

        });

    }).error(function (err) {
        responseService.setResponse('Проверьте соединение');
        $('#responseModal').modal('show');
    });

    $scope.prolongYears = years.getYearsForProlong();
    $scope.editYears = years.getYearsForEdit();
});