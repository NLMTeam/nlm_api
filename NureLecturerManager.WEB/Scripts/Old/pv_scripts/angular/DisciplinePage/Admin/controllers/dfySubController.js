﻿disciplinesApp.controller('dfySubController', function ($scope, $http, selectedDiscipline, selectedDFY, selectedCP, disciplinePageModel,
                                                               editDfy, responseService) {

    $scope.editDfy = editDfy.getEditDFY();

    // CUD models
    $scope.newDFY = {
        lecturers: [],
        year: '',
        semester: ''
    };

    $scope.conDFY = {
        Year: '',
        Semester: ''
    }

    $scope.currentDfy = selectedDFY.getDFY().body;

     //CUD methods
     //Create DFY
    $scope.create = function (e) {
        e.preventDefault();
        $('#dfy-add-modal').modal('hide');
        $http.post('/Discipline/CreateDFY', 
                        {
                            DisciplineId: selectedDiscipline.getDiscipline().body.DisciplineId,
                            Lecturers: $scope.newDFY.lecturers,
                            Assistants: $scope.newDFY.assistants,
                            Year: $scope.newDFY.year,
                            Semester: $scope.newDFY.semester
                        }).success(function (data) {

                if (data.DFYId != undefined && data.Year != undefined && data.Semester != undefined) {
                    responseService.setResponse('Привязка успешно создана');
                    $('#response-modal').modal('show');
                        disciplinePageModel.addDFY(data);
                        $scope.newDFY.lecturers = [];
                        $scope.newDFY.assistants = [];
                        $scope.newDFY.year = '';
                        $scope.newDFY.semester = '';
                }
                else {
                    responseService.setResponse(data);
                    $('#response-modal').modal('show');
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

     //Delete discipline
    $scope.delete = function () {
        var currentDFY = selectedDFY.getDFY().body;
        var model = disciplinePageModel.getModel();

        if (currentDFY == null) {
            responseService.setResponse('Выберите привязку для удаления!');
            $('#response-modal').modal();
            return;
        }

        $('#dfy-del-modal').modal('hide');
        $http.post('/Discipline/DeleteDFY?dfyId=' + currentDFY.DFYId).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Привязка успешно удалена.') {
                disciplinePageModel.deleteDFY(currentDFY.DFYId);
                selectedCP.setCP(null);
                selectedDFY.setDFY(null);
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

    var myColCompare = function (selArr, editArr) {
        if (editArr == undefined)
            return true;
        if (selArr.length != editArr.length)
            return true;

        var isHere = false;
        for (var i = 0; i < selArr.length; i++) {
            isHere = false;
            for (var j = 0; j < editArr.length; j++) {
                if (selArr[i].Id == editArr[j]) {
                    isHere = true;
                    break;
                }
            }
            if (isHere == false)
                return true;
        }

        return false;
    };

    $scope.edit = function (e) {
        e.preventDefault();
        //$scope.editLit.LiteratureId = selectedLiterature.getLiterature().body.LiteratureId;

        $('#dfy-edit-modal').modal('hide');

        var dfy = selectedDFY.getDFY().body;
        if (dfy == null) {
            responseService.setResponse("Ошибка в моделях страницы. Обратитесь в техподдержку.");
        }

        if ($scope.editDfy.Year != dfy.Year || $scope.editDfy.Semester != dfy.Semester || myColCompare(dfy.LecturerNames, $scope.editDfy.Lecturers) ||
                                            myColCompare(dfy.AssistantsNames, $scope.editDfy.Assistants)) {
            $http.post('/Discipline/EditDFY', {
                DFYId: selectedDFY.getDFY().body.DFYId,
                Lecturers: $scope.editDfy.Lecturers,
                Assistants: $scope.editDfy.Assistants,
                Year: $scope.editDfy.Year,
                Semester: $scope.editDfy.Semester
            }).success(function (data) {
                responseService.setResponse(data);
                $('#response-modal').modal('show');
                if (data == 'Привязка успешно отредактирована') {

                    var model = disciplinePageModel.getModel();
                    var currentDFY = selectedDFY.getDFY().body;
                    var discId = selectedDiscipline.getDiscipline().body.DisciplineId;

                    for (var i = 0; i < model.DFYs[discId].length ; i++) {
                        if (model.DFYs[discId][i].DFYId == $scope.editDfy.DFYId) {
                            model.DFYs[discId][i].Year = $scope.editDfy.Year;
                            model.DFYs[discId][i].Semester = $scope.editDfy.Semester;
                            model.DFYs[discId][i].LecturerNames = [];
                            model.DFYs[discId][i].AssistantsNames = [];

                            if ($scope.editDfy.Lecturers != null) {
                                for (var k = 0; k < $scope.editDfy.Lecturers.length; k++) {
                                    for (var j = 0; j < $scope.lecturers.length; j++) {
                                        if ($scope.lecturers[j].Id == $scope.editDfy.Lecturers[k]) {
                                            model.DFYs[discId][i].LecturerNames.push({
                                                FIO: $scope.lecturers[j].FIO,
                                                Id: $scope.lecturers[j].Id
                                            });
                                        }
                                    }
                                }
                            }

                            if ($scope.editDfy.Assistants != null) {
                                for (var k = 0; k < $scope.editDfy.Assistants.length; k++) {
                                    for (var j = 0; j < $scope.lecturers.length; j++) {
                                        if ($scope.lecturers[j].Id == $scope.editDfy.Assistants[k]) {
                                            model.DFYs[discId][i].AssistantsNames.push({
                                                FIO: $scope.lecturers[j].FIO,
                                                Id: $scope.lecturers[j].Id
                                            });
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    $scope.editDfy.Year = "";
                    $scope.editDfy.Semester = '';
                    $scope.editDfy.Lecturers = [];
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
        }
        else {
            responseService.setResponse('Прежние данные сохранены.');
            $('#response-modal').modal('show');
        }
    }

    $scope.continue = function (e) {
        e.preventDefault();
        $('#dfy-continue-modal').modal('hide');
        $http.post('/Discipline/ProlongDFY?DFYId=' + selectedDFY.getDFY().body.DFYId + "&year=" +
                                                     $scope.conDFY.Year + "&semester=" + $scope.conDFY.Semester).success(function (data) {
                            if (data.DFY != undefined && data.ControlPoints != undefined
                                && data.WorkTypes != undefined &&
                                data.Materials != undefined) {

                                console.log(data);

                                    responseService.setResponse('Привязка успешно продлена');
                                    $('#response-modal').modal('show');
                                    disciplinePageModel.addDFY(data.DFY);
                                    var model = disciplinePageModel.getModel();
                                    var id = data.DFY.DFYId;
                                    model.ControlPoints[id] = data.ControlPoints;
                                    
                                    for (var i = 0; i < data.ControlPoints.length; i++) {
                                        model.WorkTypes[data.ControlPoints[i].ControlPointId] = data.WorkTypes[data.ControlPoints[i]
                                                                                                .ControlPointId];

                                        for (var j=0; j<data.WorkTypes[data.ControlPoints[i].ControlPointId].length; j++){
                                            model.Materials[data.WorkTypes[data.ControlPoints[i].ControlPointId][j].WorkTypeId] = 
                                                            data.Materials[data.WorkTypes[data.ControlPoints[i].ControlPointId][j].WorkTypeId];
                                   
                                        }
                                    }

                                    $scope.conDFY.Year = '';
                                    $scope.conDFY.Semester = '';
                            }
                            else {
                                responseService.setResponse(data);
                                $('#response-modal').modal('show');
                            }
                        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }
});