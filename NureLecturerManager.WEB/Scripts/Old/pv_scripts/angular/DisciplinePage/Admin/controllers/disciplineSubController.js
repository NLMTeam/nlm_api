﻿disciplinesApp.controller('disciplineSubController', function ($scope, $http, selectedDiscipline, selectedDFY, selectedCP, disciplinePageModel,
                                                               editDiscipline, responseService) {

    $scope.editDisc = editDiscipline.getEditDiscipline();

    // CUD models
    $scope.newDisc = {
        Name: "",
        IsCourseProject: "false"
    };

    $scope.currentDiscipline = selectedDiscipline.getDiscipline().body;

     //CUD methods
     //Create discipline
    $scope.create = function (e) {
        e.preventDefault();
            $('#disc-add-modal').modal('hide');
        $http.post('/Discipline/CreateDiscipline?disciplineName=' + $scope.newDisc.Name + '&isCourseProject=' +
                                                                    $scope.newDisc.IsCourseProject).success(function (data) {

                if (data.DisciplineId != undefined && data.Name != undefined) {
                    responseService.setResponse('Дисциплина успешно создана');
                    $('#response-modal').modal('show');

                    //$http.get('/Discipline/GetDisciplineChanges?disciplineName=' + $scope.newDisc.Name).success(function (data) {
                        disciplinePageModel.addDiscipline(data);
                    //})
                    //.error(function (err) {
                    //    responseService.setResponse(err;
                    //    $('#response-modal').modal('show');
                    //});
                    $scope.newDisc.Name = '';
                }
                else {
                    responseService.setResponse(data);
                    $('#response-modal').modal('show');
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

     //Delete discipline
    $scope.delete = function () {
        var currentDiscipline = selectedDiscipline.getDiscipline().body;
        var model = disciplinePageModel.getModel();

        if (currentDiscipline == null) {
            responseService.setResponse('Выберите дисциплину для удаления!');
            $('#response-modal').modal();
            return;
        }

        $('#disc-del-modal').modal('hide');
        $http.post('/Discipline/DeleteDiscipline?disciplineId=' + currentDiscipline.DisciplineId).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Дисциплина успешно удалена.') {
                disciplinePageModel.deleteDiscipline(currentDiscipline.DisciplineId);
                selectedCP.setCP(null);
                selectedDFY.setDFY(null);
                selectedDiscipline.setDiscipline(null);
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

    $scope.edit = function (e) {
        e.preventDefault();
        //$scope.editLit.LiteratureId = selectedLiterature.getLiterature().body.LiteratureId;

        $('#disc-edit-modal').modal('hide');

        if ($scope.editDisc.Name != selectedDiscipline.getDiscipline().body.Name) {
            $http.post('/Discipline/RenameDiscipline?discId=' + $scope.editDisc.DisciplineId + '&discName=' + $scope.editDisc.Name).success(function (data) {
                responseService.setResponse(data);
                $('#response-modal').modal('show');
                if (data == 'Дисциплина успешно переименована.') {

                    var model = disciplinePageModel.getModel();
                    var currentDiscipline = selectedDiscipline.getDiscipline().body;

                    for (var i = 0; i < model.Disciplines.length ; i++) {
                        if (model.Disciplines[i].DisciplineId == $scope.editDisc.DisciplineId) {
                            model.Disciplines[i].Name = $scope.editDisc.Name;
                            break;
                        }
                    }
                    $scope.editDisc.Name = "";
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
        }
        else {
            responseService.setResponse('Прежние данные сохранены.');
            $('#response-modal').modal('show');
        }
    }
});