﻿disciplinesApp.factory('responseService', function () {
    response = {
        r: ''
    };

    return {
        getResponse: function () {
            return response;
        },
        setResponse: function (value) {
            response.r = value
        }
    };
});