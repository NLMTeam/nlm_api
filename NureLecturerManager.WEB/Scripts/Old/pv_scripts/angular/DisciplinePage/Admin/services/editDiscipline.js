﻿disciplinesApp.factory('editDiscipline', function () {
    editDiscipline = {
        DisciplineId: '',
        Name: ''
    };

    return {
        getEditDiscipline: function () {
            return editDiscipline;
        },
        setEditDiscipline: function (value) {
            editDiscipline.DisciplineId = value.DisciplineId;
            editDiscipline.Name = value.Name;
        }
    }
});