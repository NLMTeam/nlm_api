﻿disciplinesApp.factory('editWorkType', function () {
    editWorkType = {
        Name: '',
        MinMark: '',
        MaxMark: ''
    };


    return {
        getEditWorkType: function () {
            return editWorkType;
        },
        setEditWorkType: function (value) {
            editWorkType.Name = value.Name;
            editWorkType.MinMark = value.MinMark;
            editWorkType.MaxMark = value.MaxMark;
        }
    }
});