﻿disciplinesApp.factory('editDfy', function () {
    editDFY = {
        DFYId: '',
        Lecturers: [],
        Year: '',
        Semester: ''
    };

    return {
        getEditDFY: function () {
            return editDFY;
        },
        setEditDFY: function (value) {
            editDFY.DFYId = value.DFYId;
            editDFY.Lecturers = [];
            for (var i = 0; i < value.Lecturers.length; i++)
                editDFY.Lecturers.push(value.Lecturers[i].Id);

            editDFY.Assistants = [];
            for (var i = 0; i < value.Assistants.length; i++)
                editDFY.Assistants.push(value.Assistants[i].Id);
            editDFY.Year = value.Year;
            editDFY.Semester = value.Semester;
        }
    }
});