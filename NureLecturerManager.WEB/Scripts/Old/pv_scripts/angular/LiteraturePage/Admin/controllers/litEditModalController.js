﻿literatureApp.controller('litEditModalController', function ($scope, $http, editLiterature, literatureModel, selectedLiterature, selectedDiscipline, responseService) {

    $scope.editLit = editLiterature.getEditLiterature();

    //$scope.editLit = {};
    //$scope.editLit.Id = -1;
    //$scope.editLit.Name = "";

    $scope.save = function (e) {
        e.preventDefault();
        
        //$scope.editLit.LiteratureId = selectedLiterature.getLiterature().body.LiteratureId;

        $('#edit-modal').modal('hide');

        if ($scope.editLit.Name != selectedLiterature.getLiterature().body.Name) {
            $http.post('/Literature/EditBook?bookId=' + $scope.editLit.LiteratureId + '&Name=' + $scope.editLit.Name).success(function (data) {
                responseService.setResponse(data);
                $('#response-modal').modal('show');

                if (data == 'Книга успешно отредактирована.') {

                    var model = literatureModel.getLiterature();
                    var currentDiscipline = selectedDiscipline.getDiscipline().body;

                    for (var i = 0; i < model.Literature[currentDiscipline.DisciplineId].length ; i++) {
                        if (model.Literature[currentDiscipline.DisciplineId][i].LiteratureId == $scope.editLit.LiteratureId) {
                            model.Literature[currentDiscipline.DisciplineId][i].Name = $scope.editLit.Name;
                        }
                    }
                    $scope.editLit.Name = "";
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
        }
        else {
            responseService.setResponse('Прежние данные сохранены.');
            $('#response-modal').modal('show');
        }
    }
});
