﻿literatureApp.controller('litDelModalController', function ($scope, $http, literatureModel, selectedDiscipline, selectedLiterature, responseService) {

    $scope.delete = function () {
        var delLit = selectedLiterature.getLiterature().body;
        if (delLit == null)
            return;

        $('#del-modal').modal('hide');
        $http.post('/Literature/DeleteBook?bookId=' + delLit.LiteratureId).success(function (data) {

            var model = literatureModel.getLiterature();
            var currentDiscipline = selectedDiscipline.getDiscipline().body;

            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Книга успешно удалена.') {
                literatureModel.deleteLiterature(delLit.LiteratureId);
                selectedLiterature.setLiterature(null);
            }
        }).error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }
});
