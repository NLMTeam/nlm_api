﻿$(document).ready(function () {
    
    // response focuse

    $('#response-modal').on('shown.bs.modal', function () {
        $('#response-ok').focus();
    });

    // literature focuses
    $('#add-modal').on('shown.bs.modal', function () {
        $('#add-lit').focus();
    });

    $('#del-modal').on('shown.bs.modal', function () {
        $('#lit-del-cancel-btn').focus();
    });

    $('#edit-modal').on('shown.bs.modal', function () {
        $('#edit-lit').focus();
    });

    $('#load-lit-modal').on('shown.bs.modal', function () {
        $('#load-lit-file').focus();
    });

    // material focuses

    $('#mat-add-modal').on('shown.bs.modal', function () {
        $('#mat-lit-file').focus();
    });

    $('#mat-del-modal').on('shown.bs.modal', function () {
        $('#mat-del-cancel-btn').focus();
    });

    $('#mat-edit-modal').on('shown.bs.modal', function () {
        $('#newMatName').focus();
    });
});