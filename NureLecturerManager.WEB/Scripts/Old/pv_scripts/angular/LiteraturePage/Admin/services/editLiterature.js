﻿literatureApp.factory('editLiterature', function () {
    editLiterature = {
        LiteratureId: '',
        Name: '',
        DisciplineId: ''
    };

    return {
        getEditLiterature: function () {
            return editLiterature;
        },
        setEditLiterature: function (value) {
            editLiterature.LiteratureId = value.LiteratureId;
            editLiterature.Name = value.Name;
            editLiterature.DisciplineId = value.DisciplineId;
        }
    }
    
});