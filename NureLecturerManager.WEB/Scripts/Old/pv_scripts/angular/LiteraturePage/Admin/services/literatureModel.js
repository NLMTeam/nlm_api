﻿literatureApp.factory('literatureModel', function ($http, selectedDiscipline) {
    litModel = null;

    return {

        getLiterature: function () {
            if (litModel == undefined || litModel == null)
                return $http.get('/Literature/GetAdminLiterature').success(function (data) {
                    litModel = data;
                })
                .error(function (err) {
                });

            else {
                return litModel;
            }
        },

        setLiterature: function(discId, data){
            litModel.Literature[discId] = data;
        },

        addLiterature: function (data) {
            if (data != null) {
                var currentDiscipline = selectedDiscipline.getDiscipline().body;
                if (litModel.Literature[currentDiscipline.DisciplineId] != null)
                    litModel.Literature[currentDiscipline.DisciplineId].push(data);
                else {
                    litModel.Literature[currentDiscipline.DisciplineId] = [];
                    litModel.Literature[currentDiscipline.DisciplineId].push(data);
                }
            }
        },
        
        deleteLiterature: function (id) {
            var disciplineId = selectedDiscipline.getDiscipline().body.DisciplineId;
            for (var i = 0; i < litModel.Literature[disciplineId].length; i++)
                if (litModel.Literature[disciplineId][i].LiteratureId == id)
                    litModel.Literature[disciplineId].splice(i, 1);
        },

        addMaterial: function (material, disciplineId) {
            if (material == null || disciplineId == undefined)
                return;

            if (litModel.Materials[disciplineId] == null || litModel.Materials[disciplineId] == undefined)
                litModel.Materials[disciplineId] = [];

            litModel.Materials[disciplineId].push(material);
        },

        deleteMaterial: function (id, disciplineId) {
            if (id == undefined || id == -1 || disciplineId == undefined || disciplineId == -1)
                return;

            for (var i = 0; i < litModel.Materials[disciplineId].length; i++) {
                if (litModel.Materials[disciplineId][i].Id == id)
                {
                    litModel.Materials[disciplineId].splice(i, 1);
                    break;
                }
            }
        },

        editMaterial: function (id, disciplineId, name) {
            if (id == undefined || id == -1)
                return;
            for (var i = 0; i < litModel.Materials[disciplineId].length; i++) {
                if (litModel.Materials[disciplineId][i].Id == id) {
                    litModel.Materials[disciplineId][i].Type = name;
                    break;
                }
            }
        }
    };
});