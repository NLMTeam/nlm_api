﻿literatureApp.controller('literatureMainController', function ($scope, $http, literatureModel, selectedDiscipline, selectedLiterature,
                                                                                editLiterature, responseService, archievedLiterature,
                                                                                currentLiterature, tracking) {

    $scope.ready = false;
    $scope.response = responseService.getResponse();

    var literature = currentLiterature.getModel();

    tracking.value = 'now';
    $scope.tracking = tracking.value;

    if (typeof (literature.success) != "undefined") {
        literature.success(function (data) {
            $scope.model = data;
            literatureModel.setModel(data);

            selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
            $scope.currentDiscipline = selectedDiscipline.getDiscipline();

            selectedLiterature.setLiterature(null);
            $scope.selectedLiterature = selectedLiterature.getLiterature();
        });
    }
    else {
        $scope.model = literature;
        literatureModel.setModel(literature);

        selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
        $scope.currentDiscipline = selectedDiscipline.getDiscipline();

        selectedLiterature.setLiterature(null);
        $scope.selectedLiterature = selectedLiterature.getLiterature();
    }
    
    $scope.ready = true;

    $scope.editClick = function (e) {
        e.preventDefault();
        if ($scope.tracking == 'past' || selectedLiterature.getLiterature().body == null || !selectedDiscipline.getDiscipline().body.IsAdmin) {
            return;
        }

        editLiterature.setEditLiterature({
            DisciplineId: $scope.selectedLiterature.body.DisciplineId,
            LiteratureId: $scope.selectedLiterature.body.LiteratureId,
            Name: $scope.selectedLiterature.body.Name,
        });

        $('#edit-modal').modal();
    };

    $scope.getTxt = function (e) {
        e.preventDefault();

        if (selectedDiscipline.getDiscipline().body == null)
            return;

        if ($scope.model.Literature[$scope.currentDiscipline.body.DisciplineId] != null &&
            $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId] != undefined &&
            $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId].length > 0) {
            var str = '';
            for (var i = 0; i < $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId].length; i++)
                str += $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId][i].Name.trim() + '*8*--*-_*';

            window.open('/Literature/CheckBooksForEmpty?data=' + str, '_blank');
        } else {
            //responseService.setResponse("Список литературы по даной дисциплине пуст.");
            //$('#response-modal').modal();
            return;
        }
    };

    $scope.addMaterialModal = function () {
        if (selectedDiscipline.getDiscipline().body == null || !selectedDiscipline.getDiscipline().body.IsAdmin  || $scope.tracking == 'past')
            return;

        $('#mat-add-modal').modal('show');
    };

    $scope.addLitModal = function (e) {
        e.preventDefault();
        if (selectedDiscipline.getDiscipline().body == null || !selectedDiscipline.getDiscipline().body.IsAdmin || $scope.tracking == 'past' )
            return;
        $('#add-modal').modal();
    };

    $scope.delLitModal = function (e) {
        e.preventDefault();
        if ($scope.tracking == 'past' || selectedLiterature.getLiterature().body == null || !selectedDiscipline.getDiscipline().body.IsAdmin)
            return;
        $('#del-modal').modal();
    };

    //$('#ed-lit-
    $scope.loadLitModal = function (e) {
        e.preventDefault();
        if ($scope.tracking == 'past' || selectedDiscipline.getDiscipline().body == null || !selectedDiscipline.getDiscipline().body.IsAdmin)
            return;
        $('#load-lit-modal').modal();
    };

    $scope.getArchievedModel = function () {
        $scope.tracking = 'past';
        tracking.value = 'past';

        var model = archievedLiterature.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                $scope.model = data;
                literatureModel.setModel(data);

                // selection services initialization
                if ($scope.model.Disciplines.length > 0) {
                    selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                }
                else {
                    selectedDiscipline.setDiscipline(null);
                }

                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                selectedLiterature.setLiterature(null);
            });
        }
        else {
            //disciplinePageModel.setModel(model);

            $scope.model = model;
            literatureModel.setModel(model);

            if ($scope.model.Disciplines.length > 0) {
                selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
            }
            else {
                selectedDiscipline.setDiscipline(null);
            }

            $scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectedLiterature.setLiterature(null);
        }
    }

    $scope.getCurrentModel = function () {
        $scope.tracking = 'now';
        tracking.value = 'now';

        var model = currentLiterature.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                $scope.model = data;
                literatureModel.setModel(data);

                // selection services initialization
                if ($scope.model.Disciplines.length > 0) {
                    selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                }
                else {
                    selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                }

                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                selectedLiterature.setLiterature(null);
            });
        }
        else {
            //disciplinePageModel.setModel(model);

            $scope.model = model;
            literatureModel.setModel(model);

            if ($scope.model.Disciplines.length > 0) {
                selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
            }
            else {
                selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
            }

            $scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectedLiterature.setLiterature(null);
        }
    }
}); 

