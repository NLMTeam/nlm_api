﻿  literatureApp.controller('literatureAddModalController', function ($scope, $http, selectedDiscipline, literatureModel, responseService) {
    $scope.newLit = {
        Name : ''
    };

    $scope.save = function (e) {
        e.preventDefault();
        
        var currentDiscipline = selectedDiscipline.getDiscipline().body;
        var model = literatureModel.getLiterature();

        $('#add-modal').modal('hide');
        $http.post('/Literature/CreateBook?disciplineId=' + currentDiscipline.DisciplineId + "&name=" + $scope.newLit.Name).success(function (data) {
            if (data.LiteratureId != undefined && data.Name != undefined) {
                literatureModel.addLiterature(data);
                $scope.newLit.Name = '';

                responseService.setResponse('Книга успешно добавлена');
                $('#response-modal').modal('show');
            }
            else {
                responseService.setResponse(data);
                $('#response-modal').modal('show');
            }
        }).error(function(err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }
});
