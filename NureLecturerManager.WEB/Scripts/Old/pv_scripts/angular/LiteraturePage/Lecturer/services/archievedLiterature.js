﻿literatureApp.factory('archievedLiterature', function ($http, responseService) {
    archModel = null;

    return {

        getModel: function () {
            if (archModel == undefined || archModel == null) {
                return $http.get('/Literature/GetLecturersArchievedLiterature').success(function (data) {
                    archModel = data;
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });
            }
            else {
                return archModel;
            }
        },
    };
});