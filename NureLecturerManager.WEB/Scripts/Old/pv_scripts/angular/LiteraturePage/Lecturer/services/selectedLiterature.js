﻿literatureApp.factory('selectedLiterature', function () {
    literature = { body: null };

    return {
        getLiterature: function () {
            return literature;
        },
        setLiterature: function (value) {
            // Deselecting old value
            if (literature.body != null) {
                literature.body.activity = "";
            }

            // Selecting new value
            literature.body = value;
            if (value != null) {
                literature.body.activity = "bg-selected-warning";
            }
        }
    }
});