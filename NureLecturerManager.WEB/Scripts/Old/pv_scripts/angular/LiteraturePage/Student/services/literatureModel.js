﻿literatureApp.factory('literatureModel', function ($http) {
    litModel = null;

    return {

        getLiterature: function () {
                return litModel;
        },

        setModel: function (value) {
            litModel = value;
        }
    };
});