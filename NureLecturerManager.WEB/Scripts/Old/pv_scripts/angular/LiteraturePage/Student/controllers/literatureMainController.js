﻿literatureApp.controller('literatureMainController', function ($scope, $http, literatureModel, selectedDiscipline, responseService,
                                                                                archievedLiterature, currentLiterature) {

    $scope.ready = false;
    $scope.response = responseService.getResponse();

    var literature = currentLiterature.getModel();
    $scope.tracking = 'now';

    if (typeof (literature.success) != "undefined") {
        literature.success(function (data) {
            $scope.model = data;
            literatureModel.setModel(data);

            selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
            $scope.currentDiscipline = selectedDiscipline.getDiscipline();
        });
    }
    else {
        $scope.model = literature;
        literatureModel.setModel(literature);

        selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
        $scope.currentDiscipline = selectedDiscipline.getDiscipline();
    }
    
    $scope.ready = true;

    $scope.getTxt = function (e) {
        e.preventDefault();

        if ($scope.currentDiscipline.body != null && $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId] != null &&
            $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId] != undefined &&
            $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId].length > 0) {
            var str = '';
            for (var i = 0; i < $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId].length; i++)
                str += $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId][i].Name.trim() + '*8*--*-_*';

            window.open('/Literature/CheckBooksForEmpty?data=' + str, '_blank');
        } else {
            //responseService.setResponse("Список литературы по даной дисциплине пуст.");
            //$('#response-modal').modal();
            return;
        }
    };

    $scope.getArchievedModel = function () {
        $scope.tracking = 'past';

        var model = archievedLiterature.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                $scope.model = data;
                literatureModel.setModel(data);

                // selection services initialization
                if ($scope.model.Disciplines.length > 0) {
                    selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                    $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                }
            });
        }
        else {
            $scope.model = model;
            literatureModel.setModel(model);

            if ($scope.model.Disciplines.length > 0) {
                selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
            }
        }
    }

    $scope.getCurrentModel = function () {
        $scope.tracking = 'now';

        var model = currentLiterature.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                $scope.model = data;
                literatureModel.setModel(data);

                // selection services initialization
                if ($scope.model.Disciplines.length > 0) {
                    selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                    $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                }
            });
        }
        else {
            //disciplinePageModel.setModel(model);

            $scope.model = model;
            literatureModel.setModel(model);

            if ($scope.model.Disciplines.length > 0) {
                selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
            }
        }
    }
});

