﻿/// <reference path="literatureItem.js" />
literatureApp.directive('literature', function (selectedDiscipline, literatureModel) {
    return {
        restrict: 'E',
        template:
                '<li class="default-cursor lit-item" ng-bind="literature.Name"></li>',
        scope: {
            literature: '='
        }
    };
});