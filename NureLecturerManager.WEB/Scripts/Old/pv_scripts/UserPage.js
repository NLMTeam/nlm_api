﻿function readURL(input) {

    if (input.files && input.files[0]) {
        var arr = input.files[0].name.split('.');
        var ext = arr[arr.length - 1];
        console.log(ext);

        if (ext != 'png' && ext != 'jpg' && ext != 'bmp' && ext!='gif') {
            return;
            $('#load-image-btn').addClass('a-disabled');
        }

        $('#load-image-btn').removeClass('a-disabled');

        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInput").change(function () {
    readURL(this);
});

$("#load-image-btn").click(function (e) {
    e.preventDefault();
    var input = $('#imgInput')[0];
    console.log(input);
    if (!(input.files && input.files[0]))
        return;

    console.log(input.files[0]);
    var data = new FormData();
    data.append("file0", input.files[0]);

    console.log(data);
    $.ajax({
        type: "POST",
        url: '/Account/ChangeImage',
        contentType: false,
        processData: false,
        data: data,
        success: function (result) {
            window.location = '/Account/ImageWasChanged';
        },
        error: function (xhr, status, p3) {
            console.error(xhr.responseText);
        }
    });
});