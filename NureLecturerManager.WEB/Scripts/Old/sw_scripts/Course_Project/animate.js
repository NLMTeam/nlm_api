﻿var height = $('.zero-height').first().height();
$(".zero-height").height(0);
function displayInfo() {

    var timer = setInterval(function () {
        var actualHeight = $('.zero-height').first().height();
        if (actualHeight < height) {
            $('.zero-height').first().height(actualHeight + 1);
        }
        else {
            clearInterval(timer);
        }
    }, 20);

    return false;
}

function hideInfo() {

    var timer = setInterval(function () {
        var actualHeight = $('.zero-height').first().height();
        if (actualHeight > 0) {
            $('.zero-height').first().height(actualHeight - 1);
        }
        else {
            clearInterval(timer);
        }
    }, 20);

    return false;
}

window.hideInfoQuick = function () {
    $(".zero-height").height(0);
};

window.checkElementDisplay = function () {
    return $(".zero-height").height() > 0;
};
