﻿var app = angular.module('courseProjectApp');

app.service('selectService', function () {
    var obj = {};

    var year, group, discipline, courseProject, consolidation;

    obj.selectYear = function (value) {
        year = value;
    }
    obj.getYear = function () {
        return year;
    }

    obj.selectGroup = function (value) {
        group = value;
    }
    obj.getGroup = function () {
        return group;
    }

    obj.selectDiscipline = function (value) {
        discipline = value;
    }
    obj.getDiscipline = function () {
        return discipline;
    }

    obj.selectProject = function (value) {
        courseProject = value;
    }
    obj.getProject = function () {
        return courseProject;
    }

    obj.selectConsolidation = function (value) {
        consolidation = value;
    }
    obj.getConsolidation = function () {
        return consolidation;
    }

    return obj;
});