﻿$(document).ready(function () {
    
    // response focuse

    $('#MessageDialog').on('shown.bs.modal', function () {
        $('#response-ok').focus();
    });
    
    // delete focuse
    $('#ConfirmDeleteDialog').on('shown.bs.modal', function () {
        $('#del-cancel').focus();
    });
    // course project focuses

    $('#createCourseProjectDialog').on('shown.bs.modal', function () {
        $('#theme-cp').focus();
    });

    $('#editCourseProjectDialog').on('shown.bs.modal', function () {
        $('#edit-cp-btn').focus();
    });

    // consolidation focuses

    $('#createConsolidationModal').on('shown.bs.modal', function () {
        $('#createConsolidationGroupSelect').focus();
    });

    $('#editConsolidationModal').on('shown.bs.modal', function () {
        $('#edit-con').focus();
    });
});