﻿var courseProjectApp = angular.module('courseProjectApp');

function ModalCallback($scope, $rootScope, $http, selectService, communicationService, dataStorage) {

    communicationService.register('create consolidation', function (model) {
        $scope.Year = selectService.getYear();
        $scope.createdConsolidation = model;
        $scope.Students = model.Students;
        $scope.Groups = dataStorage.getGroups(selectService.getYear(), selectService.getDiscipline().DisciplineId);

        // default options
        if ($scope.Students.length > 0) {
            $scope.createdConsolidation.Student = $scope.Students[0];
        }

        if ($scope.Groups.length > 0) {
            $scope.createdConsolidation.Group = selectService.getGroup();
        }

        window.openModalId = 'createConsolidationModal';
        $('#createConsolidationModal').modal('show');
    });

    communicationService.register('consolidation edit', function (model) {
        $scope.Year = selectService.getYear();
        var serverData = model.Key;
        var fillData = model.Value;

        $scope.editedConsolidation = {};

        if (typeof (serverData.PassDate) != 'undefined' && serverData.PassDate != null) {
            var dateArr = serverData.PassDate.split(".");
            var dateArr2 = serverData.PassDate.split("/");
            var date;
            if (dateArr.length == 3)
                date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
            else
                date = new Date(+dateArr2[2], +dateArr2[0] - 1, +dateArr2[1]);
            $scope.editedConsolidation.PassDate = date;
        }
        else {
            $scope.editedConsolidation.PassDate = null;
        }

        if (typeof (serverData.Mark) != 'undefined' && serverData.Mark != null) {
            $scope.editedConsolidation.Mark = serverData.Mark;
        }
        else {
            $scope.editedConsolidation.Mark = null;
        }

        window.openModalId = 'editConsolidationModal';
        $('#editConsolidationModal').modal('show');
    });

    $scope.editConsolidation = function () {
        var form = $("#editConsForm");
        if (!form[0].checkValidity()) {
            form.find(":submit").click();
            return;
        }

        var discipline = selectService.getDiscipline();

        $http.post('/CourseProjects/EditConsolidation', {
            Mark: $scope.editedConsolidation.Mark, PassDate: $scope.editedConsolidation.PassDate, DisciplineId: discipline.DisciplineId
        }).success(function (message) {
            communicationService.execute('open message', [message]);

            var selectConsolidation = selectService.getConsolidation();

            if (typeof ($scope.editedConsolidation.PassDate) != 'undefined') {
                selectConsolidation.PassDate = $scope.editedConsolidation.PassDate;
            }
            if (typeof ($scope.editedConsolidation.Mark) != 'undefined') {
                selectConsolidation.Mark = $scope.editedConsolidation.Mark;
                if ($scope.editedConsolidation.PassDate.toString() == 'Invalid Date') {
                    selectConsolidation.PassDate = new Date();
                }
            }

        });
        $('#editConsolidationModal').modal('hide');
    }

    communicationService.register('Confirm consolidation delete', function () {

        var consolidation = selectService.getConsolidation();

        var project = selectService.getProject();
        var year = selectService.getYear();
        var group = selectService.getGroup();
        var discipline = selectService.getDiscipline();

        dataStorage.storeDisciplineConsolidations(discipline.DisciplineId, year, group, undefined);
        dataStorage.storeCourseProjectConsolidations(year, project.CourseProjectId, undefined);

        $http.post('/CourseProjects/DeleteConsolidation', { consolidationId: consolidation.ConsolidationId }).success(function (Message) {
            communicationService.execute('open message', [Message]);
            communicationService.execute('Delete consolidation success');
        });
    })

    $scope.loadYearInfo = function () {
        $http.get("/CourseProjects/GetYearInfo?year=" + $scope.createdConsolidation.Year).success(function (model) {
            // copy data
            $scope.createdConsolidation.Groups = model.Groups;
            $scope.createdConsolidation.Disciplines = model.Disciplines;
            $scope.createdConsolidation.Projects = model.CourseProjects;
            $scope.createdConsolidation.Students = model.Students;
            $scope.Students = model.Students;
            $scope.CourseProjects = model.CourseProjects;

            // default options
            $scope.createdConsolidation.Student = $scope.Students[0];
            $scope.createdConsolidation.Discipline = model.Disciplines[0];
            $scope.createdConsolidation.Group = model.Groups[0];
            $scope.createdConsolidation.Project = $scope.CourseProjects[0];
        });
    }

    $scope.loadGroupInfo = function () {
        if ($scope.createdConsolidation.Group == null) {
            $scope.Students = [];
            return;
        }
        $http.get("/CourseProjects/GetStudentsOfGroup?groupNum=" + $scope.createdConsolidation.Group).success(function (model) {
            // copy data
            $scope.Students = model;
        })
    }

    $scope.loadDisciplineInfo = function () {
        $http.get("/CourseProjects/GetDisciplineInfo?disciplineId=" + $scope.createdConsolidation.Discipline.DisciplineId).success(function (model) {
            // copy data
            $scope.createdConsolidation.Projects = model.CourseProjects;

            // default options
            $scope.createdConsolidation.Project = model.CourseProjects[0];
        })
    }

    $scope.loadGroupStudents = function () {
        $http.get('/CourseProjects/GetStudentsOfGroup?groupNum=' + $scope.createdConsolidation.Group.GroupId).success(function (students) {
            $scope.Students = students;
        })
    }

    $scope.createConsolidation = function () {

        if (typeof ($scope.createdConsolidation.Student) == 'undefined') {
            var student = $scope.Students[0];
        }
        else {
            var student = $scope.createdConsolidation.Student;
        }

        var project = selectService.getProject();
        var year = selectService.getYear();
        var group = selectService.getGroup();
        var discipline = selectService.getDiscipline();

        if (student == null) {
            return;
        }

        dataStorage.storeDisciplineConsolidations(discipline.DisciplineId, year, group, undefined);
        dataStorage.storeCourseProjectConsolidations(year, project.CourseProjectId, undefined);

        $http.post('/CourseProjects/CreateConsolidation', {
            model: {
                UserId: student.UserId, CourseProjectId: project.CourseProjectId, DisciplineId: discipline.DisciplineId
            }
        }).success(function (message) {
            $('#' + window.openModalId).modal('hide');
            communicationService.execute('open message', [message]);
            if (message.Title != "Ошибка") {
                communicationService.execute('create consolidation complete', []);
            }
        })
    }

    $scope.cancel = function () {
        $('#' + window.openModalId).modal('hide');
    }

}

courseProjectApp.controller('consolidationModal', ModalCallback);

function consolidationCallback($scope, $rootScope, $http, selectService, communicationService, storeService, dataStorage) {
    $scope.openCreateModal = function () {

        var discipline = selectService.getDiscipline();

        if (discipline == undefined)
            return;

        var projects = dataStorage.getProjects(discipline.DisciplineId);
        var project = selectService.getProject();
        var group = selectService.getGroup();

        if (projects == undefined || projects.length == 0 || project == undefined || group == undefined) {
            return;
        }

        $scope.createdConsolidation = {};
        var group = selectService.getGroup();

        $http.get('/CourseProjects/CreateConsolidation?groupId=' + group).success(function (model) {
            communicationService.execute('create consolidation', [model]);
        });
    }

    $scope.openEditModal = function () {
        var discipline = selectService.getDiscipline();

        if (discipline == undefined)
            return;

        var projects = dataStorage.getProjects(discipline.DisciplineId);

        var consolidation = selectService.getConsolidation();

        if ( projects == undefined || projects.length == 0 || consolidation == undefined) {
            return;
        }

        if (consolidation == undefined) {
            return;
        }

        var group = selectService.getGroup();

        $http.get('/CourseProjects/EditConsolidation?consolidationId=' + consolidation.ConsolidationId + "&groupId=" + group).success(function (model) {
            communicationService.execute('consolidation edit', [model]);
        })
    }

    $scope.openConfirmDialog = function () {
        var discipline = selectService.getDiscipline();

        if (discipline == undefined)
            return;

        var projects = dataStorage.getProjects(discipline.DisciplineId);
        var consolidation = selectService.getConsolidation();

        if (projects == undefined || projects.length == 0 || consolidation == undefined) {
            return;
        }

        if (typeof (consolidation) == 'undefined') {
            var Message = { Title: "Ошибка", Body: "Выберите закрепление, для того чтобы его отредактировать." };
            communicationService.execute('open message', [Message]);
        }

        communicationService.execute('confirm delete open', ["Вы уверены, что хотите удалить закрепление за курсовым проектом?",
            "Confirm consolidation delete"]);
    }
};

courseProjectApp.controller('consolidation', consolidationCallback);

courseProjectApp.factory('ConsolidationInteract', function ($rootScope) {
    var initConsolidation = {};

    initConsolidation.initCreateBroadCast = function () {
        this.CreateBroadCast();
    }

    initConsolidation.CreateBroadCast = function () {
        $rootScope.$broadcast('createConsolidationBroadcast');
    }

    initConsolidation.initEditBroadCast = function () {
        $rootScope.$broadcast('editConsolidation');
    }

    initConsolidation.DeleteBroadCast = function () {
        $rootScope.$broadcast('confirmDeleteBroadCast');
    }

    initConsolidation.CreateUpdateBroadCast = function () {
        $rootScope.$broadcast("CreateUpdateBroadCast");
    }

    return initConsolidation;
});

courseProjectApp.factory('MessageInteract', function ($rootScope) {
    var interact = {};

    interact.BroadCastMessage = function () {
        $rootScope.$broadcast('messageBroadCast');
    };

    interact.BroadcastDeleteConfirm = function () {
        $rootScope.$broadcast('confirmDeleteBroadCast');
    }
    return interact;
});

//consolidationCallback.$inject = ['$scope', '$rootScope', '$http', 'ConsolidationInteract', 'MessageInteract', 'selectService'];
//ModalCallback.$inject = ['$scope', '$rootScope', '$http', 'ConsolidationInteract', 'MessageInteract'];