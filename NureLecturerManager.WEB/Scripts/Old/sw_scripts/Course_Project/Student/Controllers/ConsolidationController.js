﻿var app = angular.module('courseProjectApp');

app.controller("Consolidation", function($scope, selectService, $http, communicationService, dataStorage) {
    $scope.ConsolidateProject = function () {
        var project = selectService.getProject();

        if (project == undefined) {
            return;
        }

        var consStatus = selectService.getConsolidationStatus();
        if (consStatus) {
            return;
        }
        // send to message controller
        communicationService.execute('open confirm consolidation create', []);
    }

    communicationService.register('create consolidation confirmed', function () {
        var project = selectService.getProject();

        $http.post('/CourseProjects/ConsolidateStudent?cpId=' + project.CourseProjectId).success(function (message) {
            communicationService.execute('open message', [message]);

            if (message.Title != "Ошибка") {
                communicationService.execute('consolidation created', [message.Consolidation]);
            }
        })
    })

    $scope.DeleteConsolidation = function () {
        var consStatus = selectService.getConsolidationStatus();

        if (!consStatus) {
            return;
        }

        var discipline = selectService.getDiscipline();

        var project = null;
        var projects = dataStorage.getProjects(discipline.DisciplineId);
        var userConsolidation = selectService.getUserConsolidation();
        for (var i = 0; i < projects.length; i++) {
            if (projects[i].CourseProjectId == userConsolidation.CourseProjectId) {
                project = projects[i];
            }
        }

        // send to message controller
        communicationService.execute('open confirm consolidation delete', [discipline, project]);
    }

    communicationService.register('delete consolidation complete', function () {
        var discipline = selectService.getDiscipline();

        $http.post('/CourseProjects/DeleteStudentConsolidation?disciplineId=' + discipline.DisciplineId).success(function (message) {
            communicationService.execute('open message', [message]);

            if (message.Title == 'Ошибка') {
                communicationService.execute('show project');
            }

            communicationService.execute('consolidation deleted', []);
        })
    })
})