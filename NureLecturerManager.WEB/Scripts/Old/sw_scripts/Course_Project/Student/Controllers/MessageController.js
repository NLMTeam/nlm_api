﻿var app = angular.module('courseProjectApp');

app.controller('Message', function ($scope, communicationService, selectService) {

    // confirmations

    communicationService.register('open confirm consolidation create', function () {
        var project = selectService.getProject();
        $scope.DeleteMessage = 'Вы хотите закрепится за курсовым проектом "' + project.Theme + '"';
        $scope.isCreateOpen = true;
        $("#ConfirmDeleteDialog").modal('show');
    });

    $scope.hideConfirmDialog = function () {
        $("#ConfirmDeleteDialog").modal('hide');
    };

    $scope.confirm = function () {
        $("#ConfirmDeleteDialog").modal('hide');
        if ($scope.isCreateOpen) {
            communicationService.execute('create consolidation confirmed', []);
        }
        else {
            communicationService.execute('delete consolidation complete', []);
        }
    }

    communicationService.register('open confirm consolidation delete', function (discipline, project) {
        $scope.DeleteMessage = 'Вы уверены, что хотите удалить Ваше закрепление за курсовым проектом по дисциплине "' +
            discipline.Name + '"' + " за проектом " + project.Theme;

        $scope.isCreateOpen = false;
        $("#ConfirmDeleteDialog").modal('show');
    })

    // message

    communicationService.register('open message', function (message) {
        $scope.Message = message;
        $("#MessageDialog").modal('show');
    })

    $scope.hide = function () {
        $("#MessageDialog").modal('hide');
    }

})