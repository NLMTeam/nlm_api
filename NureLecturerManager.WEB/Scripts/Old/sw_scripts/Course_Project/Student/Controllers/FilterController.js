﻿var app = angular.module('courseProjectApp');

app.controller('filter', function callBack($scope, $rootScope, $http, selectService, storeService, dataStorage, communicationService) {
    $scope.CourseProjectMode = 'ThemeList';
    var cpLoad, consLoad;
    cpLoad = consLoad = false;

    $scope.loaded = false;

    $scope.allowProjectEdit = false;
    $scope.allowConsolidationEdit = false;
    $scope.preventDeselect = false;

    $scope.displayYear = "Год";
    $scope.displayGroup = "Группа";

    $scope.currentProject = null;
    $scope.IsArchieve = false;
    $scope.UserConsolidation = null;
    $('#themeInfo').slideUp(1, function () { });

    function transformConsolidations (consolidations) {
        if (consolidations != null) {
            for (var i = 0; i < consolidations.length; i++) {
                if (consolidations[i].PassDate != null && consolidations[i].PassDate != "" && typeof(consolidations[i].PassDate) != typeof(new Date())) {
                    var dateArr = consolidations[i].PassDate.split(".");
                    if (dateArr.length == 3) {
                        var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                        consolidations[i].PassDate = date;
                    }
                    else {
                        var dateArr = consolidations[i].PassDate.split("/");
                        var date = new Date(+dateArr[2], +dateArr[0] != 0 ? +dateArr[0] - 1 : 12, +dateArr[1]);
                        consolidations[i].PassDate = date;
                    }
                }

                if (consolidations[i].ConsolidationDate != null && consolidations[i].ConsolidationDate != "" && typeof (consolidations[i].ConsolidationDate) != typeof (new Date())) {
                    var dateArr = consolidations[i].ConsolidationDate.split(".");
                    if (dateArr.length == 3) {
                        var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                        consolidations[i].ConsolidationDate = date;
                    }
                    else {
                        var dateArr = consolidations[i].ConsolidationDate.split("/");
                        var date = new Date(+dateArr[2], + dateArr[0] != 0 ? +dateArr[0] - 1 : 12, +dateArr[1]);
                        consolidations[i].ConsolidationDate = date;
                    }
                }
            }

            return consolidations;
        }
    }
    function modalCallback(model) {

        // return server CourseProjectMainViewModel

        $scope.Disciplines = model.Disciplines != null ? model.Disciplines : [];
        $scope.CourseProjects = model.CourseProjects != null ? model.CourseProjects : [];
        $scope.Years = model.Years != null ? model.Years : [];
        $scope.Groups = model.Groups != null ? model.Groups : [];
        $scope.Consolidations = model.Consolidations != null ? model.Consolidations : [];

        var consolidations = $scope.Consolidations;

        $scope.Consolidations = transformConsolidations(consolidations);
        $scope.IsConsolidated = model.IsConsolidated;
        selectService.setConsolidationStatus(model.IsConsolidated);
        selectService.selectUserConsolidation(model.Consolidation);

        // we loaded data
        $scope.loaded = true;

        // mark default values 
        if ($scope.Disciplines.length > 0) {
            $scope.selectedDiscipline = $scope.Disciplines[0];
            selectService.selectDiscipline($scope.Disciplines[0]);
        }

        if ($scope.Years.length > 0) {
            $scope.displayYear = $scope.Years[0];
            selectService.selectYear($scope.Years[0]);
            $scope.selectedYear = $scope.Years[0];
        }
        else {
            $scope.displayYear = "Год";
            delete $scope.selectedYear;
        }

        if ($scope.Groups.length > 0) {
            $scope.displayGroup = $scope.Groups[0];
            selectService.selectGroup($scope.Groups[0]);
            $scope.selectedGroup = $scope.Groups[0];
        }
        else {
            $scope.displayGroup = "Группа";
            delete $scope.selectedGroup;
        }

        $scope.UserConsolidation = model.Consolidation;

        //if ($scope.Consolidations != null && $scope.Consolidations.length > 0) {
        //    for (var i = 0; i < $scope.Consolidations.length; i++) {
        //        if ($scope.Consolidations.len)
        //    }
        //}
        $scope.$applyAsync(function () {
            $scope.selectConsolidation($scope.UserConsolidation);
        });
    }

    $scope.getCurrentModel = function () {

        var stObj = storeService.loadModel();

        if (stObj.success != undefined) {
            stObj.success(modalCallback);
        }
        else {
            modalCallback(stObj);
        }

        $scope.currentProject = null;
    }

    $scope.getCurrentModel();

    $scope.loadDisciplineInfo = function () {

        if ($scope.selectedDiscipline == null) {
            return;
        }

        // select discipline
        selectService.selectDiscipline($scope.selectedDiscipline);

        // must return angular ajax object or data from storage.
        var storeResult = storeService.loadDisciplineInfo($scope.selectedDiscipline.DisciplineId);

        selectService.selectConsolidation(undefined);
        selectService.selectYear(undefined);
        selectService.selectGroup(undefined);
        selectService.selectProject(undefined);

        if (typeof (storeResult) == 'undefined') {
            throw new Error('Store service return undefined');
        }

        // use duck typing to detect what we have got
        if (typeof (storeResult.success) != "undefined") {
            storeResult.success(function (model) {
                $scope.CourseProjects = model.CourseProjects;
                $scope.displayGroup = "Группа";

                $scope.currentProject = null;
                delete $scope.selectedConsolidation;
                delete $scope.selectedGroup;

                $scope.Consolidations = [];
                $scope.IsConsolidated = model.IsConsolidated;
                $scope.UserConsolidation = model.Consolidation;
                selectService.setConsolidationStatus(model.IsConsolidated);
                selectService.selectUserConsolidation(model.Consolidation);
                dataStorage.storeUserConsolidation($scope.selectedDiscipline.DisciplineId, model.Consolidation);

                if ($scope.CourseProjectMode == "Info") {
                    $("#themeInfo").slideUp('fast', function () { });
                }

                if ($scope.UserConsolidation != null) {
                    $scope.$applyAsync(function () {
                        $scope.selectConsolidation($scope.UserConsolidation);
                    });
                }
            });
        }
        else {
            $scope.CourseProjects = storeResult.Projects;
            $scope.displayGroup = "Группа";

            $scope.currentProject = null;
            delete $scope.selectedConsolidation;
            delete $scope.selectedGroup;

            $scope.Consolidations = [];
            $scope.IsConsolidated = storeResult.IsConsolidated;
            $scope.UserConsolidation = storeResult.Consolidation;
            selectService.setConsolidationStatus(storeResult.IsConsolidated);
            selectService.selectUserConsolidation($scope.UserConsolidation);


            if ($scope.CourseProjectMode == "Info") {
                $("#themeInfo").slideUp('fast', function () { });
            }

            if ($scope.UserConsolidation != null) {
                $scope.$applyAsync(function () {
                    $scope.selectConsolidation($scope.UserConsolidation);
                });
            }
        }

        var groupStore = storeService.loadGroups("", $scope.selectedDiscipline);

        if (groupStore.success == undefined) {
            $scope.Groups = groupStore;

            if (groupStore.length > 0) {
                $scope.selectGroup(groupStore[0]);
            }
        }
        else {
            groupStore.success(function (groups) {
                $scope.Groups = groups;
                
                if (groups.length > 0) {
                    $scope.selectGroup(groups[0]);
                }

            });
        }
    }

    $scope.selectProject = function (project) {
        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');

            if ($scope.currentProject.CourseProjectId == project.CourseProjectId && !$scope.preventDeselect) {
                var group = selectService.getGroup();

                if (group != undefined) {
                    var discipline = selectService.getDiscipline();

                    var stObj = storeService.ConsolidationsForDisciplineAndGroup("", group, discipline);

                    if (stObj.success == undefined) {
                        $scope.Consolidations = transformConsolidations(stObj);
                    }
                    else {
                        stObj.success(function (consolidations) {
                            $scope.Consolidations = transformConsolidations(consolidations);
                        })
                    }
                }
                selectService.selectProject(undefined);
                $scope.currentProject = null;
                $("#themeInfo").slideUp();
                return;
            }
        }

        $scope.preventDeselect = false;
        // select
        selectService.selectProject(project);
        $scope.currentProject = project;

        // display selction
        $("#project" + project.CourseProjectId).addClass('active');

        // change the description of project if it's viewed
        if ($scope.CourseProjectMode == "Info") {
            $scope.Project = project;
        }

        // load data if it's possible
        $scope.loadProjectInfo(project);
    }

    $scope.loadProjectInfo = function (project) {

        var storeObj = storeService.ConsolidationsForProject("", project.CourseProjectId);

        if (typeof (storeObj.success) != "undefined") {
            storeObj.success(function (data) {
                var consolidations = transformConsolidations(data);
                $scope.Consolidations = consolidations;
                $scope.selectedConsolidation = undefined;
                selectService.selectConsolidation(undefined);
                if ($scope.UserConsolidation != null) {
                    $scope.$applyAsync(function () {
                        $scope.selectConsolidation($scope.UserConsolidation);
                    });
                }

            });
        }
        else {
            $scope.Consolidations = transformConsolidations(storeObj);
            $scope.selectedConsolidation = undefined;
            selectService.selectConsolidation(undefined);
            if ($scope.UserConsolidation != null) {
                $scope.$applyAsync(function () {
                    $scope.selectConsolidation($scope.UserConsolidation);
                });
            }
        }
    }

    $scope.selectGroup = function (group) {
        $scope.displayGroup = group;
        selectService.selectGroup(group);
        $scope.selectedGroup = group;
        // we load consolidations for group and noe deselect project

        if ($scope.currentProject != null) {
            selectService.selectProject(undefined);
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
            $scope.currentProject = null;
        }

        $scope.loadGroupInfo(group);
    }

    $scope.loadGroupInfo = function (group) {
        // load consolidations for discipline + year + group
        var discipline = selectService.getDiscipline();

        var storeObj = storeService.ConsolidationsForDisciplineAndGroup("", group, discipline);

        if (storeObj == undefined) {
            throw new Error("Store service returned undefined for projects");
        }

        if (storeObj.success != undefined) {
            storeObj.success(function (consolidations) {
                $scope.Consolidations = transformConsolidations(consolidations);
                $scope.selectedConsolidation = undefined;
                selectService.selectConsolidation(undefined);
                if ($scope.UserConsolidation != undefined && $scope.UserConsolidation != null) {
                    $scope.currentProject = { CourseProjectId: $scope.UserConsolidation.CourseProjectId };
                }
            })
        }
        else {
            $scope.Consolidations = transformConsolidations(storeObj);
            $scope.selectedConsolidation = undefined;
            selectService.selectConsolidation(undefined);
            if ($scope.UserConsolidation != null) {
                $scope.$applyAsync(function () {
                    $scope.selectConsolidation($scope.UserConsolidation);
                });
            }
            if ($scope.UserConsolidation != undefined && $scope.UserConsolidation != null) {
                $scope.currentProject = { CourseProjectId: $scope.UserConsolidation.CourseProjectId };
            }
        }
    }

    $scope.selectConsolidation = function (consolidation) {

        if (consolidation == null) {
            return;
        }

        var cons = selectService.getConsolidation();
        if (typeof (cons) != 'undefined') {

            var prevElement = document.getElementById(cons.ConsolidationId);

            if (prevElement != null) {
                prevElement.classList.remove('warning');
            }

            if (cons.ConsolidationId == consolidation.ConsolidationId) {
                selectService.selectConsolidation(undefined);
                return;
            }
        }

        selectService.selectConsolidation(consolidation);
        $scope.selectedConsolidation = consolidation;

        var consElement = document.getElementById(consolidation.ConsolidationId);

        if (consElement != null) {
            consElement.classList.add('warning');
        }
        else {
            if (document.getElementById(consolidation.CourseProjectId) == null) {
                return;
            }
        }

        if (document.getElementById(consolidation.CourseProjectId) == null) {
            return;
        }

        var container = $('#projectContainer'),
            scrollTo = $('#project' + consolidation.CourseProjectId);

        container.scrollTop(
            scrollTo.offset().top - container.offset().top + container.scrollTop()
        );

        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
        }
        $("#project" + consolidation.CourseProjectId).addClass('active');

        for (var i = 0; i < $scope.CourseProjects.length; i++) {
            var project = $scope.CourseProjects[i];

            if (project.CourseProjectId == consolidation.CourseProjectId) {
                selectService.selectProject(project);
                $scope.currentProject = project;
                break;
            }
        }

    }

    $scope.showProject = function (consolidation) {

        var container = $('#projectContainer'),
            scrollTo = $('#project' + consolidation.CourseProjectId);

        container.scrollTop(
            scrollTo.offset().top - container.offset().top + container.scrollTop()
        );

        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
        }
        $("#project" + consolidation.CourseProjectId).addClass('active');

        for (var i = 0; i < $scope.CourseProjects.length; i++) {
            var project = $scope.CourseProjects[i];

            if (project.CourseProjectId == consolidation.CourseProjectId) {
                selectService.selectProject(project);
                $scope.currentProject = project;
                break;
            }
        }
    }

    $scope.displayInfo = function (project) {
        $scope.CourseProjectMode = "Info";
        $scope.preventDeselect = true;
        $scope.Project = {};

        $scope.Project.Theme = project.Theme;
        $scope.Project.Description = project.Description;
        $scope.Project.Type = project.Type;
        $("#themeInfo").slideDown();
    }

    $scope.displayList = function () {
        $scope.CourseProjectMode = 'ThemeList';
        $('#themeInfo').slideUp('slow', function () { });
    }

    communicationService.register('consolidation created', function (consolidation) {
        var project = selectService.getProject();

        if (project != undefined)
        {
            dataStorage.storeCourseProjectConsolidations("", project.CourseProjectId, undefined);
        }

        var discipline = selectService.getDiscipline();
        var group = selectService.getGroup();

        if (group == undefined) {
            group = $scope.Groups[0];
        }

        dataStorage.storeDisciplineConsolidations(discipline.DisciplineId, "", group, undefined);

        // reload for discipline and group
        storeService.ConsolidationsForDisciplineAndGroup("", group, discipline).success(function (consolidations) {
            $scope.Consolidations = transformConsolidations(consolidations);
            $scope.selectedConsolidation = undefined;
            selectService.selectConsolidation(undefined);
        });

        // reload for course project
        storeService.ConsolidationsForProject("", project.CourseProjectId).success(function (consolidations) {

        })

        $scope.IsConsolidated = true;
        dataStorage.storeConsoldationStatus(discipline.DisciplineId, true);

        selectService.setConsolidationStatus(true);

        $scope.UserConsolidation = consolidation;
        dataStorage.storeUserConsolidation(discipline.DisciplineId, consolidation);
        selectService.selectUserConsolidation(consolidation);
    })

    communicationService.register('consolidation deleted', function () {
        var project = selectService.getProject();

        dataStorage.storeCourseProjectConsolidations("", $scope.UserConsolidation.CourseProjectId, undefined);

        var discipline = selectService.getDiscipline();
        var group = selectService.getGroup();

        if (group == undefined) {
            group = $scope.Groups[0];
        }

        dataStorage.storeDisciplineConsolidations(discipline.DisciplineId, "", group, undefined);

        storeService.ConsolidationsForDisciplineAndGroup("", group, discipline).success(function (consolidations) {
            $scope.Consolidations = transformConsolidations(consolidations);
            $scope.selectedConsolidation = undefined;
            selectService.selectConsolidation(undefined);
        });

        // deselect theme
        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
            $scope.currentProject = null;
            selectService.selectProject(undefined);
        }

        $scope.IsConsolidated = false;
        selectService.setConsolidationStatus(false);
        selectService.selectUserConsolidation(undefined);
        dataStorage.storeConsoldationStatus(discipline.DisciplineId, false);
        dataStorage.storeUserConsolidation(discipline.DisciplineId, undefined);
    });

    communicationService.register('show project', function () {
        $('#project' + $scope.UserConsolidation.CourseProjectId).addClass('active');

        var container = $('#projectContainer'),
            scrollTo = $('#project' + $scope.UserConsolidation.CourseProjectId);

        container.scrollTop(
            scrollTo.offset().top - container.offset().top + container.scrollTop()
        );

        setTimeout(function () {
            $scope.IsConsolidated = true;
            selectService.setConsolidationStatus(true);
            dataStorage.storeConsoldationStatus(discipline.DisciplineId, true);
        }, 10);
    })

});