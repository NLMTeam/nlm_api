﻿var app = angular.module('courseProjectApp');

app.constant('loadConfig', {
    yearUrl: function () {
        return '/CourseProjects/GetYears';
    },
    disciplineInfoUrl : function (disciplineId)  {
        return "/CourseProjects/GetDisciplineInfo?disciplineId=" + disciplineId;
    },
    disciplineUrl: function (year, groupNum) {
        return '/CourseProjects/GetGroupDisciplines?year=' + year + '&groupNum=' + groupNum;
    },
    groupUrl: function (year, discipline) {
        return '/CourseProjects/GetYearGroups?year=' + year + '&disciplineId=' + discipline.DisciplineId;
    },
    courseProjectUrl: function (discipline) {
        return '/CourseProjects/CourseProjectsForDiscipline?disciplineId=' + discipline.DisciplineId;
    },
    consolidationsForProjectUrl: function (year, courseProjectId) {
        return '/CourseProjects/GetMarksOfCourseProject?year=' + year + '&cpId=' + courseProjectId;
    },
    consolidationsForDisciplineAndGroup: function (year, disc, groupNum) {
        return '/CourseProjects/GetMarksForDisciplineAndGroup?year=' + year + '&disciplineName=' + disc.Name + "&groupNum=" + groupNum;
    },
    modelUrl: function () {
        return "/CourseProjects/GetStudentModel";
    }

});

app.service('storeService', function ($http, loadConfig, dataStorage) {
    var obj = {};

    var years, groups, disciplines, projects, marks;

    obj.loadGroups = function (year, discipline) {
        var groups = dataStorage.getGroups(year, discipline.DisciplineId);

        if (groups == undefined) {
            return $http.get(loadConfig.groupUrl(year, discipline)).success(function (data) {
                dataStorage.storeGroups(year, discipline.DisciplineId, data);
            });
        }
        else {
            return groups;
        }
    }
    obj.getGroups = function () {
        return groups;
    }

    obj.loadProjects = function (discipline) {
        // it's used after creating course project, so it must load data always
        return $http.get(loadConfig.courseProjectUrl(discipline)).success(function (data) {
            projects = data.CourseProjects;
            dataStorage.storeProjects(discipline.DisciplineId, data);
        });
    }
    obj.getProjects = function () {
        return projects;
    }

    obj.ConsolidationsForProject = function (year, cpId) {
        var consolidations = dataStorage.getCourseProjectConsolidations(year, cpId);

        if (consolidations == undefined) {
            return $http.get(loadConfig.consolidationsForProjectUrl(year, cpId)).success(function (data) {
                dataStorage.storeCourseProjectConsolidations(year, cpId, data);
            });
        }
        else {
            return consolidations;
        }
    }
    obj.ConsolidationsForDisciplineAndGroup = function (year, group, discipline) {

        var consolidations = dataStorage.getDisciplineConsolidations(discipline.DisciplineId, year, group);

        if (consolidations == undefined) {
            return $http.get(loadConfig.consolidationsForDisciplineAndGroup(year, discipline, group)).success(function (data) {
                dataStorage.storeDisciplineConsolidations(discipline.DisciplineId, year, group, data);
                marks = data.Consolidations;
            });
        }
        else {
            return consolidations;
        }
    }
    obj.getConsolidations = function () {
        return marks;
    }

    obj.loadDisciplineInfo = function (disciplineId) {

        if (typeof (disciplineId) != typeof (1)) {
            throw new Error("Discipline Id must be a digit, but get -" + disciplineId);
        }

        var projects = dataStorage.getProjects(disciplineId);

        if (projects == undefined) {
            return $http.get(loadConfig.disciplineInfoUrl(disciplineId)).success(function (model) {
                dataStorage.storeProjects(disciplineId, model.CourseProjects);
                dataStorage.storeYears(disciplineId, model.Years);
                dataStorage.storeConsoldationStatus(disciplineId, model.IsConsolidated);
                dataStorage.storeUserConsolidation(disciplineId, model.Consolidation);
            });
        }
        else {
            return {
                Years: dataStorage.getYears(disciplineId),
                Projects: projects, 
                IsConsolidated: dataStorage.getConsolidationStatus(disciplineId),
                Consolidation: dataStorage.getUserConsolidation(disciplineId)
            };
        }
    }

    obj.loadModel = function () {
        var model = dataStorage.getModel();

        if (model == undefined) {
            return $http.get(loadConfig.modelUrl()).success(function (model) {
                dataStorage.storeModel(model);

                dataStorage.storeDisciplines(model.Disciplines);


                // save locally course projects and years if there is discipline which can be connected to them
                if (model.Disciplines != null && model.Disciplines.length > 0) {
                    var discipline = model.Disciplines[0];
                    dataStorage.storeProjects(discipline.DisciplineId, model.CourseProjects);
                    dataStorage.storeConsoldationStatus(discipline.DisciplineId, model.IsConsolidated);
                    dataStorage.storeUserConsolidation(discipline.DisciplineId, model.Consolidation);
                }

                // save groups if there is year for groups
                if (model.Disciplines != null && model.Disciplines.length > 0) {
                    dataStorage.storeGroups("", model.Disciplines[0].DisciplineId, model.Groups);
                }

                // save locally consolidations if there is discipline year group
                if (model.Disciplines != null && model.Disciplines.length > 0 && model.Groups != null && model.Groups.length > 0) {

                    dataStorage.storeDisciplineConsolidations(model.Disciplines[0].DisciplineId, "", model.Groups[0],
                    model.Consolidations);
                }

            });
        }
        else {
                dataStorage.storeDisciplines(model.Disciplines);

                // save locally course projects and years if there is discipline which can be connected to them
                if (model.Disciplines != null && model.Disciplines.length > 0) {
                    var discipline = model.Disciplines[0];
                    dataStorage.storeProjects(discipline.DisciplineId, model.CourseProjects);
                    dataStorage.storeConsoldationStatus(discipline.DisciplineId, model.IsConsolidated);
                    dataStorage.storeUserConsolidation(discipline.DisciplineId, model.Consolidation);
                }

                // save groups if there is year for groups
                if (model.Disciplines != null && model.Disciplines.length > 0) {
                    dataStorage.storeGroups(model.Years[0], model.Disciplines[0].DisciplineId, model.Groups);
                }

                // save locally consolidations if there is discipline year group
                if (model.Disciplines != null && model.Disciplines.length > 0 && model.Groups != null && model.Groups.length > 0) {

                    dataStorage.storeDisciplineConsolidations(model.Disciplines[0].DisciplineId, model.Years[0], model.Groups[0],
                    model.Consolidations);
                }

            return model;
        }

    }

    obj.loadArchievedModel = function () {
        var model = dataStorage.getArchievedModel();

        if (model == undefined) {
            return $http.get(loadConfig.archivedModelUrl()).success(function (model) {
                dataStorage.storeArchivedModel(model);

                dataStorage.storeDisciplines(model.Disciplines);

                // save locally course projects and years if there is discipline which can be connected to them
                if (model.Disciplines != null && model.Disciplines.length > 0) {
                    var discipline = model.Disciplines[0];
                    dataStorage.storeProjects(discipline.DisciplineId, model.CourseProjects);
                    dataStorage.storeYears(discipline.DisciplineId, model.Years);
                }

                // save groups if there is year for groups
                if (model.Years != null && model.Years.length > 0 && model.Disciplines.length > 0) {
                    dataStorage.storeGroups(model.Years[0], model.Disciplines[0].DisciplineId, model.Groups);
                }

                // save locally consolidations if there is discipline year group
                if (model.Disciplines != null && model.Disciplines.length > 0 && model.Years != null && model.Years.length > 0
                    && model.Groups != null && model.Groups.length > 0) {
                    dataStorage.storeDisciplineConsolidations(model.Disciplines[0].DisciplineId, model.Years[0], model.Groups[0],
                    model.Consolidations);
                }

            });
        }
        else {
            dataStorage.storeDisciplines(model.Disciplines);

            // save locally course projects and years if there is discipline which can be connected to them
            if (model.Disciplines != null && model.Disciplines.length > 0) {
                var discipline = model.Disciplines[0];
                dataStorage.storeProjects(discipline.DisciplineId, model.CourseProjects);
                dataStorage.storeYears(discipline.DisciplineId, model.Years);
            }

            // save groups if there is year for groups
            if (model.Years != null && model.Years.length > 0 && model.Disciplines != null && model.Disciplines.length > 0) {
                dataStorage.storeGroups(model.Years[0], model.Disciplines[0].DisciplineId, model.Groups);
            }

            // save locally consolidations if there is discipline year group
            if (model.Disciplines != null && model.Disciplines.length > 0 && model.Years != null && model.Years.length > 0
                && model.Groups != null && model.Groups.length > 0) {

                dataStorage.storeDisciplineConsolidations(model.Disciplines[0].DisciplineId, model.Years[0], model.Groups[0],
                model.Consolidations);
            }

            return model;
        }
    }

    return obj;
});