﻿var app = angular.module('courseProjectApp');

app.service('selectService', function () {
    var obj = {};

    var year, group, discipline, courseProject, consolidation, consStatus, userConsolidation;

    obj.selectYear = function (value) {
        year = value;
    }
    obj.getYear = function () {
        return year;
    }

    obj.selectGroup = function (value) {
        group = value;
    }
    obj.getGroup = function () {
        return group;
    }

    obj.selectDiscipline = function (value) {
        discipline = value;
    }
    obj.getDiscipline = function () {
        return discipline;
    }

    obj.selectProject = function (value) {
        courseProject = value;
    }
    obj.getProject = function () {
        return courseProject;
    }

    obj.selectConsolidation = function (value) {
        consolidation = value;
    }
    obj.getConsolidation = function () {
        return consolidation;
    }

    obj.setConsolidationStatus = function (value) {
        consStatus = value;
    }
    obj.getConsolidationStatus = function () {
        return consStatus;
    }

    obj.selectUserConsolidation = function (consolidation) {
        userConsolidation = consolidation;
    }
    obj.getUserConsolidation = function () {
        return userConsolidation;
    }

    return obj;
});