﻿var app = angular.module('courseProjectApp');

app.service('dataStorage', function () {
    var storeObj = {};

    storeObj.Projects = {};
    storeObj.Years = {};
    storeObj.Groups = {};
    storeObj.ProjectConsolidations = {};
    storeObj.DisciplineConsolidations = {};

    var serviceObj = {};

    serviceObj.storeProjects = function (disciplineId, data) {
        if (typeof (disciplineId) != typeof (2)) {
            throw new Error('Discipline id must be a digit, but get - ' + disciplineId);
        }

        storeObj.Projects[disciplineId] = data;
    };

    serviceObj.getProjects = function (disciplineId) {
        return storeObj.Projects[disciplineId];
    }

    serviceObj.storeYears = function (disciplineId, data) {
        if (typeof (disciplineId) != typeof (1)) {
            throw new Error("Discipline Id must be a digit, but -" + disciplineId);
        }

        storeObj.Years[disciplineId] = data;
    }

    serviceObj.getYears = function (disciplineId) {
        return storeObj.Years[disciplineId];
    }

    serviceObj.storeGroups = function (year, disciplineId, data) {
        if (typeof (disciplineId) != typeof (1)) {
            throw new Error("Discipline Id must be a digit, but -" + year);
        }

        var idObj = {
            Year: year,
            DisciplineId: disciplineId
        };

        storeObj.Groups[JSON.stringify(idObj)] = data;
    }

    serviceObj.getGroups = function (year, disciplineId) {
        var idObj = {
            Year: year,
            DisciplineId: disciplineId
        }
        return storeObj.Groups[JSON.stringify(idObj)];
    }

    serviceObj.storeCourseProjectConsolidations = function (year, courseProjectId, data) {

        if (typeof (year) != typeof ("") || typeof (courseProjectId) != typeof (1)) {
            throw new Error("Invalid identifier(s) year - " + year + ", courseProjectId -"
                + courseProjectId);
        }

        var idObj = {
            Year: year,
            CourseProjectId: courseProjectId
        };

        storeObj.ProjectConsolidations[JSON.stringify(idObj)] = data;

    }

    serviceObj.getCourseProjectConsolidations = function (year, courseProjectId) {
        var idObj = {
            Year: year,
            CourseProjectId: courseProjectId
        }

        return storeObj.ProjectConsolidations[JSON.stringify(idObj)];
    }

    serviceObj.storeDisciplineConsolidations = function (disciplineId, year, groupId, data) {

        if (typeof (disciplineId) != typeof (21)) {
            throw new Error("DisciplineId must be a digit, but get - " + disciplineId);
        }

        if (typeof (year) != typeof ("")) {
            throw new Error("Year must be a string, but get - " + year);
        }

        if (typeof (groupId) != typeof ("")) {
            throw new Error("GroupId must be a string, but get - " + groupId);
        }

        var idObj = {
            DisciplineId: disciplineId,
            Year: year,
            GroupId: groupId
        };

        storeObj.DisciplineConsolidations[JSON.stringify(idObj)] = data;
    }

    serviceObj.getDisciplineConsolidations = function (disciplineId, year, groupId) {
        var idObj = {
            DisciplineId: disciplineId,
            Year: year,
            GroupId: groupId
        };

        return storeObj.DisciplineConsolidations[JSON.stringify(idObj)];
   }

    serviceObj.storeDisciplines = function (data) {
        storeObj.Disciplines = data;
    }

    serviceObj.getDisciplines = function () {
        return storeObj.Disciplines;
    }

    serviceObj.deleteProjectConsolidations = function (courseProjectId) {
        for (var discConsolidationNum in storeObj.DisciplineConsolidations)
        {
            discConsolidations = storeObj.DisciplineConsolidations[discConsolidationNum];
            for (var consNum in discConsolidations) {
                var cons = discConsolidations[consNum];

                if (cons.CourseProjectId === courseProjectId) {
                    var index = discConsolidations.indexOf(cons);
                    discConsolidations.splice(index, 1);
                }
            }
        }
    }

    serviceObj.printStoreData = function () {
        //console.log(storeObj);
    }

    return serviceObj;
});