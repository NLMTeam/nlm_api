﻿var app = angular.module('courseProjectApp');

app.controller('message', function ($scope, $rootScope, communicationService) {
    communicationService.register('open message', function (message) {
        $scope.Message = message;
        $('#MessageDialog').modal('show');
    })

    $scope.$on('messageBroadCast', function () {
        $scope.Message = $rootScope.Message;
        $('#MessageDialog').modal('show');

        document.getElementById('messageOkBtn').focus();
    });

    $scope.hide = function () {
        $('#MessageDialog').modal('hide');
    }

    communicationService.register('confirm delete open', function (message, callBackName) {
        $scope.callBackName = callBackName;
        $scope.DeleteMessage = message;
        $("#ConfirmDeleteDialog").modal('show');
    });

    $scope.$on('confirmDeleteBroadCast', function () {
        $("#ConfirmDeleteDialog").modal('show');
    });

    $scope.confirm = function () {
        $("#ConfirmDeleteDialog").modal('hide');

        communicationService.execute($scope.callBackName, []);
    }

    $scope.hideConfirmDialog = function () {
        $("#ConfirmDeleteDialog").modal('hide');
    }
})