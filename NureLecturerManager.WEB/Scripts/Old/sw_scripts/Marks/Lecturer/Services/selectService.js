﻿/*
Service that must be used for selcting discipline, group, year.
It has to store information about user selects.
*/

var app = angular.module('marksApp');


app.service('selectService', function callBack() {
    var obj = {};

    var discipline, group, year, mark;

    obj.selectDiscipline = function (disc, callback) {
        discipline = disc;

        callback.apply(obj, [disc]);
    }
    obj.getDiscipline = function () {
        return discipline;
    }

    obj.selectGroup = function (Group, callback) {
        group = Group;

        callback.apply(obj, [Group]);
    }
    obj.getGroup = function () {
        return group;
    }

    obj.selectYear = function (Year, callback) {
        year = Year;
        callback.apply(obj, [Year]);
    }
    obj.getYear = function () {
        return year;
    }

    obj.selectMark = function (Mark) {
        mark = Mark;
    }
    obj.getMark = function () {
        return mark;
    }

    return obj;
})