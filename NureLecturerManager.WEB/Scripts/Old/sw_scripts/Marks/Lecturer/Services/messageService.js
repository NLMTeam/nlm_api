﻿marksApp = angular.module('marksApp');

marksApp.service('messageService', function ($rootScope) {
    var serviceObj = {};
    var message = "";

    serviceObj.getMessage = function () {
        return message;
    };

    serviceObj.setMessage = function (value) {
        message = value;
    }

    return serviceObj;
})