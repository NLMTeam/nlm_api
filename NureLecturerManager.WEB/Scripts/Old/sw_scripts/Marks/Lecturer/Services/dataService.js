﻿var marksApp = angular.module('marksApp');

marksApp.service("dataService", function () {
    var serviceObject = {};

    serviceObject.selectDiscipline = function (discipline) {
        if (typeof (discipline) == 'undefined') {
            return serviceObject.discipline;
        }

        serviceObject.discipline = discipline;
    }

    serviceObject.selectGroup = function (group) {
        if (typeof (group) == 'undefined') {
            return serviceObject.group;
        }

        serviceObject.group = group;
    }



    return serviceObject;
});

