﻿marksApp = angular.module('marksApp');

marksApp.directive('message', function (messageService, communicationService) {
    return {
        restrict: 'E',
        scope: { },
        link: function (scope, element, attrs) {
            communicationService.register('message Broadcast', function (message) {
                scope.message = messageService.getMessage();
                try {
                    scope.$apply();
                }
                catch (e) {
                    console.log('scope in progress');
                }
                finally {
                    $('#messageDialog').modal('show');
                }
            });

            scope.hide = function () {
                $("#messageDialog").modal('hide');
            };
        },
        templateUrl: "/Scripts/sw_scripts/Marks/Lecturer/Directives/messageTemplate.html"
    };
});