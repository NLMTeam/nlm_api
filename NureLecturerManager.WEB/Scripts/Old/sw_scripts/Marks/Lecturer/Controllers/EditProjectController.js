﻿var app = angular.module('marksApp');

app.controller('editProjectInit', function ($scope, $rootScope, $http, dataService, messageService, selectService, communicationService, 
    storeService, FileUploader) {

    $scope.openEditModal = function () {
        window.event.preventDefault();
        if (typeof (selectService.getMark()) == 'undefined') {
            return;
        }

        communicationService.execute('open edit Modal', []);
    }

    $scope.generateMarkFile = function () {
        window.event.preventDefault();
        $http.get("/Marks/GenerateMarkFile?discipline=" + selectService.getDiscipline().Name + "&group=" + selectService.getGroup())
            .success(function (url) {
                window.open(url);
            })
    };

    $scope.uploader = new FileUploader({
        url:"/Marks/UploadExcelFile"
    });

    $scope.fileLoading = "no-load";

    $scope.uploadFile = function () {
        console.log("Try to open file chose dialog");
        $("#uploadFile").click();

        $("#uploadFile").on('change', function (e) {
            $scope.fileLoading = "loading";
            console.log($scope.uploader);

            $scope.item = $scope.uploader.queue[0];

            $scope.item.remove = function () {
                $scope.uploader.queue[0].remove();
                $scope.fileLoading = "no-load";
            }

            $scope.item.cancel = function () {
                $scope.uploader.queue[0].cancel();
                $scope.fileLoading = "no-load";
            }

            $scope.uploader.onProgressItem = function (fileName, progress) {
                if (progress == 100) {
                    $scope.fileLoading = "server-processing";
                }
            }

            $scope.uploader.onSuccessItem = function (file, response, status, headers) {
                $scope.fileLoading = "no-load";

                messageService.setMessage(response);
                communicationService.execute('message Broadcast', []);

                communicationService.execute('reload marks', []);
            }

            $scope.uploader.onErrorItem = function (file, response, status, headers) {
                communicationService.execute('open message', [{ Title: "Ошибка", Text: e }]);
            }

            try {
                $scope.$apply();
            }
            catch (e) {
                console.warn(e.message);
            }

            //var files = e.target.files;
            //console.log("file choosed");
            //console.log($('#uploadFile')[0].files);
            //if ( files.length > 0) {
            //    var data = new FormData();
            //    for (var x = 0; x < files.length; x++) {
            //        data.append("file" + x, files[x]);
            //    }
            //    console.log(data);
            //    $.ajax({
            //        url: "/Marks/UploadExcelFile",
            //        data: data,
            //        cache: false,
            //        contentType: false,
            //        processData: false,
            //        type: "POST",
            //        success: function (data) {
            //            messageService.setMessage(data);
            //            communicationService.execute('message Broadcast');
            //            communicationService.execute('reload marks', []);
            //        }
            //    })
            //}
        })
    }

    communicationService.register("edit marks close", function () {
        var marks = [];
        console.log(selectService.getMark());
        for (var i = 0; i < selectService.getMark().StudentMarks.length; i++)
        {
            var studentMark = selectService.getMark().StudentMarks[i];
            console.log(studentMark);
            marks.push(studentMark.Value);
        }
        console.log(marks);
        $http.post('/Marks/EditMark', { id: selectService.getMark().UserId, values: marks }).success(function (message) {
            messageService.setMessage(message);
            $("#editMarkDialog").modal('hide');
            communicationService.execute('message Broadcast', []);
            communicationService.execute('reload marks', []);
        });
    })

    $scope.printTable = function () {

        var html = "<table border=\"1\" cellpadding=\"3\" id=\"printTable\">" +
            "<tbody>" +
            "<tr>" +
            "<th> ФИО Студента </th>";

        var marks = storeService.getCurrent('marks');
        for (var index in marks.Key) {
            var workType = marks.Key[index];
            console.log(workType);
            html += "<th>" + workType.Name + "</th>";
        }

        html += "</tr>";

        for (var index in marks.Value) {
            var mark = marks.Value[index];
            html += "<tr>";
            html += "<td>" + mark.FIO + "</td>";
            for (var ind in mark.StudentMarks) {
                var studMark = mark.StudentMarks[ind];
                html += "<td>" + studMark.Value + "</td>";
            }
            html += "</tr>";
        }

        html += "</tbody>" +
            "</table>";

        console.log(html);

        var printWin = window.open("");

        printWin.document.write(html);

        printWin.print();
        printWin.close();
    }
})

app.controller('editProjectModal', function ($scope, $rootScope, $http, selectService, communicationService) {
    communicationService.register('open edit Modal', function () {
        $scope.StudentMarks = selectService.getMark().StudentMarks;
        $scope.Mark = selectService.getMark();

        $("#editMarkDialog").modal('show');
        window.openModalId = "editMarkDialog";
    });

    $scope.cancel = function () {
        $('#editMarkDialog').modal('hide');
        communicationService.execute('reload marks', []);
    }

    $scope.editMarks = function () {
        console.clear();
        $('#editMarkDialog').modal('hide');
        //console.log($rootScope.selectedMark);
        communicationService.execute("edit marks close", []);
    }
})
