﻿var marksApp = angular.module('marksApp');

marksApp.controller('filter', function ($scope, $rootScope, $http, dataService, storeService, selectService, communicationService) {

    //load data from server. DONE !!!
    $scope.loaded = false;

    $scope.Years = storeService.getYears(true);
    if ($scope.Years.length == 0) {
        $scope.Groups = [];
        $scope.Disciplines = [];
        $scope.Worktypes = [];
        $scope.Marks = [];

        $scope.loaded = true;
    }
    else {
        $scope.Groups = storeService.getGroups($scope.Years[0], true);
        if ($scope.Groups.length == 0) {
            $scope.Disciplines = [];
            $scope.Worktypes = [];
            $scope.Marks = [];

            selectService.selectYear($scope.Years[0], function (year) { $scope.currentYear = year });
            $scope.currentYear = $scope.Years[0];

            $scope.loaded = true;
        }
        else {
            $scope.Disciplines = storeService.getDisciplines($scope.Groups[0], true);
            if ($scope.Disciplines.length == 0) {
                $scope.Worktypes = [];
                $scope.Marks = [];

                $scope.currentYear = $scope.Years[0];
                $scope.currentGroup = $scope.Groups[0];
                selectService.selectYear($scope.Years[0], function (year) { $scope.currentYear = year });
                selectService.selectGroup($scope.Groups[0], function (group) { $scope.currentGroup = group });

                $scope.loaded = true;
            }
            else {
                selectService.selectDiscipline($scope.Disciplines[0], function (disc) { $scope.currentDiscipline = disc });
                var marks = JSON.parse(storeService.getWorktypesAndMarks($scope.Groups[0], $scope.Disciplines[0].Name, true));
                $scope.Worktypes = marks.Categorys;
                $scope.Marks = marks.Marks;

                for (var i = 0; i < $scope.Marks.length; i++) {
                    var mark = $scope.Marks[i];

                    if (mark.StudentMarks.length < $scope.Worktypes.length) {
                        for (var j = mark.StudentMarks.length; j < $scope.Worktypes.length; j++) {
                            mark.StudentMarks.push({ Key: $scope.Worktypes[j], Value: "" });
                        }
                    }
                }

                // Is this view data? I'm not sure. 
                // It's used for selecting object using angular. So it must be also here.

                selectService.selectDiscipline($scope.Disciplines[0], function (disc) { $scope.currentDiscipline = disc });
                selectService.selectYear($scope.Years[0], function (year) { $scope.currentYear = year });
                selectService.selectGroup($scope.Groups[0], function (group) { $scope.currentGroup = group });
                $scope.currentYear = $scope.Years[0];
                $scope.currentGroup = $scope.Groups[0];
                $scope.currentDiscipline = $scope.Disciplines[0];

                $scope.loaded = true;
            }
        }
    }

    // data Service maybe something like selectService

    //dataService.selectDiscipline($scope.currentDiscipline);
    //dataService.selectGroup($scope.currentGroup);

    // filter groups and disciplines by year
    $scope.filterByYear = function () {
        window.event.preventDefault();
        $scope.Groups = storeService.getGroups($scope.currentYear, true);

        if ($scope.Groups.length > 0) {
            var group = $scope.Groups[0];
            console.clear();
            console.log(group);

            // select and display
            selectService.selectGroup(group, function () { $scope.currentGroup = group });
            $scope.Disciplines = storeService.getDisciplines(group, true);
            selectService.selectDiscipline($scope.Disciplines[0], function (disc) { $scope.currentDiscipline = disc });
        }
        else {
            $scope.Disciplines = [];
        }
    }

    // filter discipline by group
    $scope.filterByGroup = function (group) {
        window.event.preventDefault();

        selectService.selectGroup(group, function (group) { $scope.currentGroup = group; });

        $scope.Disciplines = storeService.getDisciplines(group, true);

        selectService.selectDiscipline($scope.Disciplines[0], function (disc) { $scope.currentDiscipline = disc });
    }

    // reload marks for current discipline
    $scope.filterByDiscipline = function (discipline) {
        window.event.preventDefault();

        selectService.selectDiscipline(discipline, function (disc) { $scope.currentDiscipline = disc });

        var marks = JSON.parse(storeService.getWorktypesAndMarks(selectService.getGroup(), discipline.Name, true));

        $scope.Worktypes = marks.Categorys;
        $scope.Marks = marks.Marks;

        for (var i = 0; i < $scope.Marks.length; i++) {
            var mark = $scope.Marks[i];

            if (mark.StudentMarks.length < $scope.Worktypes.length) {
                for (var j = mark.StudentMarks.length; j < $scope.Worktypes.length; j++) {
                    mark.StudentMarks.push({ Key: $scope.Worktypes[j], Value: "" });
                }
            }
        }
    }

    // Select current mark
    $scope.selectMark = function (mark) {
        if (typeof (selectService.getMark()) != 'undefined') {
            document.getElementById(selectService.getMark().UserId).classList.remove('warning');
        }

        if (selectService.getMark() == mark) {
            selectService.selectMark(undefined);
            $scope.selectedMark = undefined;
            return;
        }
        selectService.selectMark(mark);
        $scope.selectedMark = mark;

        document.getElementById(mark.UserId).classList.add('warning');
    };
    communicationService.register('reload marks', function () {

        var discipline = selectService.getDiscipline();
        var group = selectService.getGroup();

        var marks = JSON.parse(storeService.getWorktypesAndMarks(group, discipline.Name, true));

        $scope.Worktypes = marks.Key;
        $scope.Marks = marks.Value;

        for (var i = 0; i < $scope.Marks.length; i++) {
            var mark = $scope.Marks[i];

            if (mark.StudentMarks.length < $scope.Worktypes.length) {
                for (var j = mark.StudentMarks.length; j < $scope.Worktypes.length; j++) {
                    mark.StudentMarks.push({ Key: $scope.Worktypes[j], Value: "" });
                }
            }
        }
    })
});