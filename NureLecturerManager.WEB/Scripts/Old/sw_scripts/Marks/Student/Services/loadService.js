﻿/* 
    This service has to download information. 
    And it has to provide callback interface for other code.
*/

var app = angular.module('marksApp');

// constant values for url path's where we can get info
app.constant('loadConfig', {
    modelUrl : "/Marks/GetStudentModel",
    disciplineUrl: "/Marks/GetGroupDisciplines?groupNum={0}",
    yearUrl: "/Marks/GetYears",
    groupUrl: "/Marks/GetYearGroups?year={0}",
    markUrl: "/Marks/GetStudentMarks?disciplineName={0}&groupNum={1}"
});

// service that returns for every need object with configured request,
// where one can add listeners for success, error or other response.
app.service('loadService', function ($http, loadConfig) {
    var obj = {};

    var disciplines, years, groups, marks;

    obj.loadDisciplines = function (group, async) {
        if (async) {
            return $http.get(loadConfig.disciplineUrl.format(group));
        }
        else {
            var result;
            $.ajax({
                method: "GET",
                url: loadConfig.disciplineUrl.format(group),
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    }

    obj.loadGroups = function (year, async) {
        if (async) {
            return $http.get(loadConfig.groupUrl.format(year));
        }
        else {
            var result;
            $.ajax({
                method: "GET",
                url: loadConfig.groupUrl.format(year),
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    }

    obj.loadMarks = function (group, discipline, async) {
        if (async) {
            return $http.get(loadConfig.markUrl.format(group, discipline));
        }
        else {
            var result;
            $.ajax({
                method: "GET",
                url: loadConfig.markUrl.format(discipline, group),
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    }

    obj.loadYears = function (async) {
        if (async) {
            return $http.get(loadConfig.yearUrl);
        }
        else {
            var result;
            $.ajax({
                method: "GET",
                url: loadConfig.yearUrl,
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    }

    obj.loadModel = function () {
        return $http.get(loadConfig.modelUrl);
    }

    return obj;
});