﻿/*
    This is service that init point for loading data and it also stores loaded data
*/

var marksApp = angular.module('marksApp');

/*
    Provides opportunity to synchronous or not load all acting objects.
    Also getCurrent method returns currrent object state for some property
*/

marksApp.service("storeService", function ($http, loadService, loadConfig) {
    var obj = {};

    var disciplines, groups, years, workTypes, marks;

    obj.getDisciplines = function (group, sync) {
        var disciplines = loadService.loadDisciplines(group, !sync);
        return disciplines;
    }

    obj.getYears = function (sync) {
        var years = loadService.loadYears(!sync);
        return years;
    }

    obj.getGroups = function (year, sync) {
        var groups = loadService.loadGroups(year, !sync);;
        return groups;
    }
    
    obj.getWorktypesAndMarks = function (groupNum, discipline, sync) {
        var marks = loadService.loadMarks(groupNum, discipline, !sync);
        obj.marks = marks;
        return marks;
    }

    obj.getModel = function () {
        return $http.get(loadConfig.modelUrl).success(function (model) {
            disciplines = model.Disciplines;
            years = model.Years;
            groups = model.Groups;
            workTypes = model.Worktypes;
            marks = model.Marks;
        });
    }

    obj.getCurrent = function (name) {
        switch (name.toUpperCase()) {
            case 'DISCIPLINES' : {
                return disciplines;
            }
            case 'YEARS' : {
                return years;
            }
            case 'GROUPS': {
                return groups;
            }
            case 'MARKS': {
                return JSON.parse(obj.marks);;
            }
            default:
                throw new Error('unknown property');
        }
    }
    return obj;
})