﻿var app = angular.module('groupApp');

app.directive('message', function (communicationService) {
    return {
        restrict: "E",
        templateUrl: "/Scripts/sw_scripts/Groups/Lecturer/Directives/messageTemplate.html",
        link: function (scope, element, attrs) {
            communicationService.register('open message', function (message) {
                scope.Message = message;
                try {
                    scope.$apply();
                }
                catch (e) {
                    console.log(e);
                }
                finally {
                    console.log(message);
                    $("#MessageDialog").modal('show');
                }
            });
            scope.hideMessage = function () {
                console.log('hide');
                $("#MessageDialog").modal('hide');
            };
        }
    }
});