﻿var app = angular.module('groupApp');

app.directive('controlEdit', function ($http, communicationService, selectService) {
    return {
        restrict: "E",
        templateUrl: "/Scripts/sw_scripts/Groups/Lecturer/Directives/Control/editTemplate.html",
        link: function (scope, element, attrs) {
            communicationService.register('control edit open', function () {

                if (typeof (selectService.getControl()) == 'undefined') {
                    var message = { Title: "Инфо", Text: "Выберите контроль для того чтобы его отредактировать." };
                    communicationService.execute('open message', [message]);
                }
                var control = selectService.getControl();
                var splittedDate = control.FormatDate.split(".");
                if (+splittedDate != 0) {
                    control.PassDate = new Date(+splittedDate[2], splittedDate[1] - 1, +splittedDate[0]);
                }
                else {
                    control.PassDate = new Date(+splittedDate[2], 12, +splittedDate[0]);
                }
                $http.get('/Groups/EditGroupControl?groupControlId=' + control.GroupControlId).success(function (data) {
                    console.clear();
                    console.log(control);
                    scope.editControl = {
                        WorkTypes: data, Type: control.Type, Date: control.PassDate,
                        Place: control.Place, GroupControlId: control.GroupControlId, WorkTypeName: control.WorkType,
                        GroupNum: scope.Groups[0].GroupId
                    };

                    $('#GroupControlEditDialog').modal('show');
                });
            });

            scope.cancelControl = function () {
                $('#GroupControlEditDialog').modal('hide');
            }

            scope.editCont = function () {
                var editForm = $('#editControlForm');
                if (!editForm[0].checkValidity()) {
                    editForm.find(':submit').click();
                    return;
                }
                var workType = { WorkTypeId: -1 };
                for (var i = 0; i < scope.editControl.WorkTypes.Length; i++) {
                    if (scope.editControl.WorkTypes[i].Name == scope.editControl.WorkTypeName) {
                        workType = scope.editControl.WorkTypes[i];
                    }
                }

                $http.post('/Groups/EditGroupControl', {
                    WorkType: scope.editControl.WorkTypeName, Type: scope.editControl.Type,
                    PassDate: scope.editControl.Date, Place: scope.editControl.Place
                }).success(function (data) {

                    $('#GroupControlEditDialog').modal('hide');
                    communicationService.printCallBacks();
                    communicationService.execute('open message', [data]);

                    console.log(scope.editControl);
                    communicationService.execute('update edited control', [scope.editControl]);

                });
            }
        }
    }
})