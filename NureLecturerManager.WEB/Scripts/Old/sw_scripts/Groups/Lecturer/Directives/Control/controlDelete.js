﻿var app = angular.module('groupApp');

app.directive('controlDelete', function ($http, communicationService, selectService) {
    return {
        restrict: "E",
        templateUrl: "/Scripts/sw_scripts/Groups/Lecturer/Directives/Control/deleteTemplate.html",
        link: function (scope, element, attrs) {
            communicationService.register('control delete open', function () {
                if (typeof (selectService.getControl()) == 'undefined') {
                    communicationService.execute('open message', [{
                        Title: 'Инфо', Text: 'Выберите контроль в таблице для того, ' +
                            'чтобы его удалить.'
                    }]);
                }
                communicationService.execute('confirm delete', [{ Title: "Подтвердите удаление", Body: "Вы уверены что хотите удалить групповой контроль?" },
                    function () {
                        var control = selectService.getControl();
                        $http.post('/Groups/DeleteGroupControl', { id: control.GroupControlId }).success(function (data) {
                            communicationService.execute('open message', [data]);
                            if (data.Title != 'Ошибка') {
                                communicationService.execute('delete success', [control]);
                            }
                        });
                    }]);
            })
        }
    }
})