﻿var app = angular.module('groupApp');

app.constant('loadConfig', {
    yearUrl: function () {
        return "/Groups/GetYears";
    },
    groupUrl: function (year) {
        return "/Groups/GetYearGroups?year=" + year;
    },
    controlUrl: function (group) {
        return "/Groups/GetGroupControls?GroupId=" + group.GroupId + "&CaptainId=" + group.CaptainId;
    },
    allControlUrl : function (year) {
        return "/Groups/GetAllControls?year=" + year;
    },
    studentUrl: function (group) {
        return "/Groups/GetGroupStudents?GroupId=" + group.GroupId + "&CaptainId=" + group.CaptainId;
    }
});

app.service('storeService', function ($http, loadConfig) {
    var obj = {};
    var years, groups, groupControls, groupStudents, allControls;

    years = [];
    groups = [];
    groupControls = [];
    groupStudents = [];
    allControls = [];

    obj.loadYears = function () {
        return $http.get(loadConfig.yearUrl()).success(function (data) {
            years = data;
        });
    }
    obj.getYears = function () {
        return years;
    }

    obj.loadGroups = function (year) {
        return $http.get(loadConfig.groupUrl(year)).success(function (data) {
            groups = data;
        });
    }
    obj.loadOtherGroups = function () {
        return $http.get('/Groups/GetOtherGroups').success(function (data) {
            groups = data;
        });
    }
    obj.getGroups = function () {
        return groups;
    }

    obj.loadControls = function (group) {
        return $http.get(loadConfig.controlUrl(group)).success(function (data) {
            groupControls = data;
        });
    }
    obj.getControls = function () {
        return groupControls;
    }

    obj.loadStudents = function (group) {
        return $http.get(loadConfig.studentUrl(group)).success(function (data) {
            groupStudents = data;
        });
    }
    obj.getStudents = function () {
        return groupStudents;
    }

    obj.loadAllControls = function (year) {
        return $http.get(loadConfig.allControlUrl(year)).success(function (data) {
            allControls = data;
        });
    }
    obj.getAllControls = function () {
        return allControls;
    }
    obj.deleteAllControls = function () {
        delete allControls;
    }

    obj.printData = function () {
        console.log({
            Years: years,
            Groups: groups,
            Controls: groupControls,
            Students: groupStudents,
            AllControls: allControls
        });
    }
    return obj;
});