﻿var app = angular.module('groupApp');

app.service('studentCUDservice', function (communicationService) {
    var obj = {};
    communicationService.register('student create', function () {
        console.log('student create');
        communicationService.execute('student create open');
    });

    communicationService.register('student edit', function () {
        console.log('student edit');
        communicationService.execute('student edit open');
    });

    communicationService.register('student delete', function () {
        console.log('student delete');
        communicationService.execute('student delete open')
    });
    return obj;
});