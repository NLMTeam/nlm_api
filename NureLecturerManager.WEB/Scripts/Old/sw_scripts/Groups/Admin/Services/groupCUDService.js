﻿var app = angular.module('groupApp');

app.service('groupCUDservice', function (communicationService) {
    var obj = {};
    communicationService.register('group create', function () {
        console.log('group create');
        communicationService.execute('group create open');
    });

    communicationService.register('group edit', function () {
        console.log('group edit');
        communicationService.execute('group edit open');
    });

    communicationService.register('group delete', function () {
        console.log('group delete');
        communicationService.execute('group delete open');
    });

    return obj;
});