﻿var app = angular.module('groupApp');

app.directive('studentEdit', function ($http, communicationService, selectService) {
    return {
        restrict: "E",
        templateUrl: "/Scripts/sw_scripts/Groups/Admin/Directives/Student/editTemplate.html",
        link: function (scope, element, attrs) {
            communicationService.register('student edit open', function () {
                if (typeof (selectService.getStudent()) == 'undefined') {
                    var message = { Title: "Инфо", Text: 'Выберите студента для того, чтобы отредактировать.' };
                    communicationService.execute('open message', [message]);
                }
                var student = selectService.getStudent();

                scope.editedStudent = { userId: student.UserId, FIO: student.FIO, Mail: student.Mail };

                $('#StudentEditDialog').modal('show');
            });

            scope.cancelStudent = function () {
                $('#StudentEditDialog').modal('hide');
            }

            scope.editStudent = function () {
                var editForm = $('#StudentEditForm');
                if (!editForm[0].checkValidity()) {
                    editForm.find(':submit').click();
                    return;
                }

                $http.post('/Groups/EditStudent', scope.editedStudent).success(function (data) {
                    communicationService.execute('open message', [data]);
                    $('#StudentEditDialog').modal('hide');

                    console.log(scope.editedStudent);
                    communicationService.execute('edit student success', [scope.editedStudent]);
                });
            }
        }
    }
})