﻿var app = angular.module('groupApp');

app.directive('studentDelete', function ($http, communicationService, selectService) {
    return {
        restrict: 'E',
        templateUrl: '/Scripts/sw_scripts/Groups/Admin/Directives/Student/deleteTemplate.html',
        link: function (scope, element, attrs) {
            communicationService.register('student delete open', function () {
                if (typeof (selectService.getStudent()) == 'undefined') {
                    var message = { Title: "Инфо", Text: 'Выберите студента для того, чтобы его удалить.' };
                    communicationService.execute('open message', [message]);
                }

                var message = { Title: "Подтвердите удаление", Body: "Вы уверены что хотите удалить студента с базы?" };
                communicationService.execute('confirm delete', [message, function () {
                    var student = selectService.getStudent();
                    $http.post('/Groups/DeleteStudent', { GroupNum: student.GroupId, userId: student.UserId }).success(function (data) {
                        communicationService.execute('open message', [data]);

                        communicationService.execute('delete student success', [student]);
                    });
                }])
            })
        }
    }
})