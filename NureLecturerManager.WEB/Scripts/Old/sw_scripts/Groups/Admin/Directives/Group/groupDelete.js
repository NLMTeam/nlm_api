﻿var app = angular.module('groupApp');

app.directive('groupDelete', function ($http, communicationService, selectService) {
    return {
        restrict: 'E',
        templateUrl: '/Scripts/sw_scripts/Groups/Admin/Directives/Group/deleteTemplate.html',
        link: function (scope, element, attrs) {
            communicationService.register('group delete open', function () {
                var group = selectService.getGroup();
                console.log(group);

                if (typeof (group) == 'undefined') {
                    var message = { Title: "Инфо", Text: 'Выберите группу для того, чтобы ее удалить.' };
                    communicationService.execute('open message', [message]);
                }

                var message = { Title: "Подтвердите удаление", Body: "Вы уверены что хотите удалить группу с базы?" };
                communicationService.execute('confirm delete', [message, function () {
                    $http.post('/Groups/DeleteGroup', { groupNum: group.GroupId }).success(function (data) {
                        communicationService.execute('open message', [data]);
                        communicationService.execute('group delete success', [group]);
                    });
                }]);
            });
        }
    }
})