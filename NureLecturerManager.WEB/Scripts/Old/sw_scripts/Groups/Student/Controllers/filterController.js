﻿var app = angular.module('groupApp');

app.controller('filter', function ($scope, $rootScope, $http, selectService, storeService) {
    $scope.loaded = false;

    storeService.loadModel().success(function (model) {
        $scope.Years = model.Years;
        $scope.Groups = model.Groups;
        $scope.GroupStudents = model.GroupStudents;
        $scope.GroupControls = model.GroupControls;

        $scope.mode = {};
        $scope.mode.Value = "StudentControls";
        selectService.setMode('StudentControls');
        if ($scope.Years.length > 0) {
            $scope.mode.CurrentYear = $scope.Years[0];
        }

        $scope.loaded = true;

        if ($scope.Groups.length > 0) {
            $scope.CurrentGroup = $scope.Groups[0];
        }
    })

    $scope.switchToControls = function (mode) {
        mode.Value = "StudentControls";
        selectService.setMode("StudentControls");

    };
    $scope.switchToInfos = function (mode) {
        mode.Value = "StudentInfos";
        selectService.setMode("StudentInfos");
    };

    $scope.loadGroup = function (group) {
        storeService.loadStudents(group).success(function (data) {
            $scope.GroupStudents = data;
            if (typeof ($scope.chosedStudent) != 'undefined')
                delete $scope.chosedStudent;
        });
        storeService.loadControls(group).success(function (data) {
            $scope.GroupControls = data;
            if (typeof ($scope.chosedControl) != 'undefined')
                delete $scope.chosedControl;
        });

        if (typeof ($scope.CurrentGroup) != 'undefined') {
            var groupElement = document.getElementById('group' + $scope.CurrentGroup.GroupId);
            groupElement.classList.remove('active');
        }

        $scope.CurrentGroup = group;
        selectService.setGroup(group);

        var groupElement = document.getElementById('group' + group.GroupId);
        groupElement.classList.add('active');
        console.log(selectService.getGroup());
    };

    $scope.filterGroups = function () {

        selectService.setYear($scope.mode.CurrentYear);
        delete $scope.CurrentGroup;
        selectService.setGroup(undefined);

        storeService.loadGroups($scope.mode.CurrentYear).success(function (data) {
            $scope.Groups = data;
            delete $scope.CurrentGroup;

            if (selectService.getControlMode() == 'all') {
                storeService.loadAllControls($scope.mode.CurrentYear).success(function (data) {
                    $scope.GroupControls = data;
                });
            }
            else {
                storeService.loadControls($scope.Groups[0]).success(function (data) {
                    $scope.GroupControls = data;
                })
            }
            storeService.loadStudents($scope.Groups[0]).success(function (data) {
                $scope.GroupStudents = data;
            })

            selectService.setGroup($scope.Groups[0]);
        });

    };

    $scope.choseGroupControl = function (control) {
        if (typeof ($scope.chosedControl) != 'undefined' && $scope.chosedControl != null) {
            var element = document.getElementById('control' + $scope.chosedControl.GroupControlId);
            element.classList.remove('warning');
        }

        $scope.chosedControl = control;
        selectService.setControl(control);

        var element = document.getElementById('control' + control.GroupControlId);
        element.classList.add('warning');
    }

    $scope.choseStudent = function (student) {
        if (typeof ($scope.chosedStudent) !== 'undefined' && $scope.chosedStudent != null) {
            var element = document.getElementById('student' + $scope.chosedStudent.UserId);
            element.classList.remove('warning');
        }
        $scope.chosedStudent = student;
        selectService.setStudent(student);

        var element = document.getElementById('student' + student.UserId);

        element.classList.add('warning');
    }

    $scope.loadOtherGroups = function () {
        var groups = storeService.loadOtherGroups().success(function (data) {
            $scope.Groups = data;
        })
    }

    $scope.loadGroupControls = function () {
        selectService.setControlMode("my");
        if (typeof (storeService.getControls()) == 'undefined') {
            storeService.loadControls(selectService.getGroup()).success(function (data) {
                $scope.GroupControls = data;
            });
        }
        else {
            $scope.GroupControls = storeService.getControls();
        }
    }

    $scope.loadAllControls = function () {
        selectService.setControlMode('all');

        storeService.loadAllControls(selectService.getYear()).success(function (data) {
            $scope.GroupControls = data;
        });
    }
});