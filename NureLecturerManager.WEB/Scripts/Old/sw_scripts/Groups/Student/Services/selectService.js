﻿var app = angular.module('groupApp');

app.service('selectService', function () {
    var obj = {};

    var mode, group, year, groupControl, student, controlMode;

    obj.setMode = function (value) {
        mode = value;
    }
    obj.getMode = function () {
        return mode;
    }

    obj.setGroup = function (value) {
        group = value;
    }
    obj.getGroup = function () {
        return group;
    }

    obj.setYear = function (value) {
        year = value;
    }
    obj.getYear = function () {
        return year;
    }

    obj.setControl = function (value) {
        groupControl = value;
    }
    obj.getControl = function () {
        return groupControl;
    }

    obj.setControlMode = function (value) {
        controlMode = value;
    }
    obj.getControlMode = function () {
        return controlMode;
    }

    obj.setStudent = function (value) {
        student = value;
    }
    obj.getStudent = function () {
        return student;
    }

    return obj;
});