﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class News
    {
        public News()
        {
            Subscriptions = new HashSet<NewsSubscription>();
        }

        public int Id { get; set; }
        public string Theme { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDateTime { get; set; }
        public DateTime DateLimit { get; set; }
        public bool IsForLecturers { get; set; }

        public int AuthorId { get; set; }
        public Lecturer Author { get; set; }
        public ICollection<NewsSubscription> Subscriptions { get; set; }
    }
}
