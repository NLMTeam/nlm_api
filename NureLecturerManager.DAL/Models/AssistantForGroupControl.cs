﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class AssistantForGroupControl
    {
        public int GroupControlId { get; set; }
        public int LecturerId { get; set; }

        public GroupControl GroupControl { get; set; }
        public Lecturer Lecturer { get; set; }
    }
}
