﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NLM.DAL.Models
{
    public class ControlPointMark
    {
        public int ControlPointId { get; set; }

        public int UserId { get; set; }
        public int Mark { get; set; }
        public System.DateTime PassDate { get; set; }
        public string FinalMark { get; set; }

        public virtual ControlPoint ControlPoint { get; set; }
        public virtual Student Student { get; set; }

    }
}
