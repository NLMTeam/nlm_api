﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NLM.DAL.Models
{
    public class Lecturer
    {
        public Lecturer()
        {
            Assistants = new HashSet<Assistant>();
            Answers = new HashSet<Answer>();
            News = new HashSet<News>();
            Disciplines = new HashSet<LecturerForYear>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }
        public string Post { get; set; }
        public string Department { get; set; }
        public String Specialization { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<AssistantForGroupControl> AssistantsForGroupControl { get; set; }
        public virtual ICollection<Assistant> Assistants { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<LecturerForYear> Disciplines { get; set; }
    }
}
