﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class Token
    {
        public Int32 UserId { get; set; }
        public byte[] TokenValue { get; set; }

        public virtual User User { get; set; }
    }
}
