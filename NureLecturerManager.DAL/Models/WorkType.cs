﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class WorkType
    {
        public WorkType()
        {
            this.GroupControls = new HashSet<GroupControl>();
        }

        public int WorkTypeId { get; set; }
        public string Name { get; set; }
        public int DisciplineForYearId { get; set; }
        public int? MaxMark { get; set; }
        public int? MinMark { get; set; }
        //public string PassPlace { get; set; }

        public virtual DisciplineForYear DisciplineForYear { get; set; }
        public virtual ICollection<GroupControl> GroupControls { get; set; }
    }
}
