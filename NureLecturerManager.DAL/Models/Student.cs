﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models
{
    public class Student
    {
        public Student()
        {
            Consolidations = new HashSet<Consolidation>();
            ControlPointMarks = new HashSet<ControlPointMark>();
            StudentControls = new HashSet<StudentControl>();
            Questions = new HashSet<Question>();

        }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }
        public string GroupNumber { get; set; }
        public Group Group { get; set; }
        public virtual ICollection<Consolidation> Consolidations { get; set; }
        public virtual ICollection<ControlPointMark> ControlPointMarks { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<StudentControl> StudentControls { get; set; }
        public virtual ICollection<Question> Questions { get; set; } 
    }
}
