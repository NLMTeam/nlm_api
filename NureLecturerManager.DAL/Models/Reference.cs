﻿using System;

namespace NLM.DAL.Models
{
    public class Reference
    { 
        public int WorkTypeId { get; set; }
        public int LiteratureId { get; set; }
        public int PageFrom { get; set; }
        public int? PageTo { get; set; }

        public virtual Literature Literature { get; set; }
        public virtual WorkType WorkType { get; set; }
    }
}
