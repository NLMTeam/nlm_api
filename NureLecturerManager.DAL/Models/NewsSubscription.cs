﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
   public class NewsSubscription
    {
        public int NewsSubscriptionId { get; set; }
        public int NewsId { get; set; }
        public string GroupId { get; set; }

        public Group Group { get; set; }
        public News News { get; set; }
    }
}
