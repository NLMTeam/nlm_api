﻿using System;
using System.Collections.Generic;


namespace NLM.DAL.Models
{
    public class Discipline
    {
        public Discipline()
        {
            this.Literature = new HashSet<Literature>();
            this.SharedMaterials = new HashSet<SharedMaterials>();
            this.DisciplinesForYears = new HashSet<DisciplineForYear>();
            this.CourseProjects = new HashSet<CourseProject>();
        }

        public int DisciplineId { get; set; }
        public bool IsCourseProject { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Literature> Literature { get; set; }
        public virtual ICollection<SharedMaterials> SharedMaterials { get; set; } 
        public virtual ICollection<DisciplineForYear> DisciplinesForYears { get; set; }
        public virtual ICollection<CourseProject> CourseProjects { get; set; }
    }
}
