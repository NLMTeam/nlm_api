﻿using NLM.DAL.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models
{
    public class User
    {
        public User()
        {
            this.CourseProjects = new HashSet<CourseProject>();
        }
        public int UserId { get; set; }
        public string FIO { get; set; }
        //public string Type { get; set; }
        public UserType Type { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public byte[] Saline { get; set; }
        public string Mail { get; set; }
        public string Image { get; set; }

        public virtual ICollection<CourseProject> CourseProjects { get; set; }
        [ForeignKey("UserId")]
        public virtual Lecturer Lecturer { get; set; }
        [ForeignKey("UserId")]
        public virtual Student Student { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
    }
}
