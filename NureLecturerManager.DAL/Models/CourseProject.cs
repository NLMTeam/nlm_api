﻿using NLM.DAL.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models
{
    public class CourseProject
    {
        public CourseProject()
        {
            this.Consolidations = new HashSet<Consolidation>();
        }

        public int CourseProjectId { get; set; }
        public string Theme { get; set; }
        public string Description { get; set; }
        //public string Type { get; set; }
        public CourseProjectType Type { get; set; }
        public string Document { get; set; }
        public int? DisciplineId { get; set; }
        public bool IsArchieved { get; set; }
        public int? UserId { get; set; }

        public virtual Discipline Discipline { get; set; }

        // Преподаватель

        public virtual User User { get; set; }
        public virtual ICollection<Consolidation> Consolidations { get; set; }
    }
}
