﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class LecturerForYear
    {
        public int LecturerForYearId { get; set; }
        public int DisciplineForYearId { get; set; }
        public int LecturerId { get; set; }

        public DisciplineForYear DisciplineForYear { get; set; }
        public virtual Lecturer Lecturer { get; set; }
    }
}
