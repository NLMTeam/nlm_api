﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models
{
    public class ControlPoint
    {
        public ControlPoint()
        {
            this.ControlPointMarks = new HashSet<ControlPointMark>();
        }

        public int ControlPointId { get; set; }
        public int DisciplineForYearId { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public System.DateTime? Date { get; set; }
        public int? MaxMark { get; set; }

        public int? MinMark { get; set; }

        public virtual DisciplineForYear DisciplineForYear { get; set; }

        public virtual ICollection<ControlPointMark> ControlPointMarks { get; set; }
    }
}
