﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models
{
    public class StudentControl
    {
        public int GroupControlId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> Mark { get; set; }
        public System.DateTime PassDate { get; set; }

        public virtual GroupControl GroupControl { get; set; }
        public virtual Student Student { get; set; }
    }
}
