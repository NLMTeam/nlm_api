﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class AssistantForGCRepository : Interfaces.IComplexTwoKeyRepository<AssistantForGroupControl>
    {
        DataContext db;
        //CourseProjectRepository projects;
        //ControlPointRepository points;

        public AssistantForGCRepository(DataContext context)
        {
            db = context;
            //projects = new CourseProjectRepository(context);
            //points = new ControlPointRepository(context);
        }

        public void Create(AssistantForGroupControl item)
        {
            db.AssistantForGroupControl.Add(item);
        }

        public void Delete<L>(L id1, L id2)
        {
            AssistantForGroupControl item = db.AssistantForGroupControl.Find(id1, id2);

            db.AssistantForGroupControl.Remove(item);
        }

        public AssistantForGroupControl Get<L>(L id1, L id2)
        {
            return db.AssistantForGroupControl.Find(id1, id2);
            //if (s != null)
            //{
            //    int id1 = db.PartsOfDiscipline.Find(id).PartId;
            //    return db.PartsOfDiscipline.Where(t => t.PartId == id1).FirstOrDefault();
            //}
            //else
            //    return null;
        }

        public ICollection<AssistantForGroupControl> Find(Func<AssistantForGroupControl, bool> predicate)
        {
            return db.AssistantForGroupControl.Where(predicate).ToList();
        }

        public ICollection<AssistantForGroupControl> GetAll()
        {
            return db.AssistantForGroupControl.ToList();
        }

        public void Update(AssistantForGroupControl item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
