﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class GroupControlRepository : Interfaces.ISingleKeyRepository<GroupControl>
    {
        DataContext db;
        //StudentControlRepository sR;

        public GroupControlRepository(DataContext context)
        {
            db = context;
            //sR = new StudentControlRepository(context);
        }

        public void Create(GroupControl item)
        {
            db.GroupControls.Add(item);
        }

        public void Delete<L>(L id)
        {
            GroupControl item = db.GroupControls.Find(id);
            //foreach (var gC in item.StudentControls)
            //    sR.Delete(gC.StudentControlId);
            db.GroupControls.Remove(item);
        }

        public GroupControl Get<L>(L id)
        {
            return db.GroupControls.Find(id);
        }

        public ICollection<GroupControl> Find(Func<GroupControl, bool> predicate)
        {
            return db.GroupControls.Where(predicate).ToList();
        }

        public ICollection<GroupControl> GetAll()
        {
            return db.GroupControls.ToList();
        }

        public void Update(GroupControl item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
