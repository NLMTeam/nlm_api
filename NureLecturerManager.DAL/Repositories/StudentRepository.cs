﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class StudentRepository : Interfaces.ISingleKeyRepository<Student>
    {
        DataContext db;
        //StudentControlRepository studentcontrols;
        //ConsolidationRepository consolidations;
        //CPMarkRepository marks;

        public StudentRepository(DataContext context)
        {
            db = context;
        }

        public void Create(Student item)
        {
            db.Students.Add(item);
        }

        public void Delete<L>(L id)
        {
            //studentcontrols = new StudentControlRepository(db);
            //consolidations = new ConsolidationRepository(db);
            //marks = new CPMarkRepository(db);
            User item = db.Users.Find(id);
            //foreach (var control in item.StudentControls)
            //{
            //    studentcontrols.Delete(control.StudentControlId);
            //}

            //foreach (var cons in item.Consolidations)
            //{
            //    consolidations.Delete(cons.ConsolidationId);
            //}

            //foreach (var mark in item.ControlPointMarks)
            //{
            //    marks.Delete(mark.ControlPointId, mark.UserId, mark.PassDate);
            //}
            //db.Users.Remove(item.User);
            foreach (var g in db.Groups)
                if (g.CaptainId == item.UserId)
                {
                    g.CaptainId = null;
                    db.Entry(g).State = EntityState.Modified;
                }
            db.Users.Remove(item);
        }

        public Student Get<L>(L id)
        {
            return db.Students.Find(id);
        }

        public ICollection<Student> Find(Func<Student, bool> predicate)
        {
            return db.Students.Where(predicate).ToList();
        }

        public ICollection<Student> GetAll()
        {
            return db.Students.Where(t => t.UserId == t.UserId ).ToList();    
        }

        public void Update(Student item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
