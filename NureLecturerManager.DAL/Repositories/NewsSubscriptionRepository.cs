﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class NewsSubscriptionRepository : Interfaces.ISingleKeyRepository<NewsSubscription>
    {
        DataContext db;

        public NewsSubscriptionRepository(DataContext context)
        {
            db = context;
        }

        public void Create(NewsSubscription item)
        {
            db.NewsSubscriptions.Add(item);
        }

        public void Delete<L>(L id)
        {
            NewsSubscription item = db.NewsSubscriptions.Find(id);
            db.NewsSubscriptions.Remove(item);
        }

        public NewsSubscription Get<L>(L id)
        {
            return db.NewsSubscriptions.Find(id);
        }

        public ICollection<NewsSubscription> Find(Func<NewsSubscription, bool> predicate)
        {
            return db.NewsSubscriptions.Where(predicate).ToList();
        }

        public ICollection<NewsSubscription> GetAll()
        {
            return db.NewsSubscriptions.ToList();
        }

        public void Update(NewsSubscription item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
