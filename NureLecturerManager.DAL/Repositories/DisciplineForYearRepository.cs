﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class DisciplineForYearRepository : Interfaces.ISingleKeyRepository<DisciplineForYear>
    {
        DataContext db;

        public DisciplineForYearRepository(DataContext context)
        {
            db = context;
        }

        public void Create(DisciplineForYear item)
        {
            db.DisciplinesForYear.Add(item);
        }

        public void Delete<L>(L id)
        {
            DisciplineForYear item = db.DisciplinesForYear.Find(id);
            db.DisciplinesForYear.Remove(item);
        }

        public DisciplineForYear Get<L>(L id)
        {
            return db.DisciplinesForYear.Find(id);
        }

        public ICollection<DisciplineForYear> Find(Func<DisciplineForYear, bool> predicate)
        {
            return db.DisciplinesForYear.Include(dfy => dfy.Lecturers).Include(dfy => dfy.Assistants).Where(predicate).ToList();
        }

        public ICollection<DisciplineForYear> GetAll()
        {
            return db.DisciplinesForYear.ToList();
        }

        public void Update(DisciplineForYear item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
