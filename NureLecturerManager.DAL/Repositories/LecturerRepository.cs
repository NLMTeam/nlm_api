﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class LecturerRepository: Interfaces.ISingleKeyRepository<Lecturer>
    { 
        DataContext db;
        //UserRepository users;

        public LecturerRepository(DataContext context)
        {
            db = context;
        }

        public void Create(Lecturer item)
        {
            db.Lecturers.Add(item);
        }

        public void Delete<L>(L id)
        {
            User item = db.Users.Find(id);
            //users = new UserRepository(db);
            //User u = db.Users.Find(id);
            //if (u != null)
            //    db.Users.Remove(u);
            //db.Users.Remove(item.User);
            db.Users.Remove(item);
        }

        public Lecturer Get<L>(L id)
        {
            return db.Lecturers.Find(id);
        }

        public ICollection<Lecturer> Find(Func<Lecturer, bool> predicate)
        {
                return db.Lecturers.Where(predicate).ToList();
        }

        public ICollection<Lecturer> GetAll()
        {
            return db.Lecturers.ToList();
        }

        public void Update(Lecturer item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
