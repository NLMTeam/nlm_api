﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class DisciplineRepository : Interfaces.ISingleKeyRepository<Discipline>
    {
        DataContext db;
        //PartOfDisciplineRepository parts;

        public DisciplineRepository(DataContext context)
        {
            db = context;
            //parts = new PartOfDisciplineRepository(context);
        }

        public void Create(Discipline item)
        {
            db.Disciplines.Add(item);
        }

        public void Delete<L>(L id)
        {
            Discipline item = db.Disciplines.Find(id);
            //foreach (var part in item.PartsOfDiscipline)
            //{
            //    parts.Delete(part.PartId);
            //}
            db.Disciplines.Remove(item);
        }

        public Discipline Get<L>(L id)
        {
            return db.Disciplines.Find(id);
        }

        public ICollection<Discipline> Find(Func<Discipline, bool> predicate)
        {
            return db.Disciplines.Where(predicate).ToList();
        }

        public ICollection<Discipline> GetAll()
        {
            return db.Disciplines.ToList();
        }

        public void Update(Discipline item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
