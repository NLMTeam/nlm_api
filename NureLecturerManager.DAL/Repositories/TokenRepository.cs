﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class TokenRepository : Interfaces.ISingleKeyRepository<Token>
    {
        DataContext db;

        public TokenRepository(DataContext context)
        {
            db = context;
        }

        public void Create(Token item)
        {
            db.Tokens.Add(item);
        }

        public void Delete<L>(L id)
        {
            Token item = db.Tokens.Find(id);
            //foreach (var part in item.PartsOfToken)
            //{
            //    parts.Delete(part.PartId);
            //}
            db.Tokens.Remove(item);
        }

        public Token Get<L>(L id)
        {
            return db.Tokens.Find(id);
        }

        public ICollection<Token> Find(Func<Token, bool> predicate)
        {
            return db.Tokens.Where(predicate).ToList();
        }

        public ICollection<Token> GetAll()
        {
            return db.Tokens.ToList();
        }

        public void Update(Token item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
