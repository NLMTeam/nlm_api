﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Interfaces
{
    public interface ISingleKeyRepository<L>:IRepository<L> where L :class 
    {
        L Get<K>(K id1);
        void Delete<K>(K id1);
    }
}
